#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/Clases/ColeccionGrupos.o \
	${OBJECTDIR}/Clases/ColeccionUsuarios.o \
	${OBJECTDIR}/Clases/ControllerMensaje.o \
	${OBJECTDIR}/Clases/ControllerMensajeFactory.o \
	${OBJECTDIR}/Clases/ControllerUsuario.o \
	${OBJECTDIR}/Clases/ControllerUsuarioFactory.o \
	${OBJECTDIR}/Clases/Conversacion.o \
	${OBJECTDIR}/Clases/ConversacionGrupo.o \
	${OBJECTDIR}/Clases/ConversacionSimple.o \
	${OBJECTDIR}/Clases/Estado.o \
	${OBJECTDIR}/Clases/Grupo.o \
	${OBJECTDIR}/Clases/IControllerMensaje.o \
	${OBJECTDIR}/Clases/IControllerUsuario.o \
	${OBJECTDIR}/Clases/Mensaje.o \
	${OBJECTDIR}/Clases/MensajeContacto.o \
	${OBJECTDIR}/Clases/MensajeSimple.o \
	${OBJECTDIR}/Clases/Menu.o \
	${OBJECTDIR}/Clases/MultimediaImagen.o \
	${OBJECTDIR}/Clases/MultimediaVideo.o \
	${OBJECTDIR}/Clases/Reloj.o \
	${OBJECTDIR}/Clases/Sesion.o \
	${OBJECTDIR}/Clases/Usuario.o \
	${OBJECTDIR}/Clases/Visualizacion.o \
	${OBJECTDIR}/DataTypes/DTConexion.o \
	${OBJECTDIR}/DataTypes/DTContacto.o \
	${OBJECTDIR}/DataTypes/DTConversacion.o \
	${OBJECTDIR}/DataTypes/DTConversacionGrupo.o \
	${OBJECTDIR}/DataTypes/DTConversacionSimple.o \
	${OBJECTDIR}/DataTypes/DTFecha.o \
	${OBJECTDIR}/DataTypes/DTGrupo.o \
	${OBJECTDIR}/DataTypes/DTHora.o \
	${OBJECTDIR}/DataTypes/DTMensaje.o \
	${OBJECTDIR}/DataTypes/DTMensajeContacto.o \
	${OBJECTDIR}/DataTypes/DTMensajeSimple.o \
	${OBJECTDIR}/DataTypes/DTMultimediaImagen.o \
	${OBJECTDIR}/DataTypes/DTMultimediaVideo.o \
	${OBJECTDIR}/DataTypes/DTUsuario.o \
	${OBJECTDIR}/DataTypes/DTVisualizacion.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/laboratorio_5

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/laboratorio_5: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/laboratorio_5 ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/Clases/ColeccionGrupos.o: Clases/ColeccionGrupos.cpp
	${MKDIR} -p ${OBJECTDIR}/Clases
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Clases/ColeccionGrupos.o Clases/ColeccionGrupos.cpp

${OBJECTDIR}/Clases/ColeccionUsuarios.o: Clases/ColeccionUsuarios.cpp
	${MKDIR} -p ${OBJECTDIR}/Clases
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Clases/ColeccionUsuarios.o Clases/ColeccionUsuarios.cpp

${OBJECTDIR}/Clases/ControllerMensaje.o: Clases/ControllerMensaje.cpp
	${MKDIR} -p ${OBJECTDIR}/Clases
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Clases/ControllerMensaje.o Clases/ControllerMensaje.cpp

${OBJECTDIR}/Clases/ControllerMensajeFactory.o: Clases/ControllerMensajeFactory.cpp
	${MKDIR} -p ${OBJECTDIR}/Clases
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Clases/ControllerMensajeFactory.o Clases/ControllerMensajeFactory.cpp

${OBJECTDIR}/Clases/ControllerUsuario.o: Clases/ControllerUsuario.cpp
	${MKDIR} -p ${OBJECTDIR}/Clases
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Clases/ControllerUsuario.o Clases/ControllerUsuario.cpp

${OBJECTDIR}/Clases/ControllerUsuarioFactory.o: Clases/ControllerUsuarioFactory.cpp
	${MKDIR} -p ${OBJECTDIR}/Clases
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Clases/ControllerUsuarioFactory.o Clases/ControllerUsuarioFactory.cpp

${OBJECTDIR}/Clases/Conversacion.o: Clases/Conversacion.cpp
	${MKDIR} -p ${OBJECTDIR}/Clases
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Clases/Conversacion.o Clases/Conversacion.cpp

${OBJECTDIR}/Clases/ConversacionGrupo.o: Clases/ConversacionGrupo.cpp
	${MKDIR} -p ${OBJECTDIR}/Clases
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Clases/ConversacionGrupo.o Clases/ConversacionGrupo.cpp

${OBJECTDIR}/Clases/ConversacionSimple.o: Clases/ConversacionSimple.cpp
	${MKDIR} -p ${OBJECTDIR}/Clases
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Clases/ConversacionSimple.o Clases/ConversacionSimple.cpp

${OBJECTDIR}/Clases/Estado.o: Clases/Estado.cpp
	${MKDIR} -p ${OBJECTDIR}/Clases
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Clases/Estado.o Clases/Estado.cpp

${OBJECTDIR}/Clases/Grupo.o: Clases/Grupo.cpp
	${MKDIR} -p ${OBJECTDIR}/Clases
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Clases/Grupo.o Clases/Grupo.cpp

${OBJECTDIR}/Clases/IControllerMensaje.o: Clases/IControllerMensaje.cpp
	${MKDIR} -p ${OBJECTDIR}/Clases
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Clases/IControllerMensaje.o Clases/IControllerMensaje.cpp

${OBJECTDIR}/Clases/IControllerUsuario.o: Clases/IControllerUsuario.cpp
	${MKDIR} -p ${OBJECTDIR}/Clases
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Clases/IControllerUsuario.o Clases/IControllerUsuario.cpp

${OBJECTDIR}/Clases/Mensaje.o: Clases/Mensaje.cpp
	${MKDIR} -p ${OBJECTDIR}/Clases
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Clases/Mensaje.o Clases/Mensaje.cpp

${OBJECTDIR}/Clases/MensajeContacto.o: Clases/MensajeContacto.cpp
	${MKDIR} -p ${OBJECTDIR}/Clases
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Clases/MensajeContacto.o Clases/MensajeContacto.cpp

${OBJECTDIR}/Clases/MensajeSimple.o: Clases/MensajeSimple.cpp
	${MKDIR} -p ${OBJECTDIR}/Clases
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Clases/MensajeSimple.o Clases/MensajeSimple.cpp

${OBJECTDIR}/Clases/Menu.o: Clases/Menu.cpp
	${MKDIR} -p ${OBJECTDIR}/Clases
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Clases/Menu.o Clases/Menu.cpp

${OBJECTDIR}/Clases/MultimediaImagen.o: Clases/MultimediaImagen.cpp
	${MKDIR} -p ${OBJECTDIR}/Clases
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Clases/MultimediaImagen.o Clases/MultimediaImagen.cpp

${OBJECTDIR}/Clases/MultimediaVideo.o: Clases/MultimediaVideo.cpp
	${MKDIR} -p ${OBJECTDIR}/Clases
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Clases/MultimediaVideo.o Clases/MultimediaVideo.cpp

${OBJECTDIR}/Clases/Reloj.o: Clases/Reloj.cpp
	${MKDIR} -p ${OBJECTDIR}/Clases
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Clases/Reloj.o Clases/Reloj.cpp

${OBJECTDIR}/Clases/Sesion.o: Clases/Sesion.cpp
	${MKDIR} -p ${OBJECTDIR}/Clases
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Clases/Sesion.o Clases/Sesion.cpp

${OBJECTDIR}/Clases/Usuario.o: Clases/Usuario.cpp
	${MKDIR} -p ${OBJECTDIR}/Clases
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Clases/Usuario.o Clases/Usuario.cpp

${OBJECTDIR}/Clases/Visualizacion.o: Clases/Visualizacion.cpp
	${MKDIR} -p ${OBJECTDIR}/Clases
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Clases/Visualizacion.o Clases/Visualizacion.cpp

${OBJECTDIR}/DataTypes/DTConexion.o: DataTypes/DTConexion.cpp
	${MKDIR} -p ${OBJECTDIR}/DataTypes
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DataTypes/DTConexion.o DataTypes/DTConexion.cpp

${OBJECTDIR}/DataTypes/DTContacto.o: DataTypes/DTContacto.cpp
	${MKDIR} -p ${OBJECTDIR}/DataTypes
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DataTypes/DTContacto.o DataTypes/DTContacto.cpp

${OBJECTDIR}/DataTypes/DTConversacion.o: DataTypes/DTConversacion.cpp
	${MKDIR} -p ${OBJECTDIR}/DataTypes
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DataTypes/DTConversacion.o DataTypes/DTConversacion.cpp

${OBJECTDIR}/DataTypes/DTConversacionGrupo.o: DataTypes/DTConversacionGrupo.cpp
	${MKDIR} -p ${OBJECTDIR}/DataTypes
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DataTypes/DTConversacionGrupo.o DataTypes/DTConversacionGrupo.cpp

${OBJECTDIR}/DataTypes/DTConversacionSimple.o: DataTypes/DTConversacionSimple.cpp
	${MKDIR} -p ${OBJECTDIR}/DataTypes
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DataTypes/DTConversacionSimple.o DataTypes/DTConversacionSimple.cpp

${OBJECTDIR}/DataTypes/DTFecha.o: DataTypes/DTFecha.cpp
	${MKDIR} -p ${OBJECTDIR}/DataTypes
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DataTypes/DTFecha.o DataTypes/DTFecha.cpp

${OBJECTDIR}/DataTypes/DTGrupo.o: DataTypes/DTGrupo.cpp
	${MKDIR} -p ${OBJECTDIR}/DataTypes
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DataTypes/DTGrupo.o DataTypes/DTGrupo.cpp

${OBJECTDIR}/DataTypes/DTHora.o: DataTypes/DTHora.cpp
	${MKDIR} -p ${OBJECTDIR}/DataTypes
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DataTypes/DTHora.o DataTypes/DTHora.cpp

${OBJECTDIR}/DataTypes/DTMensaje.o: DataTypes/DTMensaje.cpp
	${MKDIR} -p ${OBJECTDIR}/DataTypes
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DataTypes/DTMensaje.o DataTypes/DTMensaje.cpp

${OBJECTDIR}/DataTypes/DTMensajeContacto.o: DataTypes/DTMensajeContacto.cpp
	${MKDIR} -p ${OBJECTDIR}/DataTypes
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DataTypes/DTMensajeContacto.o DataTypes/DTMensajeContacto.cpp

${OBJECTDIR}/DataTypes/DTMensajeSimple.o: DataTypes/DTMensajeSimple.cpp
	${MKDIR} -p ${OBJECTDIR}/DataTypes
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DataTypes/DTMensajeSimple.o DataTypes/DTMensajeSimple.cpp

${OBJECTDIR}/DataTypes/DTMultimediaImagen.o: DataTypes/DTMultimediaImagen.cpp
	${MKDIR} -p ${OBJECTDIR}/DataTypes
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DataTypes/DTMultimediaImagen.o DataTypes/DTMultimediaImagen.cpp

${OBJECTDIR}/DataTypes/DTMultimediaVideo.o: DataTypes/DTMultimediaVideo.cpp
	${MKDIR} -p ${OBJECTDIR}/DataTypes
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DataTypes/DTMultimediaVideo.o DataTypes/DTMultimediaVideo.cpp

${OBJECTDIR}/DataTypes/DTUsuario.o: DataTypes/DTUsuario.cpp
	${MKDIR} -p ${OBJECTDIR}/DataTypes
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DataTypes/DTUsuario.o DataTypes/DTUsuario.cpp

${OBJECTDIR}/DataTypes/DTVisualizacion.o: DataTypes/DTVisualizacion.cpp
	${MKDIR} -p ${OBJECTDIR}/DataTypes
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DataTypes/DTVisualizacion.o DataTypes/DTVisualizacion.cpp

${OBJECTDIR}/main.o: main.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
