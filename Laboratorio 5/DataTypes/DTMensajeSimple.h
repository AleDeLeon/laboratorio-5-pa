#ifndef DTMENSAJESIMPLE_H
#define DTMENSAJESIMPLE_H

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

#include "DTUsuario.h"
#include "DTVisualizacion.h"

using namespace std;

class DTMensajeSimple:public DTMensaje{
public:
    DTMensajeSimple();
    DTMensajeSimple(const DTMensajeSimple& orig);
    DTMensajeSimple(string Texto, int Codigo, DTFecha *FechaEnviado, DTHora *HoraEnviado, DTUsuario *Emisor, list<DTUsuario*> *Receptores, list<DTVisualizacion*> *Visualizaciones);
    
    virtual ~DTMensajeSimple();
    
    string getTexto();
private:
    string texto;
};

#endif

