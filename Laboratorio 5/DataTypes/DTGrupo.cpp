#include "DTGrupo.h"

//CONSTRUCTORES

DTGrupo::DTGrupo() {
}

DTGrupo::DTGrupo(const DTGrupo& orig) {
}

DTGrupo::DTGrupo(string Nombre, string Imagen){
    this->nombre = Nombre;
    this->imagen = Imagen;
}

//DESTRUCTOR

DTGrupo::~DTGrupo() {
}


//GETTERS

string DTGrupo::getImagen(){
    return this->imagen;
}

string DTGrupo::getNombre(){
    return this->nombre;
}

void DTGrupo::setNombre(string Nombre){
    this->nombre = Nombre;
}