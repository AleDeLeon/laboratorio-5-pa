#include "DTConversacion.h"
#include "DTConversacionGrupo.h"

//CONSTRUCTORES

DTConversacionGrupo::DTConversacionGrupo() {
}

DTConversacionGrupo::DTConversacionGrupo(const DTConversacionGrupo& orig) {
}

DTConversacionGrupo::DTConversacionGrupo(string Nombre, int ID, bool Estado):DTConversacion(ID, Estado){
    this->nombre = Nombre;
}

//DESTRUCTOR

DTConversacionGrupo::~DTConversacionGrupo() {
}

//GETTERS

string DTConversacionGrupo::getNombre(){
    return this->nombre;
}

