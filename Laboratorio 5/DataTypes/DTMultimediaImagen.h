#ifndef DTMULTIMEDIAIMAGEN_H
#define DTMULTIMEDIAIMAGEN_H

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

#include "DTUsuario.h"
#include "DTVisualizacion.h"

using namespace std;

class DTMultimediaImagen:public DTMensaje{
public:
    DTMultimediaImagen();
    DTMultimediaImagen(const DTMultimediaImagen& orig);
    DTMultimediaImagen(string Url, string Formato, float Tamanio, string Texto, int Codigo, DTFecha *FechaEnviado, DTHora *HoraEnviado, DTUsuario *Emisor, list<DTUsuario*> *Receptores, list<DTVisualizacion*> *Visualizaciones);
    
    virtual ~DTMultimediaImagen();
    
    string getUrl();
    string getFormato();
    float getTamanio();
    string getTexto();
private:
    string url;
    string formato;
    float tamanio;
    string texto;
};

#endif

