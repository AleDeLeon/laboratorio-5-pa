#ifndef DTMENSAJECONTACTO_H
#define DTMENSAJECONTACTO_H

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

#include "DTUsuario.h"
#include "DTVisualizacion.h"

using namespace std;

class DTMensajeContacto:public DTMensaje {
public:
    DTMensajeContacto();
    DTMensajeContacto(const DTMensajeContacto& orig);
    DTMensajeContacto(string Nombre, string Telefono, int Codigo, DTFecha *FechaEnviado, DTHora *HoraEnviado, DTUsuario *Emisor, list<DTUsuario*> *Receptores, list<DTVisualizacion*> *Visualizaciones);
    
    virtual ~DTMensajeContacto();
    
    string getNombre();
    string getTelefono();
private:
    string nombre;
    string telefono;
};

#endif

