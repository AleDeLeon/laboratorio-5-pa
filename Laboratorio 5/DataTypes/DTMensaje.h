#ifndef DTMENSAJE_H
#define DTMENSAJE_H

#include "../DataTypes/DTFecha.h"
#include "../DataTypes/DTHora.h"
#include "../DataTypes/DTVisualizacion.h"
#include "../DataTypes/DTUsuario.h"

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

using namespace std;

class DTMensaje {
public:
    DTMensaje();
    DTMensaje(const DTMensaje& orig);
    DTMensaje(int Codigo, DTFecha *FechaEnviado, DTHora *HoraEnviado, DTUsuario *Emisor, list<DTUsuario*> *Receptores, list<DTVisualizacion*> *Visualizaciones);
    
    virtual ~DTMensaje();
    
    int getCodigo();
    DTFecha *getFecha();
    DTHora *getHora();
    DTUsuario *getEmisor();
    list<DTUsuario*> *getReceptores();
    list<DTVisualizacion*> *getVisualizaciones();
    
private:
    int codigo;
    DTFecha *fechaEnviado;
    DTHora *horaEnviado;
    DTUsuario *emisor;
    list<DTUsuario*> *receptores;
    list<DTVisualizacion*> *visualizaciones;
};

#endif

