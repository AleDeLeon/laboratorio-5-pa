#include "DTMensaje.h"
#include "DTMultimediaImagen.h"

//CONSTRUCTORES

DTMultimediaImagen::DTMultimediaImagen() {
}

DTMultimediaImagen::DTMultimediaImagen(const DTMultimediaImagen& orig) {
}

DTMultimediaImagen::DTMultimediaImagen(string Url, string Formato, float Tamanio, string Texto, int Codigo, DTFecha *FechaEnviado, DTHora *HoraEnviado, DTUsuario *Emisor, list<DTUsuario*> *Receptores,list<DTVisualizacion*> *Visualizaciones):DTMensaje(Codigo, FechaEnviado, HoraEnviado, Emisor, Receptores, Visualizaciones){
    this->url = Url;
    this->formato = Formato;
    this->tamanio = Tamanio;
    this->texto = Texto;
}

//DESTRUCTOR

DTMultimediaImagen::~DTMultimediaImagen() {
}

//GETTERS

string DTMultimediaImagen::getUrl(){
    return this->url;
}

string DTMultimediaImagen::getFormato(){
    return this->formato;
}

float DTMultimediaImagen::getTamanio(){
    return this->tamanio;
}

string DTMultimediaImagen::getTexto(){
    return this->texto;
}
