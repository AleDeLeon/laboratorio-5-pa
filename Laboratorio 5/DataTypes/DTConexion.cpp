#include "DTConexion.h"

//CONSTRUCTORES

DTConexion::DTConexion() {
}

DTConexion::DTConexion(const DTConexion& orig) {
}

DTConexion::DTConexion(int Dia, int Mes, int Anio, int Minutos, int Horas){
    this->dia = Dia;
    this->mes = Mes;
    this->anio = Anio;
    this->minutos = Minutos;
    this->horas = Horas;
}

//DESTRUCTOR

DTConexion::~DTConexion() {
}

//GETTERS

int DTConexion::getDia(){
    return this->dia;
}

int DTConexion::getMes(){
    return this->mes;
}

int DTConexion::getAnio(){
    return this->anio;
}

int DTConexion::getHoras(){
    return this->horas;
}

int DTConexion::getMinutos(){
    return this->minutos;
}