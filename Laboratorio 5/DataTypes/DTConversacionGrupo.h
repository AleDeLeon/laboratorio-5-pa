#ifndef DTCONVERSACIONGRUPO_H
#define DTCONVERSACIONGRUPO_H

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

using namespace std;

class DTConversacionGrupo:public DTConversacion {
public:
    DTConversacionGrupo();
    DTConversacionGrupo(const DTConversacionGrupo& orig);
    DTConversacionGrupo(string Nombre, int ID, bool Estado);
    
    virtual ~DTConversacionGrupo();
    
    string getNombre();
private:
    string nombre;
};

#endif

