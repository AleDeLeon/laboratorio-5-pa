#include "DTMensaje.h"
#include "DTMultimediaVideo.h"

//CONSTRUCTORES

DTMultimediaVideo::DTMultimediaVideo() {
}

DTMultimediaVideo::DTMultimediaVideo(const DTMultimediaVideo& orig) {
}

DTMultimediaVideo::DTMultimediaVideo(string Url, int MinutosDuracion, int SegundosDuracion, int Codigo, DTFecha *FechaEnviado, DTHora *HoraEnviado, DTUsuario *Emisor, list<DTUsuario*> *Receptores, list<DTVisualizacion*> *Visualizaciones):DTMensaje(Codigo, FechaEnviado, HoraEnviado, Emisor, Receptores, Visualizaciones){
    this->url = Url;
    this->segundosDuracion = SegundosDuracion;
    this->minutosDuracion = MinutosDuracion;
}

//DESTRUCTOR

DTMultimediaVideo::~DTMultimediaVideo() {
}

//GETTERS

string DTMultimediaVideo::getUrl(){
    return this->url;
}

int DTMultimediaVideo::getSegundosDuracion(){
    return this->segundosDuracion;
}

int DTMultimediaVideo::getMinutosDuracion(){
    return this->minutosDuracion;
}