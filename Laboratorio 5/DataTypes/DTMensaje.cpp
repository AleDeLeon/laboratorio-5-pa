#include "DTMensaje.h"

//CONSTRUCTORES

DTMensaje::DTMensaje() {
}

DTMensaje::DTMensaje(const DTMensaje& orig) {
}

DTMensaje::DTMensaje(int Codigo, DTFecha *FechaEnviado, DTHora *HoraEnviado, DTUsuario *Emisor, list<DTUsuario*> *Receptores, list<DTVisualizacion*> *Visualizaciones){
    this->codigo = Codigo;
    this->horaEnviado = HoraEnviado;
    this->fechaEnviado = FechaEnviado;
    this->emisor = Emisor;
    this->receptores = Receptores;
    this->visualizaciones = Visualizaciones;
}

//DESTRUCTOR

DTMensaje::~DTMensaje() {
}

//GETTERS

int DTMensaje::getCodigo(){
    return this->codigo;
}

DTHora *DTMensaje::getHora(){
    return this->horaEnviado;
}

DTFecha *DTMensaje::getFecha(){
    return this->fechaEnviado;
}

DTUsuario *DTMensaje::getEmisor(){
    return this->emisor;
}

list<DTUsuario*> *DTMensaje::getReceptores(){
    return this->receptores;
}

list<DTVisualizacion*> *DTMensaje::getVisualizaciones(){
    return this->visualizaciones;
}
