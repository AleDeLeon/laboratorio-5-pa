#ifndef DTCONTACTO_H
#define DTCONTACTO_H

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

using namespace std;

class DTContacto {
public:
    DTContacto();
    DTContacto(const DTContacto& orig);
    DTContacto(string Telefono, string Nombre);
    
    virtual ~DTContacto();
    
    string getTelefono();
    string getNombre();
private:
    string telefono;
    string nombre;
};

#endif

