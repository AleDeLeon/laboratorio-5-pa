#include "DTFecha.h"

//CONSTRUCTORES

DTFecha::DTFecha() {
}

DTFecha::DTFecha(const DTFecha& orig) {
}

DTFecha::DTFecha(int Dia, int Mes, int Anio){
    this->dia = Dia;
    this->mes = Mes;
    this->anio = Anio;
}

//DESTRUCTOR

DTFecha::~DTFecha() {
}

//GETTERS

int DTFecha::getDia(){
    return this->dia;
}

int DTFecha::getMes(){
    return this->mes;
}

int DTFecha::getAnio(){
    return this->anio;
}

