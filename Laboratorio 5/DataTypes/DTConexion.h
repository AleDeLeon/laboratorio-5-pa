#ifndef DTCONEXION_H
#define DTCONEXION_H

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

using namespace std;

class DTConexion {
public:
    DTConexion();
    DTConexion(const DTConexion& orig);
    DTConexion(int Dia, int Mes, int Anio, int Minutos, int Horas);
    
    virtual ~DTConexion();
    
    int getDia();
    int getMes();
    int getAnio();
    int getHoras();
    int getMinutos();
private:
    int dia;
    int mes;
    int anio;
    int minutos;
    int horas;
};

#endif
