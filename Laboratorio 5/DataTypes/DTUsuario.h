#ifndef DTUSUARIO_H
#define DTUSUARIO_H

#include "../DataTypes/DTFecha.h"
#include "../DataTypes/DTHora.h"
#include "../DataTypes/DTConexion.h"

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

using namespace std;

class DTUsuario {
public:
    DTUsuario();
    DTUsuario(const DTUsuario& orig);
    DTUsuario(string Telefono, string Nombre, string ImagenDePerfil, string Descripcion);
    
    virtual ~DTUsuario();
    
    string getTelefono();
    string getNombre();
    string getImagenDePerfil();
    string getDescripcion();
    DTFecha *getFechaRegistro();
    DTConexion *getUltimaConexion();
private:
    string telefono;
    string nombre;
    string imagenDePerfil;
    string descripcion;
};

#endif

