#include "DTMensaje.h"
#include "DTMensajeContacto.h"

//CONSTRUCTORES

DTMensajeContacto::DTMensajeContacto() {
}

DTMensajeContacto::DTMensajeContacto(const DTMensajeContacto& orig) {
}

DTMensajeContacto::DTMensajeContacto(string Nombre, string Telefono, int Codigo, DTFecha *FechaEnviado, DTHora *HoraEnviado, DTUsuario *Emisor, list<DTUsuario*> *Receptores, list<DTVisualizacion*> *Visualizaciones):DTMensaje(Codigo, FechaEnviado, HoraEnviado, Emisor, Receptores, Visualizaciones){
    this->nombre = Nombre;
    this->telefono = Telefono;
}
//DESTRUCTOR

DTMensajeContacto::~DTMensajeContacto() {
}

//GETTERS

string DTMensajeContacto::getNombre(){
    return this->nombre;
}

string DTMensajeContacto::getTelefono(){
    return this->telefono;
}