#include "DTContacto.h"

//CONSTRUCTORES

DTContacto::DTContacto() {
}

DTContacto::DTContacto(const DTContacto& orig) {
}

DTContacto::DTContacto(string Telefono, string Nombre){
    this->telefono = Telefono;
    this->nombre = Nombre;
}

//DESTRUCTOR

DTContacto::~DTContacto() {
}

//GETTERS

string DTContacto::getNombre(){
    return this->nombre;
}

string DTContacto::getTelefono(){
    return this->telefono;
}
