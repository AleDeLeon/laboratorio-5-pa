#ifndef DTVISUALIZACION_H
#define DTVISUALIZACION_H

#include "../DataTypes/DTFecha.h"
#include "../DataTypes/DTHora.h"
#include "../DataTypes/DTUsuario.h"

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

using namespace std;

class DTVisualizacion {
public:
    DTVisualizacion();
    DTVisualizacion(const DTVisualizacion& orig);
    DTVisualizacion(bool Visto, DTHora *HoraVisualizacion, DTFecha *FechaVisualizacion, DTUsuario *UsuarioVisualizacion);
    
    virtual ~DTVisualizacion();
    
    bool getVisto();
    DTHora *getHoraVisualizacion();
    DTFecha *getFechaVisualizacion();
    DTUsuario *getUsuarioVisualizacion();
private:
    DTHora *horaVisualizacion;
    DTFecha *fechaVisualizacion;
    DTUsuario *usuarioVisualizacion;
    bool visto;
};

#endif

