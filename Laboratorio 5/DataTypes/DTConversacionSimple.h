#ifndef DTCONVERSACIONSIMPLE_H
#define DTCONVERSACIONSIMPLE_H

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

using namespace std;

class DTConversacionSimple:public DTConversacion {
public:
    DTConversacionSimple();
    DTConversacionSimple(const DTConversacionSimple& orig);
    DTConversacionSimple(string Nombre, string Telefono, int ID, bool Estado);
    
    virtual ~DTConversacionSimple();
    
    string getNombre();
    string getTelefono();
private:
    string nombre;
    string telefono;
};

#endif

