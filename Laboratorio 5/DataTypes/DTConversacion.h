#ifndef DTCONVERSACION_H
#define DTCONVERSACION_H

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

using namespace std;

class DTConversacion {
public:
    DTConversacion();
    DTConversacion(const DTConversacion& orig);
    DTConversacion(int ID, bool Estado);
    
    virtual ~DTConversacion();
    
    int getID();
    bool getEstado();
private:
    int id;
    bool estado;
};

#endif

