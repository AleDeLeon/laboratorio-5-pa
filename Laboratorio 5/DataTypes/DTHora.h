#ifndef DTHORA_H
#define DTHORA_H

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

using namespace std;

class DTHora {
public:
    DTHora();
    DTHora(const DTHora& orig);
    DTHora(int Horas, int Minutos);
    
    virtual ~DTHora();
    
    int getHoras();
    int getMinutos();
private:
    int horas;
    int minutos;
};

#endif

