#include "DTMensaje.h"
#include "DTMensajeSimple.h"

//CONSTRUCTORES

DTMensajeSimple::DTMensajeSimple() {
}

DTMensajeSimple::DTMensajeSimple(const DTMensajeSimple& orig) {
}

DTMensajeSimple::DTMensajeSimple(string Texto, int Codigo, DTFecha *FechaEnviado, DTHora *HoraEnviado, DTUsuario *Emisor, list<DTUsuario*> *Receptores, list<DTVisualizacion*> *Visualizaciones):DTMensaje(Codigo, FechaEnviado, HoraEnviado, Emisor, Receptores, Visualizaciones){
    this->texto = Texto;
}

//DESTRUCTOR

DTMensajeSimple::~DTMensajeSimple() {
}

//GETTERS

string DTMensajeSimple::getTexto(){
    return this->texto;
}
