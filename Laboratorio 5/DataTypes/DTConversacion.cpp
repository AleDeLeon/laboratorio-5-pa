#include "DTConversacion.h"

//CONSTRUCTORES

DTConversacion::DTConversacion() {
}

DTConversacion::DTConversacion(const DTConversacion& orig) {
}

DTConversacion::DTConversacion(int ID, bool Estado){
    this->id = ID;
    this->estado = Estado;
}

//DESTRUCTOR

DTConversacion::~DTConversacion() {
}

//GETTERS

int DTConversacion::getID(){
    return this->id;
}

bool DTConversacion::getEstado(){
    return this->estado;
}