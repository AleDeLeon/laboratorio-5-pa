#include "DTUsuario.h"

//CONSTRUCTORES

DTUsuario::DTUsuario() {
}

DTUsuario::DTUsuario(const DTUsuario& orig) {
}

DTUsuario::DTUsuario(string Telefono, string Nombre, string ImagenDePerfil, string Descripcion){
    this->telefono = Telefono;
    this->nombre = Nombre;
    this->imagenDePerfil = ImagenDePerfil;
    this->descripcion = Descripcion;
}

//DESTRUCTOR

DTUsuario::~DTUsuario() {
}

//GETTERS

string DTUsuario::getTelefono(){
    return this->telefono;
}

string DTUsuario::getNombre(){
    return this->nombre;
}

string DTUsuario::getImagenDePerfil(){
    return this->imagenDePerfil;
}

string DTUsuario::getDescripcion(){
    return this->descripcion;
}