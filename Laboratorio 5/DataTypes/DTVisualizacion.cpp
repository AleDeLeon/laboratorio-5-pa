#include "DTVisualizacion.h"

//CONSTRUCTORES

DTVisualizacion::DTVisualizacion() {
}

DTVisualizacion::DTVisualizacion(const DTVisualizacion& orig) {
}

DTVisualizacion::DTVisualizacion(bool Visto, DTHora *HoraVisualizacion, DTFecha *FechaVisualizacion, DTUsuario *UsuarioVisualizacion){
    this->visto = Visto;
    this->fechaVisualizacion = FechaVisualizacion;
    this->horaVisualizacion = HoraVisualizacion;
    this->usuarioVisualizacion = UsuarioVisualizacion;
}

//DESTRUCTOR

DTVisualizacion::~DTVisualizacion() {
}

//GETTERS

bool DTVisualizacion::getVisto(){
    return this->visto;
}

DTHora *DTVisualizacion::getHoraVisualizacion(){
    return this->horaVisualizacion;
}

DTFecha *DTVisualizacion::getFechaVisualizacion(){
    return this->fechaVisualizacion;
}

DTUsuario *DTVisualizacion::getUsuarioVisualizacion(){
    return this->usuarioVisualizacion;
}