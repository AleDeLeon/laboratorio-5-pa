#include "DTConversacion.h"
#include "DTConversacionSimple.h"

//CONSTRUCTORES

DTConversacionSimple::DTConversacionSimple() {
}

DTConversacionSimple::DTConversacionSimple(const DTConversacionSimple& orig) {
}

DTConversacionSimple::DTConversacionSimple(string Nombre, string Telefono, int ID, bool Estado):DTConversacion(ID, Estado){
    this->nombre = Nombre;
    this->telefono = Telefono;
}

//DESTRUCTOR

DTConversacionSimple::~DTConversacionSimple() {
}

//GETTERS

string DTConversacionSimple::getNombre(){
    return this->nombre;
}

string DTConversacionSimple::getTelefono(){
    return this->telefono;
}