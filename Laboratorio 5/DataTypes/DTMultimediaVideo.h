#ifndef DTMULTIMEDIAVIDEO_H
#define DTMULTIMEDIAVIDEO_H

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

#include "DTUsuario.h"

using namespace std;

class DTMultimediaVideo:public DTMensaje {
public:
    DTMultimediaVideo();
    DTMultimediaVideo(const DTMultimediaVideo& orig);
    DTMultimediaVideo(string Url, int MinutosDuracion, int SegundosDuracion, int Codigo, DTFecha *FechaEnviado, DTHora *HoraEnviado,  DTUsuario *Emisor, list<DTUsuario*> *Receptores, list<DTVisualizacion*> *Visualizaciones);
    
    virtual ~DTMultimediaVideo();
    
    string getUrl();
    int getMinutosDuracion();
    int getSegundosDuracion();
private:
    string url;
    int minutosDuracion;
    int segundosDuracion;
};

#endif

