#ifndef DTGRUPO_H
#define DTGRUPO_H

#include "../DataTypes/DTFecha.h"
#include "../DataTypes/DTHora.h"

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

using namespace std;

class DTGrupo {
public:
    DTGrupo();
    DTGrupo(const DTGrupo& orig);
    DTGrupo(string Nombre, string Imagen);
    
    virtual ~DTGrupo();
    
    string getNombre();
    string getImagen();
    DTFecha *getFechaCreacion();
    DTHora *getHoraCreacion();
    
    void setNombre(string Nombre);
private:
    string nombre;
    string imagen;
};

#endif

