#ifndef DTFECHA_H
#define DTFECHA_H

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

using namespace std;

class DTFecha {
public:
    DTFecha();
    DTFecha(const DTFecha& orig);
    DTFecha(int Dia, int Mes, int Anio);
    
    virtual ~DTFecha();
    
    int getDia();
    int getMes();
    int getAnio();
private:
    int dia;
    int mes;
    int anio;
};

#endif

