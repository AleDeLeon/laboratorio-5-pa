#include "Clases/Usuario.h"
#include "Clases/Sesion.h"
#include "Clases/ColeccionUsuarios.h"
#include "Clases/Sesion.h"
#include "Clases/IControllerUsuario.h"
#include "Clases/ControllerUsuario.h"
#include "DataTypes/DTUsuario.h"
#include "Clases/Menu.h"

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <unistd.h>
#include <list>

using namespace std;

int main(int argc, char** argv) {
    
    Menu *m;
    m = m->getInstancia();
    m->interaccionMenuPrincipal();
    return 0;
}