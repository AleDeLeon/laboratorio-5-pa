#include "ColeccionGrupos.h"

ColeccionGrupos *ColeccionGrupos::instancia = NULL;

//CONSTRUCTORES

ColeccionGrupos::ColeccionGrupos() {
    grupos = new list<Grupo*>;
}

//GETTERS

ColeccionGrupos *ColeccionGrupos::getInstancia(){
    if(instancia == NULL){
        instancia = new ColeccionGrupos();
    }
    return instancia;
}        

list<Grupo*> *ColeccionGrupos::getListaGrupos(){
    return grupos;
}

//OTROS

void ColeccionGrupos::agregarGrupo(Grupo *g){
    this->grupos->push_back(g);
}

list<Grupo*>::iterator ColeccionGrupos::borrarGrupo(list<Grupo*>::iterator i){
    list<Grupo*>::iterator nuevoI;
    nuevoI = grupos->erase(i);
    return nuevoI;
}