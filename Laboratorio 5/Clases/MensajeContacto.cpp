#include "Mensaje.h"
#include "MensajeContacto.h"
#include "Usuario.h"

//CONSTRUCTORES

MensajeContacto::MensajeContacto(string Nombre, string Telefono):Mensaje(){
    this->nombre = Nombre;
    this->telefono = Telefono;
}

MensajeContacto::MensajeContacto() {
}

MensajeContacto::MensajeContacto(const MensajeContacto& orig) {
}

//DESTRUCTORES

MensajeContacto::~MensajeContacto() {
}

//GETTERS

string MensajeContacto::getNombre(){
    return this->nombre;
}

string MensajeContacto::getTelefono(){
    return this->telefono;
}


//SETTERS

void MensajeContacto::setNombre(string Nombre){
    this->nombre = Nombre;
}

void MensajeContacto::setTelefono(string Telefono){
    this->telefono = Telefono;
}

//OPERACIONES DE VER MENSAJE

DTMensaje *MensajeContacto::getDatos(Mensaje* m){
    DTMensaje *datosMensaje;
    DTUsuario *datosEmisor;
    DTUsuario *auxReceptor;
    DTVisualizacion *auxVisualizacion;
    DTUsuario *datosUsuarioVisto;
    list<DTUsuario*> *datosReceptores = new list<DTUsuario*>;
    list<DTVisualizacion*> *datosVisualizaciones = new list<DTVisualizacion*>;
    
    for(list<Usuario*>::iterator i = m->getReceptores()->begin(); i != m->getReceptores()->end(); i++){
        auxReceptor = new DTUsuario((*i)->getTelefono(), (*i)->getNombre(), (*i)->getImagenDePerfil(), (*i)->getDescripcion());
        datosReceptores->push_back(auxReceptor);
    }
    
    for(list<Visualizacion*>::iterator j = m->getVisualizaciones()->begin(); j != m->getVisualizaciones()->end(); j++){
        datosUsuarioVisto = new DTUsuario((*j)->getUsuarioVisto()->getTelefono(), (*j)->getUsuarioVisto()->getNombre(), (*j)->getUsuarioVisto()->getImagenDePerfil(), (*j)->getUsuarioVisto()->getDescripcion());
        auxVisualizacion = new DTVisualizacion((*j)->getVisto(), (*j)->getHoraVisualizacion(), (*j)->getFechaVisualizacion(), datosUsuarioVisto);
        datosVisualizaciones->push_back(auxVisualizacion);
    }
    
    datosEmisor = new DTUsuario(m->getEmisor()->getTelefono(), m->getEmisor()->getNombre(), m->getEmisor()->getImagenDePerfil(), m->getEmisor()->getDescripcion());
    datosMensaje = new DTMensajeContacto((&dynamic_cast<MensajeContacto&>(*m))->getNombre(), (&dynamic_cast<MensajeContacto&>(*m))->getTelefono(), m->getCodigo(), m->getFechaEnviado(), m->getHoraEnviado(), datosEmisor, datosReceptores, datosVisualizaciones);
    return datosMensaje;
}