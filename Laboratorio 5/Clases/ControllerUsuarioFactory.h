#ifndef CONTROLLERUSUARIOFACTORY_H
#define CONTROLLERUSUARIOFACTORY_H

#include "IControllerUsuario.h"
#include "ControllerUsuario.h"

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

using namespace std;

class ControllerUsuarioFactory {
public:
    ControllerUsuarioFactory();
    ControllerUsuarioFactory(const ControllerUsuarioFactory& orig);
    
    virtual ~ControllerUsuarioFactory();
    
    IControllerUsuario *getIControllerUsuario();
};

#endif

