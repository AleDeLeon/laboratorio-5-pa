#ifndef ESTADO_H
#define ESTADO_H

#include "../DataTypes/DTFecha.h"
#include "../DataTypes/DTHora.h"

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

using namespace std;

class Estado {
public:
    Estado();
    Estado(string texto);
    Estado(const Estado& orig);
    
    virtual ~Estado();
    
    string getTexto();
    DTFecha getFechaCreacion();
    DTHora getHoraCreacion();
    
    void setTexto(string texto);
    void setFechaCreacion(DTFecha fechaCreacion);
    void setHoraCreacion(DTHora horaCreacion);
private:
    string texto;
    DTFecha fechaCreacion;
    DTHora horaCreacion;
};

#endif

