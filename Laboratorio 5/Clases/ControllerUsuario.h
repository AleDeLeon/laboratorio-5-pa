#ifndef CONTROLLERUSUARIO_H
#define CONTROLLERUSUARIO_H

#include "Reloj.h"
#include "Sesion.h"
#include "ColeccionGrupos.h"
#include "ColeccionUsuarios.h"

#include "Mensaje.h"
#include "Usuario.h"
#include "MensajeSimple.h"
#include "MensajeContacto.h"
#include "MultimediaImagen.h"
#include "MultimediaVideo.h"
#include "Conversacion.h"
#include "Grupo.h"
#include "Visualizacion.h"

#include "../DataTypes/DTConexion.h"
#include "../DataTypes/DTUsuario.h"
#include "../DataTypes/DTContacto.h"
#include "../DataTypes/DTGrupo.h"
#include "../DataTypes/DTConversacion.h"

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <unistd.h>
#include <list>

using namespace std;

class ControllerUsuario:public IControllerUsuario{
public:
    ControllerUsuario();
    ControllerUsuario(const ControllerUsuario& orig);
    
    virtual ~ControllerUsuario();
    
    //OPERACIONES DE ABRIR GUASAPFING
    bool ingresoNumero(string telefono);
    void ingresoUsuario(string telefono);
    DTConexion altaUsuario(DTUsuario *nuevoUsuario);
    
    //OPERACION DE CERRAR GUASAPFING
    void cerrar();
    
    //OPERACIONES DE AGREGAR CONTACTO
    DTContacto *mostrarContacto(string telefono);
    void agregarContacto(string telefono);
    list<DTContacto*>* listarContactos();
    
    //OPERACIONES DE ALTA GRUPO
    list<DTContacto*> *quitarContacto(string telefono, list<DTContacto*> *contactosElegidos);
    list<DTContacto*> *listarContactosRestantes(list<string> telefonosElegidos);
    list<DTContacto*> *agregarContacto(string telefono, list<DTContacto*> *contactosElegidos);
    void altaGrupo(DTGrupo *datosGrupo, list<DTContacto*> *contactosElegidos);
    
    //OPERACIONES DE MODIFICAR USUARIO
    void cambiarNombre(string nuevoNombre);
    void cambiarImagenDePerfil(string nuevaImagen);
    void cambiarDescripcion(string nuevaDescripcion);
    
    //OTRAS OPERACIONES
    void cargarDatosDePrueba();
    void modificarReloj(int dia, int mes, int anio, int horas, int minutos);
    void consultarReloj();
};

#endif

