#include "IControllerMensaje.h"
#include "ControllerMensaje.h"
#include "ColeccionUsuarios.h"

//CONSTRUCTORES

ControllerMensaje::ControllerMensaje() {
}

ControllerMensaje::ControllerMensaje(const ControllerMensaje& orig) {
}

//DESTRUCTOR

ControllerMensaje::~ControllerMensaje() {
}

//OPERACIONES DE ENVIAR MENSAJE

list<DTConversacion*> *ControllerMensaje::listarConversaciones(){
    Sesion *s;
    s = s->getInstancia();
    list<DTConversacion*> *listaConversaciones = new list<DTConversacion*>;
    listaConversaciones = s->obtenerDatosConversaciones();
    return listaConversaciones;
}

list<DTConversacion*> *ControllerMensaje::listarConversacionesArchivadas(){
    Sesion *s;
    s = s->getInstancia();
    list<DTConversacion*> *listaConversacionesArchivadas = new list<DTConversacion*>;
    listaConversacionesArchivadas = s->listarConversacionesArchivadas();
    return listaConversacionesArchivadas;
}

list<DTContacto*> *ControllerMensaje::listarContactos(){
    Sesion *s;
    s = s->getInstancia();
    list<DTContacto*> *listaContactos = new list<DTContacto*>;
    listaContactos = s->listarContactos();
    return listaContactos;
}

int ControllerMensaje::crearConversacion(string telefono){
    string telefonoEmisor;
    int idConversacionNueva;
    Conversacion *aux = new ConversacionSimple();
    Sesion *s;
    s = s->getInstancia();
    ColeccionUsuarios *usuarios;
    usuarios = usuarios->getInstancia();
    
    s->crearConversacion(telefono);
    telefonoEmisor = s->getTelefono();
    
    for(list<Usuario*>::iterator i = usuarios->getListaUsuarios()->begin(); i != usuarios->getListaUsuarios()->end(); i++){
        if((*i)->getTelefono() == telefono){
            idConversacionNueva = (*i)->crearConversacion(telefonoEmisor);
        }
    }
    
    cout << endl;
    cout << "¡Conversacion creada con exito!" << endl;
    sleep(1);
    
    aux->aumentarContador();
    
    return idConversacionNueva;
}

void ControllerMensaje::crearMensaje(DTMensaje* datosMensaje, int idConversacion){
    Sesion *s;
    s = s->getInstancia();
    ColeccionUsuarios *usuarios;
    usuarios = usuarios->getInstancia();
    Mensaje *m;
    Mensaje *aux;
    int idMensajeEnviado;
    string telefonoEmisor;
    list<string> telefonosParticipantes;
    
    idMensajeEnviado = s->crearMensaje(datosMensaje, idConversacion);
    telefonoEmisor = s->getTelefono();
    telefonosParticipantes = s->getTelefonosParticipantes(idConversacion);
    
    for(list<Usuario*>::iterator i = usuarios->getListaUsuarios()->begin(); i != usuarios->getListaUsuarios()->end(); i++){
        for(list<string>::iterator j = telefonosParticipantes.begin(); j != telefonosParticipantes.end(); j++){
            if((*i)->getTelefono() == (*j) && (*i)->getTelefono() != s->getUsuarioSesion()->getTelefono()){
                (*i)->agregarMensajeNuevo(idConversacion, idMensajeEnviado, telefonoEmisor);
            }
        }
    }
    aux->aumentarContador();
}

DTContacto *ControllerMensaje::listarConversaciones(string telefono){
    Sesion *s;
    s = s->getInstancia();
    DTContacto *datosContacto;
    datosContacto = s->getDatosContacto(telefono);
    return datosContacto;
}

bool ControllerMensaje::elegirConversacion(int idConversacion){
    Sesion *s;
    s = s->getInstancia();
    bool existe;
    existe = s->elegirConversacion(idConversacion);
    return existe;
}

//OPERACIONES DE VER MENSAJE

list<DTVisualizacion*> *ControllerMensaje::infoMensajeEnviado(int idMensaje, int idConversacion){
    Sesion *s;
    s = s->getInstancia();
    list<DTVisualizacion*> *listaVisualizaciones = new list<DTVisualizacion*>;
    listaVisualizaciones = s->infoMensajeEnviado(idMensaje, idConversacion);
    return listaVisualizaciones;
}

list<DTMensaje*> *ControllerMensaje::listarMensajes(int idConversacion){
    list<DTMensaje*> *listaMensajes = new list<DTMensaje*>;
    Sesion *s;
    s = s->getInstancia();
    listaMensajes = s->listarMensajes(idConversacion);
    return listaMensajes;
}

//OPERACIONES DE ELIMINAR MENSAJE

void ControllerMensaje::eliminarMensaje(int idMensaje){
    
    
}