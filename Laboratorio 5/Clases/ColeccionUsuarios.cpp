#include "ColeccionUsuarios.h"

ColeccionUsuarios *ColeccionUsuarios::instancia = NULL;

//CONSTRUCTOR

ColeccionUsuarios::ColeccionUsuarios() {
    usuarios = new list<Usuario*>;
}

//GETTERS

ColeccionUsuarios *ColeccionUsuarios::getInstancia(){
    if(instancia == NULL){
        instancia = new ColeccionUsuarios();
    }
    return instancia;
}

list<Usuario*> *ColeccionUsuarios::getListaUsuarios(){
    return usuarios;
}

//OTROS

void ColeccionUsuarios::agregarUsuario(Usuario *u){
    this->instancia->usuarios->push_back(u);
}

list<Usuario*>::iterator ColeccionUsuarios::borrarUsuario(list<Usuario*>::iterator i){
    list<Usuario*>::iterator nuevoI;
    nuevoI = this->usuarios->erase(i);
    return nuevoI;
}