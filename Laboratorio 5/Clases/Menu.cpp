#include "Menu.h"
#include "ColeccionGrupos.h"
#include "MensajeSimple.h"
#include "MultimediaVideo.h"
#include "ControllerMensajeFactory.h"

Menu *Menu::instancia = NULL;

Menu::Menu() {
}

Menu *Menu::getInstancia(){
    if(instancia == NULL){
        instancia = new Menu();
    }
    return instancia;
}

//OPERACIONES DE SISTEMA

void Menu::abrirGuasapFING(){
    string telefonoUsuario;
    string nombreUsuario;
    string imagenUsuario;
    string descripcionUsuario;
    string respuesta;
    bool termina = false;
    Sesion *sesion;
    sesion = sesion->getInstancia();
    ControllerUsuarioFactory *f;
    IControllerUsuario *i;
    i = f->getIControllerUsuario();
    DTUsuario *nuevoUsuario;
    
    do{
        system("clear");
        cout << " ____________________________ " << endl;
        cout << "|                            |" << endl;
        cout << "|     -ABRIR GUASAPFING-     |" << endl;
        cout << "|____________________________|" << endl;
        cout << "                              " << endl;
        cout << "    ¿Cuál es su teléfono?" << endl;
        cout << endl;
        cout << "-> ";
        std::getline(cin, telefonoUsuario);
        if(i->ingresoNumero(telefonoUsuario) == true){
            if(sesion->getUsuarioSesion() == NULL){
                i->ingresoUsuario(telefonoUsuario);
                termina = true;
                break;
            }
            else{
                if(sesion->getUsuarioSesion()->getTelefono() == telefonoUsuario){
                    system("clear");
                    cout << " ____________________________ " << endl;
                    cout << "|                            |" << endl;
                    cout << "|     -ABRIR GUASAPFING-     |" << endl;
                    cout << "|____________________________|" << endl;
                    cout << "                              " << endl;
                    cout << "ERROR: Este telefono ya tiene" << endl;
                    cout << "       sesión iniciada" << endl;
                    sleep(1);
                    termina = true;
                    break;
                }
                else{
                    system("clear");
                    cout << " ____________________________ " << endl;
                    cout << "|                            |" << endl;
                    cout << "|     -ABRIR GUASAPFING-     |" << endl;
                    cout << "|____________________________|" << endl;
                    cout << "                              " << endl;
                    cout << "  Ya hay una sesion iniciada" << endl;
                    cout << "¿Desea cerrar la sesion actual?" << endl;
                    cout << endl;
                    cout << " AYUDA: Responda Y para 'si'" << endl;
                    cout << "       o N para 'no'" << endl;
                    cout << endl;
                    cout << "-> ";
                    std::getline(cin, respuesta);
                    if (respuesta == "y" || respuesta == "Y"){
                        cerrarGuasapFING();
                        i->ingresoUsuario(telefonoUsuario);
                        termina = true;
                        break;
                    }
                    if(respuesta == "n" || respuesta == "N"){
                        termina = true;
                        break;
                    }
                    if(respuesta != "n" && respuesta != "N" && respuesta != "y" && respuesta != "Y"){
                        cout << endl;
                        cout << "   ERROR: tecla incorrecta," << endl;
                        cout << "      vuelva a comenzar" << endl;
                        sleep(1);
                        termina = true;
                        break;
                    }
                    termina = true;
                    break;
                }
            }
        }
        else{
            system("clear");
            cout << " ____________________________ " << endl;
            cout << "|                            |"<< endl;
            cout << "|     -ABRIR GUASAPFING-     |" << endl;
            cout << "|____________________________|" << endl;
            cout << "                              " << endl;
            cout << "El número " << telefonoUsuario << " no se está" << endl;
            cout << "  registrado en el sistema :(" << endl;
            cout << endl;
            cout << "    ¿Desea darse de alta?" << endl;
            cout << endl;
            cout << " AYUDA: Responda Y para 'si'" << endl;
            cout << "       o N para 'no'" << endl;
            cout << endl;
            cout << "-> ";
            std::getline(cin, respuesta);
            if(respuesta == "Y" || respuesta == "y"){
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|     -ABRIR GUASAPFING-     |" << endl;
                cout << "|____________________________|" << endl;
                cout << "                              " << endl;
                
                cout << "     ¿Cuál es su nombre? " << endl;
                cout << endl;
                cout << "-> ";
                std::getline(cin, nombreUsuario);
                cout << endl;
                
                cout << " ¿Cuál sera su foto de perfil?" << endl;
                cout << endl;
                cout << "-> ";
                std::getline(cin, imagenUsuario);
                cout << endl;
                
                cout << "  ¿Cuál sera su descripción?" << endl;
                cout << endl;
                cout << "-> ";
                std::getline(cin, descripcionUsuario);
                nuevoUsuario = new DTUsuario(telefonoUsuario, nombreUsuario, imagenUsuario, descripcionUsuario);
                i->altaUsuario(nuevoUsuario);
                
                if(sesion->getUsuarioSesion() == NULL){
                    i->ingresoUsuario(telefonoUsuario);
                }
                else{
                    system("clear");
                    cout << " ____________________________ " << endl;
                    cout << "|                            |" << endl;
                    cout << "|     -ABRIR GUASAPFING-     |" << endl;
                    cout << "|____________________________|" << endl;
                    cout << "                              " << endl;
                    cout << "  Ya hay una sesion iniciada" << endl;
                    cout << "¿Desea cerrar la sesion actual?" << endl;
                    cout << endl;
                    cout << " AYUDA: Responda Y para 'si'" << endl;
                    cout << "       o N para 'no'" << endl;
                    cout << endl;
                    cout << "-> ";
                    std::getline(cin, respuesta);
                    if (respuesta == "y" || respuesta == "Y"){
                        cerrarGuasapFING();
                        i->ingresoUsuario(telefonoUsuario);
                        termina = true;
                        break;
                    }
                    if(respuesta == "n" || respuesta == "N"){
                        termina = true;
                        break;
                    }
                    if(respuesta != "n" && respuesta != "N" && respuesta != "y" && respuesta != "Y"){
                        cout << endl;
                        cout << "   ERROR: tecla incorrecta," << endl;
                        cout << "      vuelva a comenzar" << endl;
                        sleep(1);
                        termina = true;
                        break;
                    }
                }
                termina = true;
                break;
            }
            if(respuesta == "n" || respuesta == "N"){
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|     -ABRIR GUASAPFING-     |" << endl;
                cout << "|____________________________|" << endl;
                cout << "                              " << endl;
                cout << "¿Quiere ingresar otro número?" << endl;
                cout << endl;
                cout << " AYUDA: Responda Y para 'si'" << endl;
                cout << "       o N para 'no'" << endl;
                cout << endl;
                cout << "-> ";
                std::getline(cin, respuesta);
                if (respuesta == "N" || respuesta == "n"){
                    termina = true;
                    break;
                }
                if (respuesta != "N" && respuesta != "n" && respuesta != "Y" && respuesta != "y"){
                    cout << endl;
                    cout << "ERROR: tecla incorrecta," << endl;
                    cout << "   vuelva a comenzar" << endl;
                    sleep(1);
                    termina = true;
                    break;
                }
            }
            if(respuesta != "Y" && respuesta != "N" && respuesta != "n" && respuesta != "y"){
                cout << endl;
                cout << "   ERROR: tecla incorrecta," << endl;
                cout << "      vuelva a comenzar" << endl;
                sleep(1);
                termina = true;
                break;
            }
        }
    }while(termina != true);
}

void Menu::cerrarGuasapFING(){
    IControllerUsuario *i;
    ControllerUsuarioFactory *f;
    i = f->getIControllerUsuario();
    i->cerrar();
}

void Menu::agregarContacto() {
    Sesion *s;
    s = s->getInstancia();
    IControllerUsuario *i;
    ControllerUsuarioFactory *f = new ControllerUsuarioFactory;
    i = f->getIControllerUsuario();
    bool agrega = true;
    char opcion;
    string telefono_agregar;list<DTContacto*> *contactos;
    contactos = i->listarContactos();
    DTContacto *aux;
    cout << "Desea cancelar? Y/N: " <<endl;
    cin >> opcion;
    if (opcion == 'Y' || opcion == 'y') {
        agrega == false;
    }
    while(agrega == true) {
        cout<< "Desea agregar un Contacto? Y/N: ";
        cin >> opcion;
        if(opcion == 'Y' || opcion =='y') {
            for(list<DTContacto*>:: iterator c = contactos->begin(); c != contactos->end(); c++) {
                cout<<"Nombre: " << (*c)->getNombre() << endl;
                cout<<"Telefono: "<< (*c)->getTelefono()<< endl <<endl;
            }
            cout <<"Ingrese el numero de telefono del usuario a agregar: ";
            cin >> telefono_agregar;
            aux = i->mostrarContacto(telefono_agregar);
            if(aux != NULL) {
                cout <<"Nombre: "<< (*aux).getNombre()<< endl;
                cout <<"Telefono: "<< (*aux).getTelefono()<<endl;
                cout <<endl <<"Confirma agregar al usuario " << (*aux).getNombre() <<" ? Y/N: ";
                cin >> opcion;
                if(opcion == 'Y' || opcion =='y') {
                i->agregarContacto(telefono_agregar);
                }
                else if(opcion =='N' || opcion == 'n') {
                    cout << "Usuario no agregado como contacto" << endl;
                }
            }
        }
        else if (opcion == 'N' || opcion == 'n') {
            agrega = false;
        }
    }
}

void Menu::modificarUsuario(){
    string nuevoNombre;
    string nuevaImagen;
    string nuevaDescripcion;
    string respuesta;
    bool termina = false;
    Sesion *sesion;
    sesion = sesion->getInstancia();
    ControllerUsuarioFactory *f;
    IControllerUsuario *i;
    i = f->getIControllerUsuario();
    Usuario *usuarioSesion = sesion->getUsuarioSesion();
    
    do{
        system("clear");
        cout << " ____________________________ " << endl;
        cout << "|                            |" << endl;
        cout << "|     -MODIFICAR USUARIO-    |" << endl;
        cout << "|____________________________|" << endl;
        cout << "                              " << endl;
        cout << "  ¿Desea cambiar su nombre?   " << endl;
        cout << endl;
        cout << " AYUDA: Responda Y para 'si'" << endl;
        cout << "       o N para 'no'" << endl;
        cout << endl;
        cout << "-> ";
        std::getline(cin, respuesta);
        if(respuesta == "y" || respuesta == "Y"){
            system("clear");
            cout << " ____________________________ " << endl;
            cout << "|                            |" << endl;
            cout << "|     -MODIFICAR NOMBRE-     |" << endl;
            cout << "|____________________________|" << endl;
            cout << "                              " << endl;
            cout << endl;
            cout << "-> ";
            std::getline(cin, nuevoNombre);
            i->cambiarNombre(nuevoNombre);
        }
        system("clear");
        cout << " ____________________________ " << endl;
        cout << "|                            |" << endl;
        cout << "|     -MODIFICAR USUARIO-    |" << endl;
        cout << "|____________________________|" << endl;
        cout << "                              " << endl;
        cout << "¿Desea cambiar su foto de perfil?" << endl;
        cout << endl;
        cout << " AYUDA: Responda Y para 'si'" << endl;
        cout << "       o N para 'no'" << endl;
        cout << endl;
        cout << "-> ";
        std::getline(cin, respuesta);
        if(respuesta == "y" || respuesta == "Y"){
            system("clear");
            cout << " ____________________________ " << endl;
            cout << "|                            |" << endl;
            cout << "| -MODIFICAR FOTO DE PERFIL- |" << endl;
            cout << "|____________________________|" << endl;
            cout << "                              " << endl;
            cout << endl;
            cout << "-> ";
            std::getline(cin, nuevaImagen);
            i->cambiarImagenDePerfil(nuevaImagen);
        }
        system("clear");
        cout << " ____________________________ " << endl;
        cout << "|                            |" << endl;
        cout << "|     -MODIFICAR USUARIO-    |" << endl;
        cout << "|____________________________|" << endl;
        cout << "                              " << endl;
        cout << "¿Desea cambiar su descripcion?" << endl;
        cout << endl;
        cout << " AYUDA: Responda Y para 'si'" << endl;
        cout << "       o N para 'no'" << endl;
        cout << endl;
        cout << "-> ";
        std::getline(cin, respuesta);
        if(respuesta == "y" || respuesta == "Y"){
            system("clear");
            cout << " ____________________________ " << endl;
            cout << "|                            |" << endl;
            cout << "|  -MODIFICAR DESCRIPCION-   |" << endl;
            cout << "|____________________________|" << endl;
            cout << "                              " << endl;
            cout << endl;
            cout << "-> ";
            std::getline(cin, nuevaDescripcion);
            i->cambiarDescripcion(nuevaDescripcion);
        }
        termina = true;

    }while(termina != true);
}

void Menu::altaGrupo(){
    int opcion;
    bool termina = false;
    bool cancelar = false;
    string usuarioElegido;
    string respuesta;
    string nombreGrupo;
    string imagenGrupo;
    list<DTContacto*> *restantes;
    list<DTContacto*> *elegidos = new list<DTContacto*>;
    list<string> telefonosElegidos;
    DTGrupo *datosGrupo;
    Sesion *s;
    s = s->getInstancia();
    IControllerUsuario *i;
    ControllerUsuarioFactory *f;
    i = f->getIControllerUsuario();

    ColeccionGrupos *grupos;
    grupos = grupos->getInstancia();
    
    if(s->getUsuarioSesion()->getContactos()->size() != 0){
        restantes = i->listarContactosRestantes(telefonosElegidos);
        do{
            system("clear");
            cout << " ____________________________ " << endl;
            cout << "|                            |" << endl;
            cout << "|        -ALTA GRUPO-        |" << endl;
            cout << "|____________________________|" << endl;
            cout << "|                            |" << endl;
            cout << "|    1- AGREGAR MIEMBRO      |" << endl;
            cout << "|    2- QUITAR MIEMBRO       |" << endl;
            cout << "|____________________________|" << endl;
            cout << "-> ";
            cin >> opcion;
            cin.get();
            if(opcion == 1){
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|        -ALTA GRUPO-        |" << endl;
                cout << "|____________________________|" << endl;
                cout << "|                            |" << endl;
                cout << "|    1-                      |" << endl;
                cout << "|    2- QUITAR MIEMBRO       |" << endl;
                cout << "|____________________________|" << endl;
                usleep(400000);
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|        -ALTA GRUPO-        |" << endl;
                cout << "|____________________________|" << endl;
                cout << "|                            |" << endl;
                cout << "|    1- AGREGAR MIEMBRO      |" << endl;
                cout << "|    2- QUITAR MIEMBRO       |" << endl;
                cout << "|____________________________|" << endl;
                usleep(400000);
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|      -AGREGAR MIEMBRO-     |" << endl;
                cout << "|____________________________|" << endl;
                cout << "                              " << endl;
                
                if(elegidos->size() != 0){
                    cout << "  -CONTACTOS ELEGIDOS-  " << endl;
                    cout << endl;
                    for(list<DTContacto*>::iterator i = elegidos->begin(); i != elegidos->end(); i++){
                        cout << "  -NOMBRE: " << (*i)->getNombre() << endl;
                        cout << "  -TELEFONO: " << (*i)->getTelefono() << endl;
                        cout << endl;
                    }
                }
                else{
                    cout << "  -CONTACTOS ELEGIDOS-  " << endl;
                    cout << "          -          " << endl;
                }
                cout << endl;

                cout << "  -CONTACTOS RESTANTES-  " << endl;
                cout << endl;
                for(list<DTContacto*>::iterator j = restantes->begin(); j != restantes->end(); j++){
                    cout << "  -NOMBRE: " << (*j)->getNombre() << endl;;
                    cout << "  -TELEFONO: " << (*j)->getTelefono() << endl;
                    cout << endl;
                }
                cout << "TELEFONO -> ";

                std::getline(cin, usuarioElegido);
                elegidos = i->agregarContacto(usuarioElegido, elegidos);
                telefonosElegidos.push_back(usuarioElegido);
                restantes = i->listarContactosRestantes(telefonosElegidos);
            }
            if(opcion == 2){
                if(elegidos != 0){
                    system("clear");
                    cout << " ____________________________ " << endl;
                    cout << "|                            |" << endl;
                    cout << "|        -ALTA GRUPO-        |" << endl;
                    cout << "|____________________________|" << endl;
                    cout << "|                            |" << endl;
                    cout << "|    1- AGREGAR MIEMBRO      |" << endl;
                    cout << "|    2-                      |" << endl;
                    cout << "|____________________________|" << endl;
                    usleep(400000);
                    system("clear");
                    cout << " ____________________________ " << endl;
                    cout << "|                            |" << endl;
                    cout << "|        -ALTA GRUPO-        |" << endl;
                    cout << "|____________________________|" << endl;
                    cout << "|                            |" << endl;
                    cout << "|    1- AGREGAR MIEMBRO      |" << endl;
                    cout << "|    2- QUITAR MIEMBRO       |" << endl;
                    cout << "|____________________________|" << endl;
                    usleep(400000);
                    system("clear");
                    cout << " ___________________________ " << endl;
                    cout << "|                           |" << endl;
                    cout << "|      -QUITAR MIEMBRO-     |" << endl;
                    cout << "|___________________________|" << endl;
                    cout << "                             " << endl;

                    cout << "  -CONTACTOS ELEGIDOS-  " << endl;
                    cout << endl;
                    for(list<DTContacto*>::iterator k = elegidos->begin(); k != elegidos->end(); k++){
                        cout << "  -NOMBRE: " << (*k)->getNombre() << endl;
                        cout << "  -TELEFONO: " << (*k)->getTelefono() << endl;
                        cout << endl;
                    }

                    cout << "  -CONTACTOS RESTANTES-  " << endl;
                    cout << endl;
                    for(list<DTContacto*>::iterator l = restantes->begin(); l != restantes->end(); l++){
                        cout << "  -NOMBRE: " << (*l)->getNombre() << endl;;
                        cout << "  -TELEFONO: " << (*l)->getTelefono() << endl;
                        cout << endl;
                    }

                    cout << "TELEFONO -> ";

                    std::getline(cin, usuarioElegido);
                    elegidos = i->quitarContacto(usuarioElegido, elegidos);
                    telefonosElegidos.remove(usuarioElegido);
                    restantes = i->listarContactosRestantes(telefonosElegidos);
                }
                else{
                    cout << "ERROR: Debe elegir al menos" << endl;
                    cout << "        un usuario" << endl;
                    sleep(1);
                }
            }

            while(opcion != 3){
                system("clear");
                cout << " ____________________________" << endl;
                cout << "|                            |" << endl;
                cout << "|        -ALTA GRUPO-        |" << endl;
                cout << "|____________________________|" << endl;
                cout << endl;
                cout << "¿Quiere continuar editando la" << endl;
                cout << "   lista de participantes?" << endl;
                cout << endl;
                std::getline(cin, respuesta);
                if(respuesta == "Y" || respuesta == "y"){
                    opcion = 3;
                    break;
                }
                else{
                    if(respuesta == "N" || respuesta == "n"){
                        if(elegidos->size() == 0){
                            system("clear");
                            cout << " ____________________________" << endl;
                            cout << "|                            |" << endl;
                            cout << "|        -ALTA GRUPO-        |" << endl;
                            cout << "|____________________________|" << endl;
                            cout << endl;
                            cout << "ERROR: Debe elegir al menos" << endl;
                            cout << "        un usuario" << endl;
                            cout << endl;
                            cout << "¿Quiere continuar agregando?" << endl;
                            cout << endl;
                            std::getline(cin, respuesta);
                            while(opcion != 3){
                                if(respuesta == "Y" || respuesta == "y"){
                                    opcion = 3;
                                    break;
                                }else{
                                    if(respuesta == "N" || respuesta == "n"){
                                        cancelar = true;
                                        termina = true;
                                        opcion = 3;
                                        break;
                                    }
                                }
                            }
                        }
                        else{
                            termina = true;
                            opcion = 3;
                            break;
                        }
                    }
                }
            }
        }while(termina != true);

        if(cancelar != true){
            system("clear");
            cout << " ____________________________" << endl;
            cout << "|                            |" << endl;
            cout << "|        -ALTA GRUPO-        |" << endl;
            cout << "|____________________________|" << endl;
            cout << endl;
            cout << "¿Cual sera el nombre del grupo?" << endl;
            cout << "-> ";
            std::getline(cin, nombreGrupo);
            system("clear");
            cout << " ____________________________" << endl;
            cout << "|                            |" << endl;
            cout << "|        -ALTA GRUPO-        |" << endl;
            cout << "|____________________________|" << endl;
            cout << endl;
            cout << "¿Cual sera la imagen del grupo?" << endl;
            cout << "-> ";
            std::getline(cin, imagenGrupo);
            datosGrupo = new DTGrupo(nombreGrupo, imagenGrupo);
            i->altaGrupo(datosGrupo, elegidos);
        }
    }
    else{
        system("clear");
        cout << " ____________________________" << endl;
        cout << "|                            |" << endl;
        cout << "|        -ALTA GRUPO-        |" << endl;
        cout << "|____________________________|" << endl;
        cout << endl;
        cout << "ERROR: Usted no posee contactos" << endl;
        sleep(1);
    }
}

void Menu::enviarMensaje(){
    int cantidadArchivadas = 0;
    int cantidadMensajes = 0;
    int opcion;
    int idConversacionElegida;
    int minutosDuracion;
    int segundosDuracion;
    float tamanioElegido;
    bool termina = false;
    bool encontro = false;
    bool eligio = false;
    bool existeConversacion = false;
    string respuesta;
    string telefonoElegido;
    string nombreElegido;
    string textoMensaje;
    string urlElegido;
    string formatoElegido;
    string descripcionElegida;
    Sesion *s;
    s = s->getInstancia();
    Reloj *r;
    r = r->getInstancia();
    IControllerMensaje *i;
    ControllerMensajeFactory *f;
    i = f->getIControllerMensaje();
    DTMensaje *mensajeNuevo;
    DTUsuario *emisorVacio;
    list<DTUsuario*> *receptoresVacios = new list<DTUsuario*>;
    list<DTConversacion*> *listaConversaciones = new list<DTConversacion*>;
    list<DTMensaje*> *listaMensajes = new list<DTMensaje*>;
    list<DTConversacion*> *listaConversacionesArchivadas = new list<DTConversacion*>;
    list<DTContacto*> *listaContactos = new list<DTContacto*>;
    list<DTVisualizacion*> *visualizacionesVacias = new list<DTVisualizacion*>;
    
    if(s->getUsuarioSesion()->getContactos()->size() != 0){
        system("clear");
        cout << " ____________________________ " << endl;
        cout << "|                            |" << endl;
        cout << "|      -ENVIAR MENSAJE-      |" << endl;
        cout << "|____________________________|" << endl;
        cout << "                              " << endl;
        
        listaConversaciones = i->listarConversaciones();
        for(list<DTConversacion*>::iterator j = listaConversaciones->begin(); j != listaConversaciones->end(); j++){
            if((*j)->getEstado() == true){
                try{
                    if((&dynamic_cast<DTConversacionGrupo&>(*(*j))) != NULL){
                        cout << "CONVERSACION " << (*j)->getID() << ": ";
                        cout << (&dynamic_cast<DTConversacionGrupo&>(*(*j)))->getNombre();
                        cout << endl;
                        cout << endl;
                    }
                }
                catch (bad_cast &noEsGrupo){
                    cout << "CONVERSACION " << (*j)->getID() << ": ";
                    cout << (&dynamic_cast<DTConversacionSimple&>(*(*j)))->getNombre() << endl;
                    cout << "                "<<(&dynamic_cast<DTConversacionSimple&>(*(*j)))->getTelefono() << endl;
                    cout << endl;
                }
            }
            else{
                cantidadArchivadas ++;
            }
        }
        cout << endl;
        cout << "       ARCHIVADAS: " << cantidadArchivadas << endl;
        cout << endl;
        cout << "  -ENTER PARA CONTINUAR-" << endl;
        cin.get();
        
        while(termina == false){
            system("clear");
            cout << " ____________________________ " << endl;
            cout << "|                            |" << endl;
            cout << "|      -ENVIAR MENSAJE-      |" << endl;
            cout << "|____________________________|" << endl;
            cout << "|                            |" << endl;
            cout << "|   1- ELEGIR CONVERACION    |" << endl;
            cout << "|   2- VER ARCHIVADAS        |" << endl;
            cout << "|   3- NUEVA CONVERSACION    |" << endl;
            cout << "|____________________________|" << endl;
            cout << "-> ";
            cin >> opcion;
            if(opcion == 1){
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|      -ENVIAR MENSAJE-      |" << endl;
                cout << "|____________________________|" << endl;
                cout << "|                            |" << endl;
                cout << "|   1-                       |" << endl;
                cout << "|   2- VER ARCHIVADAS        |" << endl;
                cout << "|   3- NUEVA CONVERSACION    |" << endl;
                cout << "|____________________________|" << endl;
                usleep(400000);
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|      -ENVIAR MENSAJE-      |" << endl;
                cout << "|____________________________|" << endl;
                cout << "|                            |" << endl;
                cout << "|   1- ELEGIR CONVERSACION   |" << endl;
                cout << "|   2- VER ARCHIVADAS        |" << endl;
                cout << "|   3- NUEVA CONVERSACION    |" << endl;
                cout << "|____________________________|" << endl;
                usleep(400000);
                while(existeConversacion == false){
                    system("clear");
                    cout << " ____________________________ " << endl;
                    cout << "|                            |" << endl;
                    cout << "|      -ENVIAR MENSAJE-      |" << endl;
                    cout << "|____________________________|" << endl;
                    cout << "                              " << endl;
                    cout << "-Ingrese ID: ";
                    cin >> idConversacionElegida;
                    existeConversacion = i->elegirConversacion(idConversacionElegida);
                    if(existeConversacion == false){
                        cout << "ERROR: La conversacion elegida" << endl;
                        cout << " no se encuentra en su lista" << endl;
                        sleep(1);
                    }
                }existeConversacion = false;
                eligio = true;
                termina = true;
                break;
            }
            if(opcion == 2){
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|      -ENVIAR MENSAJE-      |" << endl;
                cout << "|____________________________|" << endl;
                cout << "|                            |" << endl;
                cout << "|   1- ELEGIR CONVERSACION   |" << endl;
                cout << "|   2-                       |" << endl;
                cout << "|   3- NUEVA CONVERSACION    |" << endl;
                cout << "|____________________________|" << endl;
                usleep(400000);
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|      -ENVIAR MENSAJE-      |" << endl;
                cout << "|____________________________|" << endl;
                cout << "|                            |" << endl;
                cout << "|   1- ELEGIR CONVERSACION   |" << endl;
                cout << "|   2- VER ARCHIVADAS        |" << endl;
                cout << "|   3- NUEVA CONVERSACION    |" << endl;
                cout << "|____________________________|" << endl;
                usleep(400000);
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|      -ENVIAR MENSAJE-      |" << endl;
                cout << "|____________________________|" << endl;
                cout << "                              " << endl;
                
                listaConversacionesArchivadas = i->listarConversacionesArchivadas();
                if(listaConversacionesArchivadas->size() != 0){
                    for(list<DTConversacion*>::iterator l = listaConversacionesArchivadas->begin(); l != listaConversacionesArchivadas->end(); l++){
                        try{
                            if((&dynamic_cast<DTConversacionGrupo&> (*(*l))) != NULL){
                                cout << "CONVERSACION " << (*l)->getID() << ":";
                                cout << (&dynamic_cast<DTConversacionGrupo&> (*(*l)))->getNombre() << endl;
                                cout << endl;
                            }
                        }
                        catch(bad_cast &noEsGrupo){
                            cout << "CONVERSACION " << (*l)->getID() << ":";
                            cout << (&dynamic_cast<DTConversacionSimple&> (*(*l)))->getNombre() << endl;
                            cout << "                " << (&dynamic_cast<DTConversacionSimple&> (*(*l)))->getTelefono();
                        }
                    }
                    cout << endl;
                    cout << "  -ENTER PARA CONTINUAR-" << endl;
                    cin.get();
                    while(existeConversacion == false){
                        system("clear");
                        cout << " ____________________________ " << endl;
                        cout << "|                            |" << endl;
                        cout << "|      -ENVIAR MENSAJE-      |" << endl;
                        cout << "|____________________________|" << endl;
                        cout << "                              " << endl;
                        cout << "-Ingrese ID: ";
                        cin >> idConversacionElegida;
                        existeConversacion = i->elegirConversacion(idConversacionElegida);
                        if(existeConversacion == false){
                            cout << "ERROR: La conversacion elegida" << endl;
                            cout << " no se encuentra en su lista" << endl;
                            sleep(1);
                        }
                    }existeConversacion = false;
                    eligio = true;
                }
                else{
                    cout << "    ERROR: Usted no posee " << endl;
                    cout << "  conversaciones archivadas" << endl;
                    eligio = false;
                    sleep(1);
                }
                termina = true;
                break;
            }
            if(opcion == 3){
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|      -ENVIAR MENSAJE-      |" << endl;
                cout << "|____________________________|" << endl;
                cout << "|                            |" << endl;
                cout << "|   1- ELEGIR CONVERSACION   |" << endl;
                cout << "|   2- VER ARCHIVADAS        |" << endl;
                cout << "|   3-                       |" << endl;
                cout << "|____________________________|" << endl;
                usleep(400000);
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|      -ENVIAR MENSAJE-      |" << endl;
                cout << "|____________________________|" << endl;
                cout << "|                            |" << endl;
                cout << "|   1- ELEGIR CONVERSACION   |" << endl;
                cout << "|   2- VER ARCHIVADAS        |" << endl;
                cout << "|   3- NUEVA CONVERSACION    |" << endl;
                cout << "|____________________________|" << endl;
                usleep(400000);
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|      -ENVIAR MENSAJE-      |" << endl;
                cout << "|____________________________|" << endl;
                cout << "                              " << endl;
                
                listaContactos = i->listarContactos();
                for(list<DTContacto*>::iterator m = listaContactos->begin(); m != listaContactos->end(); m++){
                    cout << "-" << (*m)->getNombre() << ": ";
                    cout << (*m)->getTelefono() << endl;
                    cout << endl;
                }
                cout << "-Ingrese telefono: ";
                cin.get();
                std::getline(cin, telefonoElegido);
                idConversacionElegida = i->crearConversacion(telefonoElegido);
                eligio = true;
                termina = true;
                break;
            }    
        }termina = false;
        if(eligio == true){
            while(termina == false){
                system("clear");
                cout << " ___________________________ " << endl;
                cout << "|                           |" << endl;
                cout << "|     -TIPO DE MENSAJE-     |" << endl;
                cout << "|___________________________|" << endl;
                cout << "|                           |" << endl;
                cout << "|   1- MENSAJE SIMPLE       |" << endl;
                cout << "|   2- MENSAJE CONTACTO     |" << endl;
                cout << "|   3- VIDEO MULTIMEDIA     |" << endl;
                cout << "|   4- IMAGEN MULTIMEDIA    |" << endl;
                cout << "|___________________________|" << endl;
                cout << "-> ";
                cin >> opcion;
                if(opcion == 1){
                    system("clear");
                    cout << " ___________________________ " << endl;
                    cout << "|                           |" << endl;
                    cout << "|     -TIPO DE MENSAJE-     |" << endl;
                    cout << "|___________________________|" << endl;
                    cout << "|                           |" << endl;
                    cout << "|   1-                      |" << endl;
                    cout << "|   2- MENSAJE CONTACTO     |" << endl;
                    cout << "|   3- VIDEO MULTIMEDIA     |" << endl;
                    cout << "|   4- IMAGEN MULTIMEDIA    |" << endl;
                    cout << "|___________________________|" << endl;
                    usleep(400000);
                    system("clear");
                    cout << " ___________________________ " << endl;
                    cout << "|                           |" << endl;
                    cout << "|     -TIPO DE MENSAJE-     |" << endl;
                    cout << "|___________________________|" << endl;
                    cout << "|                           |" << endl;
                    cout << "|   1- MENSAJE SIMPLE       |" << endl;
                    cout << "|   2- MENSAJE CONTACTO     |" << endl;
                    cout << "|   3- VIDEO MULTIMEDIA     |" << endl;
                    cout << "|   4- IMAGEN MULTIMEDIA    |" << endl;
                    cout << "|___________________________|" << endl;
                    usleep(400000);
                    system("clear");

                    cout << " ____________________________ " << endl;
                    cout << "|                            |" << endl;
                    cout << "|      -MENSAJE SIMPLE-      |" << endl;
                    cout << "|____________________________|" << endl;
                    cout << "                              " << endl;
                    cout << endl;
                    cout << "Texto del mensaje: ";
                    cin.get();
                    std::getline(cin, textoMensaje);
                    mensajeNuevo = new DTMensajeSimple(textoMensaje, 0, r->getFecha(), r->getHora(), emisorVacio, receptoresVacios, visualizacionesVacias);
                    i->crearMensaje(mensajeNuevo, idConversacionElegida);
                    termina = true;
                    break;
                }
                if(opcion == 2){
                    system("clear");
                    cout << " ___________________________ " << endl;
                    cout << "|                           |" << endl;
                    cout << "|     -TIPO DE MENSAJE-     |" << endl;
                    cout << "|___________________________|" << endl;
                    cout << "|                           |" << endl;
                    cout << "|   1- MENSAJE SIMPLE       |" << endl;
                    cout << "|   2-                      |" << endl;
                    cout << "|   3- VIDEO MULTIMEDIA     |" << endl;
                    cout << "|   4- IMAGEN MULTIMEDIA    |" << endl;
                    cout << "|___________________________|" << endl;
                    usleep(400000);
                    system("clear");
                    cout << " ___________________________ " << endl;
                    cout << "|                           |" << endl;
                    cout << "|     -TIPO DE MENSAJE-     |" << endl;
                    cout << "|___________________________|" << endl;
                    cout << "|                           |" << endl;
                    cout << "|   1- MENSAJE SIMPLE       |" << endl;
                    cout << "|   2- MENSAJE CONTACTO     |" << endl;
                    cout << "|   3- VIDEO MULTIMEDIA     |" << endl;
                    cout << "|   4- IMAGEN MULTIMEDIA    |" << endl;
                    cout << "|___________________________|" << endl;
                    usleep(400000);
                    system("clear");

                    cout << " ____________________________ " << endl;
                    cout << "|                            |" << endl;
                    cout << "|     -MENSAJE CONTACTO-     |" << endl;
                    cout << "|____________________________|" << endl;
                    cout << "                              " << endl;
                    cout << endl;
                    listaContactos = i->listarContactos();
                    for(list<DTContacto*>::iterator m = listaContactos->begin(); m != listaContactos->end(); m++){
                        cout << "-" << (*m)->getNombre() << ": ";
                        cout << (*m)->getTelefono() << endl;
                        cout << endl;
                    }
                    while(encontro != true){
                        cout << "-Ingrese telefono: ";
                        cin.get();
                        std::getline(cin, telefonoElegido);
                        for(list<DTContacto*>::iterator n = listaContactos->begin(); n != listaContactos->end(); n++){
                            if((*n)->getTelefono() == telefonoElegido){
                                nombreElegido = (*n)->getNombre();
                                encontro = true;
                                break;
                            }
                        }
                        if(encontro == false){
                            cout << "ERROR: Usted no posee este contacto" << endl;
                            sleep(1);
                        }
                    }
                    mensajeNuevo = new DTMensajeContacto(nombreElegido, telefonoElegido, 0, r->getFecha(), r->getHora(), emisorVacio, receptoresVacios, visualizacionesVacias);
                    i->crearMensaje(mensajeNuevo, idConversacionElegida);
                    termina = true;
                    break;
                }
                if(opcion == 3){
                     system("clear");
                    cout << " ___________________________ " << endl;
                    cout << "|                           |" << endl;
                    cout << "|     -TIPO DE MENSAJE-     |" << endl;
                    cout << "|___________________________|" << endl;
                    cout << "|                           |" << endl;
                    cout << "|   1- MENSAJE SIMPLE       |" << endl;
                    cout << "|   2- MENSAJE CONTACTO     |" << endl;
                    cout << "|   3-                      |" << endl;
                    cout << "|   4- IMAGEN MULTIMEDIA    |" << endl;
                    cout << "|___________________________|" << endl;
                    usleep(400000);
                    system("clear");
                    cout << " ___________________________ " << endl;
                    cout << "|                           |" << endl;
                    cout << "|     -TIPO DE MENSAJE-     |" << endl;
                    cout << "|___________________________|" << endl;
                    cout << "|                           |" << endl;
                    cout << "|   1- MENSAJE SIMPLE       |" << endl;
                    cout << "|   2- MENSAJE CONTACTO     |" << endl;
                    cout << "|   3- VIDEO MULTIMEDIA     |" << endl;
                    cout << "|   4- IMAGEN MULTIMEDIA    |" << endl;
                    cout << "|___________________________|" << endl;
                    usleep(400000);
                    system("clear");
                    
                    cout << " ____________________________ " << endl;
                    cout << "|                            |" << endl;
                    cout << "|     -VIDEO MULTIMEDIA-     |" << endl;
                    cout << "|____________________________|" << endl;
                    cout << "                              " << endl;
                    
                    cin.get();
                    cout << "-Ingrese URL: ";
                    std::getline(cin, urlElegido);
                    cout << "-Ingrese minutos: ";
                    cin >> minutosDuracion;
                    cout << "-Ingrese segundos: ";
                    cin >> segundosDuracion;
                    
                    mensajeNuevo = new DTMultimediaVideo(urlElegido, minutosDuracion, segundosDuracion, 0, r->getFecha(), r->getHora(), emisorVacio, receptoresVacios, visualizacionesVacias);
                    i->crearMensaje(mensajeNuevo, idConversacionElegida);
                    termina = true;
                    break;
                }
                if(opcion == 4){
                    system("clear");
                    cout << " ___________________________ " << endl;
                    cout << "|                           |" << endl;
                    cout << "|     -TIPO DE MENSAJE-     |" << endl;
                    cout << "|___________________________|" << endl;
                    cout << "|                           |" << endl;
                    cout << "|   1- MENSAJE SIMPLE       |" << endl;
                    cout << "|   2- MENSAJE CONTACTO     |" << endl;
                    cout << "|   3- VIDEO MULTIMEDIA     |" << endl;
                    cout << "|   4-                      |" << endl;
                    cout << "|___________________________|" << endl;
                    usleep(400000);
                    system("clear");
                    cout << " ___________________________ " << endl;
                    cout << "|                           |" << endl;
                    cout << "|     -TIPO DE MENSAJE-     |" << endl;
                    cout << "|___________________________|" << endl;
                    cout << "|                           |" << endl;
                    cout << "|   1- MENSAJE SIMPLE       |" << endl;
                    cout << "|   2- MENSAJE CONTACTO     |" << endl;
                    cout << "|   3- VIDEO MULTIMEDIA     |" << endl;
                    cout << "|   4- IMAGEN MULTIMEDIA    |" << endl;
                    cout << "|___________________________|" << endl;
                    usleep(400000);
                    system("clear");
                    cout << " ____________________________ " << endl;
                    cout << "|                            |" << endl;
                    cout << "|     -VIDEO MULTIMEDIA-     |" << endl;
                    cout << "|____________________________|" << endl;
                    cout << "                              " << endl;
                    cin.get();
                    cout << "-Ingrese URL: ";
                    std::getline(cin, urlElegido);
                    cout << "-Ingrese formato: ";
                    std::getline(cin, formatoElegido);
                    cout << "-Ingrese tamaño: ";
                    cin >> tamanioElegido;
                    cout << "¿Quiere ingresar una descripcion?" << endl;
                    while(respuesta != "º"){
                        std::getline(cin, respuesta);
                        if(respuesta == "Y" || respuesta == "y"){
                            cout << "-Ingrese descripcion: ";
                            std::getline(cin, descripcionElegida);
                            respuesta = "º";
                            break;
                        }
                        if(respuesta == "N" || respuesta == "n"){
                            respuesta = "º";
                            break;
                        }
                    }
                    mensajeNuevo = new DTMultimediaImagen(urlElegido, formatoElegido, tamanioElegido, descripcionElegida, 0, r->getFecha(), r->getHora(), emisorVacio, receptoresVacios, visualizacionesVacias);
                    i->crearMensaje(mensajeNuevo, idConversacionElegida);
                    termina = true;
                    break;
                }
            }
        }
    }
    else{
        cout << "ERROR: No pose contactos" << endl;
        sleep(1);
    }
}

void Menu::verMensaje(){
    int opcion;
    int idConversacionElegida;
    int contadorVistos = 0;
    int cantidadArchivadas = 0;
    bool termina = false;
    bool existeConversacion = false;
    bool eligio = true;
    Sesion *s;
    s = s->getInstancia();
    ControllerMensajeFactory *f;
    IControllerMensaje *i;
    i = f->getIControllerMensaje();
    list<DTMensaje*> *listaMensajes = new list<DTMensaje*>;
    list<DTConversacion*> *listaConversaciones = new list<DTConversacion*>;
    list<DTConversacion*> *listaConversacionesArchivadas = new list<DTConversacion*>;
    
    system("clear");
    cout << " ____________________________ " << endl;
    cout << "|                            |" << endl;
    cout << "|       -VER MENSAJES-       |" << endl;
    cout << "|____________________________|" << endl;
    cout << "                              " << endl;
    
    if(s->getUsuarioSesion()->getContactos()->size() != 0){
        listaConversaciones = i->listarConversaciones();
        for(list<DTConversacion*>::iterator j = listaConversaciones->begin(); j != listaConversaciones->end(); j++){
            if((*j)->getEstado() == true){
                try{
                    if((&dynamic_cast<DTConversacionGrupo&>(*(*j))) != NULL){
                        cout << "CONVERSACION " << (*j)->getID() << ": ";
                        cout << (&dynamic_cast<DTConversacionGrupo&>(*(*j)))->getNombre();
                        cout << endl;
                        cout << endl;
                    }
                }
                catch (bad_cast &noEsGrupo){
                    cout << "CONVERSACION " << (*j)->getID() << ": ";
                    cout << (&dynamic_cast<DTConversacionSimple&>(*(*j)))->getNombre() << endl;
                    cout << "                "<<(&dynamic_cast<DTConversacionSimple&>(*(*j)))->getTelefono() << endl;
                    cout << endl;
                }
            }
            else{
                cantidadArchivadas ++;
            }
        }
        cout << endl;
        cout << "       ARCHIVADAS: " << cantidadArchivadas << endl;
        cout << endl;
        cout << "  -ENTER PARA CONTINUAR-" << endl;
        cin.get();
    }
    else{
        cout << "ERROR: No pose contactos" << endl;
        sleep(1);
    }
    while(termina == false){
        system("clear");
        cout << " ____________________________ " << endl;
        cout << "|                            |" << endl;
        cout << "|       -VER MENSAJES-       |" << endl;
        cout << "|____________________________|" << endl;
        cout << "|                            |" << endl;
        cout << "|   1- ELEGIR CONVERSACION   |" << endl;
        cout << "|   2- VER ARCHIVADAS        |" << endl;
        cout << "|____________________________|" << endl;
        cout << "-> ";
        cin >> opcion;
        if(opcion == 1){
            system("clear");
            cout << " ____________________________ " << endl;
            cout << "|                            |" << endl;
            cout << "|       -VER MENSAJES-       |" << endl;
            cout << "|____________________________|" << endl;
            cout << "|                            |" << endl;
            cout << "|   1-                       |" << endl;
            cout << "|   2- VER ARCHIVADAS        |" << endl;
            cout << "|____________________________|" << endl;
            usleep(400000);
            system("clear");
            cout << " ____________________________ " << endl;
            cout << "|                            |" << endl;
            cout << "|       -VER MENSAJES-       |" << endl;
            cout << "|____________________________|" << endl;
            cout << "|                            |" << endl;
            cout << "|   1- ELEGIR CONVERSACION   |" << endl;
            cout << "|   2- VER ARCHIVADAS        |" << endl;
            cout << "|____________________________|" << endl;
            usleep(400000);
            while(existeConversacion == false){
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|       -VER MENSAJES-       |" << endl;
                cout << "|____________________________|" << endl;
                cout << "                              " << endl;
                cout << "-Ingrese ID: ";
                cin >> idConversacionElegida;
                cin.get();
                existeConversacion = i->elegirConversacion(idConversacionElegida);
                if(existeConversacion == false){
                    cout << "ERROR: La conversacion elegida" << endl;
                    cout << " no se encuentra en su lista" << endl;
                    sleep(1);
                }
            }existeConversacion = false;
            
            listaMensajes = i->listarMensajes(idConversacionElegida);
            system("clear");
            cout << " ____________________________ " << endl;
            cout << "|                            |" << endl;
            cout << "|       -VER MENSAJES-       |" << endl;
            cout << "|____________________________|" << endl;
            cout << "                              " << endl;
            for(list<DTMensaje*>::iterator l = listaMensajes->begin(); l != listaMensajes->end(); l++){
                cout << "         -MENSAJE " << (*l)->getCodigo() << "-" << endl;
                cout << "-Enviado: " << (*l)->getFecha()->getDia() << "/" << (*l)->getFecha()->getMes() << "/" << (*l)->getFecha()->getAnio() << " - " << (*l)->getHora()->getHoras() << ":" << (*l)->getHora()->getMinutos() << endl;
                cout << "-Emisor: " << (*l)->getEmisor()->getNombre() << endl;
                cout << "-Destinatarios: " << endl;
                for(list<DTUsuario*>::iterator m = (*l)->getReceptores()->begin(); m != (*l)->getReceptores()->end(); m++){
                    cout << "     -" << (*m)->getNombre() << endl;
                }
                if((*l)->getEmisor()->getTelefono() == s->getUsuarioSesion()->getTelefono()){
                    for(list<DTVisualizacion*>::iterator n = (*l)->getVisualizaciones()->begin(); n != (*l)->getVisualizaciones()->end(); n++){
                        if((*n)->getVisto() == true){
                            contadorVistos ++;
                        }
                    }
                    if(contadorVistos == (*l)->getVisualizaciones()->size()){
                        cout << "-Visto: SI" << endl;
                    }
                    else{
                        cout << "-Visto: NO" << endl;
                    }contadorVistos= 0;
                }
                try{
                    if((&dynamic_cast<DTMensajeSimple&>(*(*l))) != NULL){
                        cout << "-Contenido: " << (&dynamic_cast<DTMensajeSimple&>(*(*l)))->getTexto() << endl;
                    }
                }
                catch(bad_cast &noEsSimple){
                    try{
                        if((&dynamic_cast<DTMensajeContacto&>(*(*l))) != NULL){
                            cout << "-Contenido: " << (&dynamic_cast<DTMensajeContacto&>(*(*l)))->getNombre() << " - " << (&dynamic_cast<DTMensajeContacto&>(*(*l)))->getTelefono() << endl;
                        }
                    }
                    catch(bad_cast &noEsContacto){
                        try{
                            if((&dynamic_cast<DTMultimediaVideo&>(*(*l))) != NULL){
                                cout << "-Contenido: " << (&dynamic_cast<DTMultimediaVideo&>(*(*l)))->getUrl() << " - " << (&dynamic_cast<DTMultimediaVideo&>(*(*l)))->getMinutosDuracion() << ":" << (&dynamic_cast<DTMultimediaVideo&>(*(*l)))->getSegundosDuracion() << endl;
                            }
                        }
                        catch(bad_cast &noEsVideo){
                            cout << "-Contenido: " << (&dynamic_cast<DTMultimediaImagen&>(*(*l)))->getFormato() << " - " << (&dynamic_cast<DTMultimediaImagen&>(*(*l)))->getTamanio() << " - " << (&dynamic_cast<DTMultimediaImagen&>(*(*l)))->getUrl() << " - " << (&dynamic_cast<DTMultimediaImagen&>(*(*l)))->getTexto() << endl;
                        }
                    }
                }
                cout << endl;
            }
            cout << endl;
            cout << "   -ENTER PARA CONTINUAR-" << endl;
            cin.get();
            termina = true;
            break;
        }
        if(opcion == 2){
            system("clear");
            cout << " ____________________________ " << endl;
            cout << "|                            |" << endl;
            cout << "|       -VER MENSAJES-       |" << endl;
            cout << "|____________________________|" << endl;
            cout << "|                            |" << endl;
            cout << "|   1- ELEGIR CONVERSACION   |" << endl;
            cout << "|   2-                       |" << endl;
            cout << "|____________________________|" << endl;
            usleep(400000);
            system("clear");
            cout << " ____________________________ " << endl;
            cout << "|                            |" << endl;
            cout << "|       -VER MENSAJES-       |" << endl;
            cout << "|____________________________|" << endl;
            cout << "|                            |" << endl;
            cout << "|   1- ELEGIR CONVERSACION   |" << endl;
            cout << "|   2- VER ARCHIVADAS        |" << endl;
            cout << "|____________________________|" << endl;
            usleep(400000);
            system("clear");
            cout << " ____________________________ " << endl;
            cout << "|                            |" << endl;
            cout << "|       -VER MENSAJES-       |" << endl;
            cout << "|____________________________|" << endl;
            cout << "                              " << endl;
            
            listaConversacionesArchivadas = i->listarConversacionesArchivadas();
            if(listaConversacionesArchivadas->size() != 0){
                for(list<DTConversacion*>::iterator k = listaConversacionesArchivadas->begin(); k != listaConversacionesArchivadas->end(); k++){
                    try{
                        if((&dynamic_cast<DTConversacionGrupo&>(*(*k))) != NULL){
                            cout << "CONVERSACION " << (*k)->getID() << ": ";
                            cout << (&dynamic_cast<DTConversacionGrupo&>(*(*k)))->getNombre();
                            cout << endl;
                            cout << endl;
                        }
                    }
                    catch (bad_cast &noEsGrupo){
                        cout << "CONVERSACION " << (*k)->getID() << ": ";
                        cout << (&dynamic_cast<DTConversacionSimple&>(*(*k)))->getNombre() << endl;
                        cout << "                "<<(&dynamic_cast<DTConversacionSimple&>(*(*k)))->getTelefono() << endl;
                        cout << endl;
                    }
                }
                cout << endl;
                cout << "   -ENTER PARA CONTINUAR-" << endl;
                cin.get();
                cin.get();
                while(existeConversacion == false){
                    system("clear");
                    cout << " ____________________________ " << endl;
                    cout << "|                            |" << endl;
                    cout << "|       -VER MENSAJES-       |" << endl;
                    cout << "|____________________________|" << endl;
                    cout << "                              " << endl;
                    cout << "-Ingrese ID: ";
                    cin >> idConversacionElegida;
                    existeConversacion = i->elegirConversacion(idConversacionElegida);
                    if(existeConversacion == false){
                        cout << "ERROR: La conversacion elegida" << endl;
                        cout << " no se encuentra en su lista" << endl;
                        sleep(1);
                    }
                }existeConversacion = false;
                listaMensajes = i->listarMensajes(idConversacionElegida);
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|       -VER MENSAJES-       |" << endl;
                cout << "|____________________________|" << endl;
                cout << "                              " << endl;
                for(list<DTMensaje*>::iterator l = listaMensajes->begin(); l != listaMensajes->end(); l++){
                    cout << "         -MENSAJE " << (*l)->getCodigo() << "-" << endl;
                    cout << "-Enviado: " << (*l)->getFecha()->getDia() << "/" << (*l)->getFecha()->getMes() << "/" << (*l)->getFecha()->getAnio() << " - " << (*l)->getHora()->getHoras() << ":" << (*l)->getHora()->getMinutos() << endl;
                    cout << "-Emisor: " << (*l)->getEmisor()->getNombre() << endl;
                    cout << "-Destinatarios: " << endl;
                    for(list<DTUsuario*>::iterator m = (*l)->getReceptores()->begin(); m != (*l)->getReceptores()->end(); m++){
                        cout << "     -" << (*m)->getNombre() << endl;
                    }
                    if((*l)->getEmisor()->getTelefono() == s->getUsuarioSesion()->getTelefono()){
                        for(list<DTVisualizacion*>::iterator n = (*l)->getVisualizaciones()->begin(); n != (*l)->getVisualizaciones()->end(); n++){
                            if((*n)->getVisto() == true){
                                contadorVistos ++;
                            }
                        }
                        if(contadorVistos == (*l)->getVisualizaciones()->size()){
                            cout << "-Visto: SI" << endl;
                        }
                        else{
                            cout << "-Visto: NO" << endl;
                        }
                    }
                    try{
                        if((&dynamic_cast<DTMensajeSimple&>(*(*l))) != NULL){
                            cout << "-Contenido: " << (&dynamic_cast<DTMensajeSimple&>(*(*l)))->getTexto() << endl;
                        }
                    }
                    catch(bad_cast &noEsSimple){
                        try{
                            if((&dynamic_cast<DTMensajeContacto&>(*(*l))) != NULL){
                                cout << "-Contenido: " << (&dynamic_cast<DTMensajeContacto&>(*(*l)))->getNombre() << " - " << (&dynamic_cast<DTMensajeContacto&>(*(*l)))->getTelefono() << endl;
                            }
                        }
                        catch(bad_cast &noEsContacto){
                            try{
                                if((&dynamic_cast<DTMultimediaVideo&>(*(*l))) != NULL){
                                    cout << "-Contenido: " << (&dynamic_cast<DTMultimediaVideo&>(*(*l)))->getUrl() << " - " << (&dynamic_cast<DTMultimediaVideo&>(*(*l)))->getMinutosDuracion() << ":" << (&dynamic_cast<DTMultimediaVideo&>(*(*l)))->getSegundosDuracion() << endl;
                                }
                            }
                            catch(bad_cast &noEsVideo){
                                cout << "-Contenido: " << (&dynamic_cast<DTMultimediaImagen&>(*(*l)))->getFormato() << " - " << (&dynamic_cast<DTMultimediaImagen&>(*(*l)))->getTamanio() << " - " << (&dynamic_cast<DTMultimediaImagen&>(*(*l)))->getUrl() << " - " << (&dynamic_cast<DTMultimediaImagen&>(*(*l)))->getTexto() << endl;
                            }
                        }
                    }
                    cout << endl;
                }
                cout << endl;
                cout << "   -ENTER PARA CONTINUAR-" << endl;
                cin.get();
                cin.get();
                termina = true;
                break;
            }
        }
        else{
            cout << "    ERROR: Usted no posee " << endl;
            cout << "  conversaciones archivadas" << endl;
            sleep(1);
            termina = true;
            eligio = false;
            break;
        }
    }termina = false;
    
    int codigoMensajeVer;
    bool quiereTerminar = false;
    bool encontro = false;
    string respuesta;
    list<DTVisualizacion*> *listaVisualizaciones = new list<DTVisualizacion*>;
    if(eligio == true){
        while(quiereTerminar == false){
            while(respuesta != "Y" || respuesta != "y"){
                encontro = false;
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|       -VER MENSAJES-       |" << endl;
                cout << "|____________________________|" << endl;
                cout << "                              " << endl;
                cout << "¿Quiere ver mas informacion de" << endl;
                cout << "    sus mensajes enviados?" << endl;
                cout << "-> ";
                std::getline(cin, respuesta);
                if(respuesta == "Y" || respuesta == "y"){
                    system("clear");
                    cout << " ____________________________ " << endl;
                    cout << "|                            |" << endl;
                    cout << "|       -VER MENSAJES-       |" << endl;
                    cout << "|____________________________|" << endl;
                    cout << "                              " << endl;
                    cout << "ID MENSAJES ENVIADOS: ";
                    for(list<DTMensaje*>::iterator t = listaMensajes->begin(); t != listaMensajes->end(); t++){
                        if((*t)->getEmisor()->getTelefono() == s->getUsuarioSesion()->getTelefono()){
                            cout << (*t)->getCodigo() << "  ";
                        }
                    }
                    cout << endl;
                    while(encontro == false){
                        cout << endl;
                        cout << "-Ingrese codigo: ";
                        cin >> codigoMensajeVer;
                        cout << endl;
                        cin.get();
                        listaVisualizaciones = i->infoMensajeEnviado(codigoMensajeVer, idConversacionElegida);

                        for(list<DTMensaje*>::iterator i = listaMensajes->begin(); i != listaMensajes->end(); i++){
                            if((*i)->getCodigo() == codigoMensajeVer){
                                for(list<DTVisualizacion*>::iterator t = listaVisualizaciones->begin(); t != listaVisualizaciones->end(); t++){
                                        cout << "         -MENSAJE " << codigoMensajeVer << "-" << endl;
                                        if((*t)->getVisto() == true){
                                            cout << "-Nombre: " << (*t)->getUsuarioVisualizacion()->getNombre() << endl;
                                            cout << "-Fecha: " << (*t)->getFechaVisualizacion()->getDia() << "/" << (*t)->getFechaVisualizacion()->getMes() << "/" << (*t)->getFechaVisualizacion()->getAnio() << " - " << (*t)->getHoraVisualizacion()->getHoras() << ":" << (*t)->getHoraVisualizacion()->getMinutos() << endl;
                                            cout << "-Visto: SI" << endl;
                                            cout << endl;
                                        }
                                        else{
                                            cout << "-Nombre: " << (*t)->getUsuarioVisualizacion()->getNombre() << endl;
                                            cout << "-Fecha: " << (*t)->getFechaVisualizacion()->getDia() << "/" << (*t)->getFechaVisualizacion()->getMes() << "/" << (*t)->getFechaVisualizacion()->getAnio() << " - " << (*t)->getHoraVisualizacion()->getHoras() << ":" << (*t)->getHoraVisualizacion()->getMinutos() << endl;
                                            cout << "-Visto: NO" << endl;
                                            cout << endl;
                                        }
                                }
                                cout << endl;
                                cout << "   -ENTER PARA CONTINUAR-" << endl;
                                cin.get();
                                encontro = true;
                                break;
                            }
                        }
                        if(encontro == false){
                            cout << "ERROR: Este no es un mensaje enviado" << endl;
                            sleep(1);
                        }

                    }
                }
                if(respuesta == "N" || respuesta == "n"){
                    respuesta = "Y";
                    quiereTerminar = true;
                    break;
                }
            }
        }
    }
}

void Menu::eliminarMensaje(){
    
    Sesion *s;
    s = s->getInstancia();
    Conversacion *c;
    
    if(s->getUsuarioSesion()->getTelefono() == "090123654"){
        c = new ConversacionGrupo("Nombre 1");    
        s->getUsuarioSesion()->agregarConversacion(c);
    }
    
    c = new ConversacionGrupo("Nombre 2");
}

//OTROS

void Menu::cargarDatosDePrueba(){
    ControllerUsuarioFactory *f;
    IControllerUsuario *i;
    i = f->getIControllerUsuario();
    i->cargarDatosDePrueba();
}

void Menu::consultarReloj(){
    IControllerUsuario *i;
    ControllerUsuarioFactory *f;
    i = f->getIControllerUsuario();
    i->consultarReloj();
}

void Menu::modificarReloj(){
    IControllerUsuario *i;
    ControllerUsuarioFactory *f;
    i = f->getIControllerUsuario();
   
    int dia;
    int mes;
    int anio;
    int horas;
    int minutos;
   
    system("clear");
    cout << " ___________________________ " << endl;
    cout << "|                           |" << endl;
    cout << "|     -MODIFICAR RELOJ-     |" << endl;
    cout << "|___________________________|" << endl;
    cout << "                             " << endl;
    cout << "-DIA: ";
    cin >> dia;
    cout << "-MES: ";
    cin >> mes;
    cout << "-AÑO: ";
    cin >> anio;
    cout << "-HORA: ";
    cin >> horas;
    cout << "-MINUTOS: ";
    cin >> minutos;
    i->modificarReloj(dia, mes, anio, horas, minutos);
}

void Menu::interaccionMenuPrincipal(){
    int opcion;
    do{
        system("clear");
        cout << " ____________________________ " << endl;
        cout << "|                            |" << endl;
        cout << "|      -MENU PRINCIPAL-      |" << endl;
        cout << "|____________________________|" << endl;
        cout << "|                            |" << endl;
        cout << "|   1- ABRIR GUASAPFING      |" << endl;
        cout << "|   2- CERRAR GUASAPFING     |" << endl;
        cout << "|   3- OPCIONES DE USUARIO   |" << endl;
        cout << "|   4- DATOS DE PRUEBA       |" << endl;
        cout << "|   5- OPCIONES RELOJ        |" << endl;
        cout << "|   6- SALIR                 |" << endl;
        cout << "|____________________________|" << endl;
        cout << "-> ";
        cin >> opcion;
        cin.get();
        switch(opcion){
            case 1:
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|      -MENU PRINCIPAL-      |" << endl;
                cout << "|____________________________|" << endl;
                cout << "|                            |" << endl;
                cout << "|   1-                       |" << endl;
                cout << "|   2- CERRAR GUASAPFING     |" << endl;
                cout << "|   3- OPCIONES DE USUARIO   |" << endl;
                cout << "|   4- DATOS DE PRUEBA       |" << endl;
                cout << "|   5- OPCIONES RELOJ        |" << endl;
                cout << "|   6- SALIR                 |" << endl;
                cout << "|____________________________|" << endl;
                usleep(400000);
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|      -MENU PRINCIPAL-      |" << endl;
                cout << "|____________________________|" << endl;
                cout << "|                            |" << endl;
                cout << "|   1- ABRIR GUASAPFING      |" << endl;
                cout << "|   2- CERRAR GUASAPFING     |" << endl;
                cout << "|   3- OPCIONES DE USUARIO   |" << endl;
                cout << "|   4- DATOS DE PRUEBA       |" << endl;
                cout << "|   5- OPCIONES RELOJ        |" << endl;
                cout << "|   6- SALIR                 |" << endl;
                cout << "|____________________________|" << endl;
                usleep(400000);
                abrirGuasapFING();
                break;
            case 2:
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|      -MENU PRINCIPAL-      |" << endl;
                cout << "|____________________________|" << endl;
                cout << "|                            |" << endl;
                cout << "|   1- ABRIR GUASAPFING      |" << endl;
                cout << "|   2-                       |" << endl;
                cout << "|   3- OPCIONES DE USUARIO   |" << endl;
                cout << "|   4- DATOS DE PRUEBA       |" << endl;
                cout << "|   5- OPCIONES RELOJ        |" << endl;
                cout << "|   6- SALIR                 |" << endl;
                cout << "|____________________________|" << endl;
                usleep(400000);
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|      -MENU PRINCIPAL-      |" << endl;
                cout << "|____________________________|" << endl;
                cout << "|                            |" << endl;
                cout << "|   1- ABRIR GUASAPFING      |" << endl;
                cout << "|   2- CERRAR GUASAPFING     |" << endl;
                cout << "|   3- OPCIONES DE USUARIO   |" << endl;
                cout << "|   4- DATOS DE PRUEBA       |" << endl;
                cout << "|   5- OPCIONES RELOJ        |" << endl;
                cout << "|   6- SALIR                 |" << endl;
                cout << "|____________________________|" << endl;
                usleep(400000);
                cerrarGuasapFING();
                break;
            case 3:
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|      -MENU PRINCIPAL-      |" << endl;
                cout << "|____________________________|" << endl;
                cout << "|                            |" << endl;
                cout << "|   1- ABRIR GUASAPFING      |" << endl;
                cout << "|   2- CERRAR GUASAPFING     |" << endl;
                cout << "|   3-                       |" << endl;
                cout << "|   4- DATOS DE PRUEBA       |" << endl;
                cout << "|   5- OPCIONES RELOJ        |" << endl;
                cout << "|   6- SALIR                 |" << endl;
                cout << "|____________________________|" << endl;
                usleep(400000);
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|      -MENU PRINCIPAL-      |" << endl;
                cout << "|____________________________|" << endl;
                cout << "|                            |" << endl;
                cout << "|   1- ABRIR GUASAPFING      |" << endl;
                cout << "|   2- CERRAR GUASAPFING     |" << endl;
                cout << "|   3- OPCIONES DE USUARIO   |" << endl;
                cout << "|   4- DATOS DE PRUEBA       |" << endl;
                cout << "|   5- OPCIONES RELOJ        |" << endl;
                cout << "|   6- SALIR                 |" << endl;
                cout << "|____________________________|" << endl;
                usleep(400000);
                interaccionMenuUsuario();
                break;
            case 4:
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|      -MENU PRINCIPAL-      |" << endl;
                cout << "|____________________________|" << endl;
                cout << "|                            |" << endl;
                cout << "|   1- ABRIR GUASAPFING      |" << endl;
                cout << "|   2- CERRAR GUASAPFING     |" << endl;
                cout << "|   3- OPCIONES DE USUARIO   |" << endl;
                cout << "|   4-                       |" << endl;
                cout << "|   5- OPCIONES RELOJ        |" << endl;
                cout << "|   6- SALIR                 |" << endl;
                cout << "|____________________________|" << endl;
                usleep(400000);
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|      -MENU PRINCIPAL-      |" << endl;
                cout << "|____________________________|" << endl;
                cout << "|                            |" << endl;
                cout << "|   1- ABRIR GUASAPFING      |" << endl;
                cout << "|   2- CERRAR GUASAPFING     |" << endl;
                cout << "|   3- OPCIONES DE USUARIO   |" << endl;
                cout << "|   4- DATOS DE PRUEBA       |" << endl;
                cout << "|   5- OPCIONES RELOJ        |" << endl;
                cout << "|   6- SALIR                 |" << endl;
                cout << "|____________________________|" << endl;
                usleep(400000);
                cargarDatosDePrueba();
                break;
            case 5:
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|      -MENU PRINCIPAL-      |" << endl;
                cout << "|____________________________|" << endl;
                cout << "|                            |" << endl;
                cout << "|   1- ABRIR GUASAPFING      |" << endl;
                cout << "|   2- CERRAR GUASAPFING     |" << endl;
                cout << "|   3- OPCIONES DE USUARIO   |" << endl;
                cout << "|   4- DATOS DE PRUEBA       |" << endl;
                cout << "|   5-                       |" << endl;
                cout << "|   6- SALIR                 |" << endl;
                cout << "|____________________________|" << endl;
                usleep(400000);
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|      -MENU PRINCIPAL-      |" << endl;
                cout << "|____________________________|" << endl;
                cout << "|                            |" << endl;
                cout << "|   1- ABRIR GUASAPFING      |" << endl;
                cout << "|   2- CERRAR GUASAPFING     |" << endl;
                cout << "|   3- OPCIONES DE USUARIO   |" << endl;
                cout << "|   4- DATOS DE PRUEBA       |" << endl;
                cout << "|   5- OPCIONES RELOJ        |" << endl;
                cout << "|   6- SALIR                 |" << endl;
                cout << "|____________________________|" << endl;
                usleep(400000);
                interaccionMenuReloj();
                break;
            case 6:
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|      -MENU PRINCIPAL-      |" << endl;
                cout << "|____________________________|" << endl;
                cout << "|                            |" << endl;
                cout << "|   1- ABRIR GUASAPFING      |" << endl;
                cout << "|   2- CERRAR GUASAPFING     |" << endl;
                cout << "|   3- OPCIONES DE USUARIO   |" << endl;
                cout << "|   4- DATOS DE PRUEBA       |" << endl;
                cout << "|   5- OPCIONES RELOJ        |" << endl;
                cout << "|   6-                       |" << endl;
                cout << "|____________________________|" << endl;
                usleep(400000);
                system("clear");
                cout << " ____________________________ " << endl;
                cout << "|                            |" << endl;
                cout << "|      -MENU PRINCIPAL-      |" << endl;
                cout << "|____________________________|" << endl;
                cout << "|                            |" << endl;
                cout << "|   1- ABRIR GUASAPFING      |" << endl;
                cout << "|   2- CERRAR GUASAPFING     |" << endl;
                cout << "|   3- OPCIONES DE USUARIO   |" << endl;
                cout << "|   4- DATOS DE PRUEBA       |" << endl;
                cout << "|   5- OPCIONES RELOJ        |" << endl;
                cout << "|   6- SALIR                 |" << endl;
                cout << "|____________________________|" << endl;
                usleep(400000);
                break;
            default:
                cout << endl;
                cout << "ERROR: Debe elegir una de las" << endl;
                cout << "    opciones disponibles" << endl;
                sleep(1);
        }
    }while(opcion != 6);
}

void Menu::interaccionMenuUsuario(){
    int opcion;
    do{        
        system("clear");
        cout << " _____________________________ " << endl;
        cout << "|                             |" << endl;
        cout << "|    -OPCIONES DE USUARIO-    |" << endl;
        cout << "|_____________________________|" << endl;
        cout << "|                             |" << endl;
        cout << "|   1- AGREGAR CONTACTOS      |" << endl;
        cout << "|   2- ALTA GRUPO             |" << endl;
        cout << "|   3- ENVIAR MENSAJES        |" << endl;
        cout << "|   4- VER MENSAJES           |" << endl;
        cout << "|   5- ARCHIVAR CONVERSACION  |" << endl;
        cout << "|   6- MODIFICAR USUARIO      |" << endl;
        cout << "|   7- ELIMINAR MENSAJES      |" << endl;
        cout << "|   8- ATRAS                  |" << endl;
        cout << "|_____________________________|" << endl;
        cout << "-> ";
        cin >> opcion;
        cin.get();
        
        switch(opcion){
            case 1:
                //AGREGAR CONTACTOS
                system("clear");
                cout << " _____________________________ " << endl;
                cout << "|                             |" << endl;
                cout << "|    -OPCIONES DE USUARIO-    |" << endl;
                cout << "|_____________________________|" << endl;
                cout << "|                             |" << endl;
                cout << "|   1-                        |" << endl;
                cout << "|   2- ALTA GRUPO             |" << endl;
                cout << "|   3- ENVIAR MENSAJES        |" << endl;
                cout << "|   4- VER MENSAJES           |" << endl;
                cout << "|   5- ARCHIVAR CONVERSACION  |" << endl;
                cout << "|   6- MODIFICAR USUARIO      |" << endl;
                cout << "|   7- ELIMINAR MENSAJES      |" << endl;
                cout << "|   8- ATRAS                  |" << endl;
                cout << "|_____________________________|" << endl;
                usleep(400000);
                system("clear");
                cout << " _____________________________ " << endl;
                cout << "|                             |" << endl;
                cout << "|    -OPCIONES DE USUARIO-    |" << endl;
                cout << "|_____________________________|" << endl;
                cout << "|                             |" << endl;
                cout << "|   1- AGREGAR CONTACTOS      |" << endl;
                cout << "|   2- ALTA GRUPO             |" << endl;
                cout << "|   3- ENVIAR MENSAJES        |" << endl;
                cout << "|   4- VER MENSAJES           |" << endl;
                cout << "|   5- ARCHIVAR CONVERSACION  |" << endl;
                cout << "|   6- MODIFICAR USUARIO      |" << endl;
                cout << "|   7- ELIMINAR MENSAJES      |" << endl;
                cout << "|   8- ATRAS                  |" << endl;
                cout << "|_____________________________|" << endl;
                usleep(400000);
                agregarContacto();
                break;
            case 2:
                //ALTA GRUPO
                system("clear");
                cout << " _____________________________ " << endl;
                cout << "|                             |" << endl;
                cout << "|    -OPCIONES DE USUARIO-    |" << endl;
                cout << "|_____________________________|" << endl;
                cout << "|                             |" << endl;
                cout << "|   1- AGREGAR CONTACTOS      |" << endl;
                cout << "|   2-                        |" << endl;
                cout << "|   3- ENVIAR MENSAJES        |" << endl;
                cout << "|   4- VER MENSAJES           |" << endl;
                cout << "|   5- ARCHIVAR CONVERSACION  |" << endl;
                cout << "|   6- MODIFICAR USUARIO      |" << endl;
                cout << "|   7- ELIMINAR MENSAJES      |" << endl;
                cout << "|   8- ATRAS                  |" << endl;
                cout << "|_____________________________|" << endl;
                usleep(400000);
                system("clear");
                cout << " _____________________________ " << endl;
                cout << "|                             |" << endl;
                cout << "|    -OPCIONES DE USUARIO-    |" << endl;
                cout << "|_____________________________|" << endl;
                cout << "|                             |" << endl;
                cout << "|   1- AGREGAR CONTACTOS      |" << endl;
                cout << "|   2- ALTA GRUPO             |" << endl;
                cout << "|   3- ENVIAR MENSAJES        |" << endl;
                cout << "|   4- VER MENSAJES           |" << endl;
                cout << "|   5- ARCHIVAR CONVERSACION  |" << endl;
                cout << "|   6- MODIFICAR USUARIO      |" << endl;
                cout << "|   7- ELIMINAR MENSAJES      |" << endl;
                cout << "|   8- ATRAS                  |" << endl;
                cout << "|_____________________________|" << endl;
                usleep(400000);
                altaGrupo();
                break;
            case 3:
                //ENVIAR MENSAJES
                system("clear");
                cout << " _____________________________ " << endl;
                cout << "|                             |" << endl;
                cout << "|    -OPCIONES DE USUARIO-    |" << endl;
                cout << "|_____________________________|" << endl;
                cout << "|                             |" << endl;
                cout << "|   1- AGREGAR CONTACTOS      |" << endl;
                cout << "|   2- ALTA GRUPO             |" << endl;
                cout << "|   3-                        |" << endl;
                cout << "|   4- VER MENSAJES           |" << endl;
                cout << "|   5- ARCHIVAR CONVERSACION  |" << endl;
                cout << "|   6- MODIFICAR USUARIO      |" << endl;
                cout << "|   7- ELIMINAR MENSAJES      |" << endl;
                cout << "|   8- ATRAS                  |" << endl;
                cout << "|_____________________________|" << endl;
                usleep(400000);
                system("clear");
                cout << " _____________________________ " << endl;
                cout << "|                             |" << endl;
                cout << "|    -OPCIONES DE USUARIO-    |" << endl;
                cout << "|_____________________________|" << endl;
                cout << "|                             |" << endl;
                cout << "|   1- AGREGAR CONTACTOS      |" << endl;
                cout << "|   2- ALTA GRUPO             |" << endl;
                cout << "|   3- ENVIAR MENSAJES        |" << endl;
                cout << "|   4- VER MENSAJES           |" << endl;
                cout << "|   5- ARCHIVAR CONVERSACION  |" << endl;
                cout << "|   6- MODIFICAR USUARIO      |" << endl;
                cout << "|   7- ELIMINAR MENSAJES      |" << endl;
                cout << "|   8- ATRAS                  |" << endl;
                cout << "|_____________________________|" << endl;
                usleep(400000);
                enviarMensaje();
                break;
            case 4:
                //VER MENSAJES
                system("clear");
                cout << " _____________________________ " << endl;
                cout << "|                             |" << endl;
                cout << "|    -OPCIONES DE USUARIO-    |" << endl;
                cout << "|_____________________________|" << endl;
                cout << "|                             |" << endl;
                cout << "|   1- AGREGAR CONTACTOS      |" << endl;
                cout << "|   2- ALTA GRUPO             |" << endl;
                cout << "|   3- ENVIAR MENSAJES        |" << endl;
                cout << "|   4-                        |" << endl;
                cout << "|   5- ARCHIVAR CONVERSACION  |" << endl;
                cout << "|   6- MODIFICAR USUARIO      |" << endl;
                cout << "|   7- ELIMINAR MENSAJES      |" << endl;
                cout << "|   8- ATRAS                  |" << endl;
                cout << "|_____________________________|" << endl;
                usleep(400000);
                system("clear");
                cout << " _____________________________ " << endl;
                cout << "|                             |" << endl;
                cout << "|    -OPCIONES DE USUARIO-    |" << endl;
                cout << "|_____________________________|" << endl;
                cout << "|                             |" << endl;
                cout << "|   1- AGREGAR CONTACTOS      |" << endl;
                cout << "|   2- ALTA GRUPO             |" << endl;
                cout << "|   3- ENVIAR MENSAJES        |" << endl;
                cout << "|   4- VER MENSAJES           |" << endl;
                cout << "|   5- ARCHIVAR CONVERSACION  |" << endl;
                cout << "|   6- MODIFICAR USUARIO      |" << endl;
                cout << "|   7- ELIMINAR MENSAJES      |" << endl;
                cout << "|   8- ATRAS                  |" << endl;
                cout << "|_____________________________|" << endl;
                usleep(400000);
                verMensaje();
                break;
            case 5:
                //ARCHIVAR CONVERSACION
                system("clear");
                cout << " _____________________________ " << endl;
                cout << "|                             |" << endl;
                cout << "|    -OPCIONES DE USUARIO-    |" << endl;
                cout << "|_____________________________|" << endl;
                cout << "|                             |" << endl;
                cout << "|   1- AGREGAR CONTACTOS      |" << endl;
                cout << "|   2- ALTA GRUPO             |" << endl;
                cout << "|   3- ENVIAR MENSAJES        |" << endl;
                cout << "|   4- VER MENSAJES           |" << endl;
                cout << "|   5-                        |" << endl;
                cout << "|   6- MODIFICAR USUARIO      |" << endl;
                cout << "|   7- ELIMINAR MENSAJES      |" << endl;
                cout << "|   8- ATRAS                  |" << endl;
                cout << "|_____________________________|" << endl;
                usleep(400000);
                system("clear");
                cout << " _____________________________ " << endl;
                cout << "|                             |" << endl;
                cout << "|    -OPCIONES DE USUARIO-    |" << endl;
                cout << "|_____________________________|" << endl;
                cout << "|                             |" << endl;
                cout << "|   1- AGREGAR CONTACTOS      |" << endl;
                cout << "|   2- ALTA GRUPO             |" << endl;
                cout << "|   3- ENVIAR MENSAJES        |" << endl;
                cout << "|   4- VER MENSAJES           |" << endl;
                cout << "|   5- ARCHIVAR CONVERSACION  |" << endl;
                cout << "|   6- MODIFICAR USUARIO      |" << endl;
                cout << "|   7- ELIMINAR MENSAJES      |" << endl;
                cout << "|   8- ATRAS                  |" << endl;
                cout << "|_____________________________|" << endl;
                usleep(400000);
                eliminarMensaje();
                break;
            case 6:
                //MODIFICAR USUARIO
                system("clear");
                cout << " _____________________________ " << endl;
                cout << "|                             |" << endl;
                cout << "|    -OPCIONES DE USUARIO-    |" << endl;
                cout << "|_____________________________|" << endl;
                cout << "|                             |" << endl;
                cout << "|   1- AGREGAR CONTACTOS      |" << endl;
                cout << "|   2- ALTA GRUPO             |" << endl;
                cout << "|   3- ENVIAR MENSAJES        |" << endl;
                cout << "|   4- VER MENSAJES           |" << endl;
                cout << "|   5- ARCHIVAR CONVERSACION  |" << endl;
                cout << "|   6-                        |" << endl;
                cout << "|   7- ELIMINAR MENSAJES      |" << endl;
                cout << "|   8- ATRAS                  |" << endl;
                cout << "|_____________________________|" << endl;
                usleep(400000);
                system("clear");
                cout << " _____________________________ " << endl;
                cout << "|                             |" << endl;
                cout << "|    -OPCIONES DE USUARIO-    |" << endl;
                cout << "|_____________________________|" << endl;
                cout << "|                             |" << endl;
                cout << "|   1- AGREGAR CONTACTOS      |" << endl;
                cout << "|   2- ALTA GRUPO             |" << endl;
                cout << "|   3- ENVIAR MENSAJES        |" << endl;
                cout << "|   4- VER MENSAJES           |" << endl;
                cout << "|   5- ARCHIVAR CONVERSACION  |" << endl;
                cout << "|   6- MODIFICAR USUARIO      |" << endl;
                cout << "|   7- ELIMINAR MENSAJES      |" << endl;
                cout << "|   8- ATRAS                  |" << endl;
                cout << "|_____________________________|" << endl;
                usleep(400000);
                modificarUsuario();
                break;
            case 7:
                //ELIMINAR MENSAJES
                system("clear");
                cout << " _____________________________ " << endl;
                cout << "|                             |" << endl;
                cout << "|    -OPCIONES DE USUARIO-    |" << endl;
                cout << "|_____________________________|" << endl;
                cout << "|                             |" << endl;
                cout << "|   1- AGREGAR CONTACTOS      |" << endl;
                cout << "|   2- ALTA GRUPO             |" << endl;
                cout << "|   3- ENVIAR MENSAJES        |" << endl;
                cout << "|   4- VER MENSAJES           |" << endl;
                cout << "|   5- ARCHIVAR CONVERSACION  |" << endl;
                cout << "|   6- MODIFICAR USUARIO      |" << endl;
                cout << "|   7- ELIMINAR MENSAJES      |" << endl;
                cout << "|   8- ATRAS                  |" << endl;
                cout << "|_____________________________|" << endl;
                usleep(400000);
                system("clear");
                cout << " _____________________________ " << endl;
                cout << "|                             |" << endl;
                cout << "|    -OPCIONES DE USUARIO-    |" << endl;
                cout << "|_____________________________|" << endl;
                cout << "|                             |" << endl;
                cout << "|   1- AGREGAR CONTACTOS      |" << endl;
                cout << "|   2- ALTA GRUPO             |" << endl;
                cout << "|   3- ENVIAR MENSAJES        |" << endl;
                cout << "|   4- VER MENSAJES           |" << endl;
                cout << "|   5- ARCHIVAR CONVERSACION  |" << endl;
                cout << "|   6- MODIFICAR USUARIO      |" << endl;
                cout << "|   7-                        |" << endl;
                cout << "|   8- ATRAS                  |" << endl;
                cout << "|_____________________________|" << endl;
                usleep(400000);
                //BORRAR DESDE ACA
                system("clear");
                cout << "Aca iria ELIMINAR MENSAJES" << endl;
                sleep(1);
                //HASTA ACA
                break;
            case 8:
                system("clear");
                cout << " _____________________________ " << endl;
                cout << "|                             |" << endl;
                cout << "|    -OPCIONES DE USUARIO-    |" << endl;
                cout << "|_____________________________|" << endl;
                cout << "|                             |" << endl;
                cout << "|   1- AGREGAR CONTACTOS      |" << endl;
                cout << "|   2- ALTA GRUPO             |" << endl;
                cout << "|   3- ENVIAR MENSAJES        |" << endl;
                cout << "|   4- VER MENSAJES           |" << endl;
                cout << "|   5- ARCHIVAR CONVERSACION  |" << endl;
                cout << "|   6- MODIFICAR USUARIO      |" << endl;
                cout << "|   7- ELIMINAR MENSAJES      |" << endl;
                cout << "|   8-                        |" << endl;
                cout << "|_____________________________|" << endl;
                usleep(400000);
                system("clear");
                cout << " _____________________________ " << endl;
                cout << "|                             |" << endl;
                cout << "|    -OPCIONES DE USUARIO-    |" << endl;
                cout << "|_____________________________|" << endl;
                cout << "|                             |" << endl;
                cout << "|   1- AGREGAR CONTACTOS      |" << endl;
                cout << "|   2- ALTA GRUPO             |" << endl;
                cout << "|   3- ENVIAR MENSAJES        |" << endl;
                cout << "|   4- VER MENSAJES           |" << endl;
                cout << "|   5- ARCHIVAR CONVERSACION  |" << endl;
                cout << "|   6- MODIFICAR USUARIO      |" << endl;
                cout << "|   7- ELIMINAR MENSAJES      |" << endl;
                cout << "|   8- ATRAS                  |" << endl;
                cout << "|_____________________________|" << endl;
                usleep(400000);
                break;
            default:
                cout << endl;
                cout << "ERROR: Debe elegir una de las" << endl;
                cout << "    opciones disponibles" << endl;
                sleep(1);
        }      
    }while(opcion != 8);
} 

void Menu::interaccionMenuReloj(){
    int opcion;
    do{
        system("clear");
        cout << " _____________________________ " << endl;
        cout << "|                             |" << endl;
        cout << "|     -OPCIONES DE RELOJ-     |" << endl;
        cout << "|_____________________________|" << endl;
        cout << "|                             |" << endl;
        cout << "|   1- MODIFICAR EL RELOJ     |" << endl;
        cout << "|   2- CONSULTAR EL RELOJ     |" << endl;
        cout << "|   3- ATRAS                  |" << endl;
        cout << "|_____________________________|" << endl;
        cout << "-> ";
        cin >> opcion;
        cin.get();
        
        switch(opcion){
            case 1:
                system("clear");
                cout << " _____________________________ " << endl;
                cout << "|                             |" << endl;
                cout << "|     -OPCIONES DE RELOJ-     |" << endl;
                cout << "|_____________________________|" << endl;
                cout << "|                             |" << endl;
                cout << "|   1-                        |" << endl;
                cout << "|   2- CONSULTAR EL RELOJ     |" << endl;
                cout << "|   3- ATRAS                  |" << endl;
                cout << "|_____________________________|" << endl;
                usleep(400000);
                system("clear");
                cout << " _____________________________ " << endl;
                cout << "|                             |" << endl;
                cout << "|     -OPCIONES DE RELOJ-     |" << endl;
                cout << "|_____________________________|" << endl;
                cout << "|                             |" << endl;
                cout << "|   1- MODIFICAR EL RELOJ     |" << endl;
                cout << "|   2- CONSULTAR EL RELOJ     |" << endl;
                cout << "|   3- ATRAS                  |" << endl;
                cout << "|_____________________________|" << endl;
                usleep(400000);
                modificarReloj();
                break;
            case 2:
                system("clear");
                cout << " _____________________________ " << endl;
                cout << "|                             |" << endl;
                cout << "|     -OPCIONES DE RELOJ-     |" << endl;
                cout << "|_____________________________|" << endl;
                cout << "|                             |" << endl;
                cout << "|   1- MODIFICAR EL RELOJ     |" << endl;
                cout << "|   2-                        |" << endl;
                cout << "|   3- ATRAS                  |" << endl;
                cout << "|_____________________________|" << endl;
                usleep(400000);
                system("clear");
                cout << " _____________________________ " << endl;
                cout << "|                             |" << endl;
                cout << "|     -OPCIONES DE RELOJ-     |" << endl;
                cout << "|_____________________________|" << endl;
                cout << "|                             |" << endl;
                cout << "|   1- MODIFICAR EL RELOJ     |" << endl;
                cout << "|   2- CONSULTAR EL RELOJ     |" << endl;
                cout << "|   3- ATRAS                  |" << endl;
                cout << "|_____________________________|" << endl;
                usleep(400000);
                consultarReloj();   
                break;
            case 3:
                system("clear");
                cout << " _____________________________ " << endl;
                cout << "|                             |" << endl;
                cout << "|     -OPCIONES DE RELOJ-     |" << endl;
                cout << "|_____________________________|" << endl;
                cout << "|                             |" << endl;
                cout << "|   1- MODIFICAR EL RELOJ     |" << endl;
                cout << "|   2- CONSULTAR EL RELOJ     |" << endl;
                cout << "|   3-                        |" << endl;
                cout << "|_____________________________|" << endl;
                usleep(400000);
                system("clear");
                cout << " _____________________________ " << endl;
                cout << "|                             |" << endl;
                cout << "|     -OPCIONES DE RELOJ-     |" << endl;
                cout << "|_____________________________|" << endl;
                cout << "|                             |" << endl;
                cout << "|   1- MODIFICAR EL RELOJ     |" << endl;
                cout << "|   2- CONSULTAR EL RELOJ     |" << endl;
                cout << "|   3- ATRAS                  |" << endl;
                cout << "|_____________________________|" << endl;
                usleep(400000);
                break;
        }
    }while(opcion != 3);
}
