#ifndef VISUALIZACION_H
#define VISUALIZACION_H

#include "../DataTypes/DTFecha.h"
#include "../DataTypes/DTHora.h"
#include "../DataTypes/DTVisualizacion.h"

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

using namespace std;

class Conversacion;
class Usuario;
class Mensaje;

class Visualizacion {
public:
    Visualizacion();
    Visualizacion(DTHora *HoraVisualizacion, DTFecha *FechaVisualizacion, bool Visto, Usuario *UsuarioVisto);
    Visualizacion(const Visualizacion& orig);
    
    virtual ~Visualizacion();
    
    DTHora *getHoraVisualizacion();
    DTFecha *getFechaVisualizacion();
    bool getVisto();
    Usuario *getUsuarioVisto();
    
    void setHoraVisualizacion(DTHora *HoraVisualizacion);
    void setFechaVisualizacion(DTFecha *HoraVisualizacion);
    void setVisto(bool Visto);
    void setUsuarioVisto(Usuario *UsuarioVisto);
    
    //OPERACIONES VER MENSAJES
    DTVisualizacion *getDatos();
private:
    DTHora *horaVisualizacion;
    DTFecha *fechaVisualizacion;
    bool visto;
    Usuario *usuarioVisto;
};

#endif
