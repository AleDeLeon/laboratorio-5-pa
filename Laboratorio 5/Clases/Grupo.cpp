#include "Grupo.h"

//CONSTRUCTORES

Grupo::Grupo(string Nombre, string Imagen){
    this->nombre = Nombre;
    this->imagen = Imagen;
    this->usuariosGrupo = new list<Usuario*>;
    this->conversacion = new ConversacionGrupo();
}

Grupo::Grupo() {
}

Grupo::Grupo(const Grupo& orig) {
}

//DESTRUCTOR

Grupo::~Grupo() {
}

//GETTERS

string Grupo::getNombre(){
    return this->nombre;
}

string Grupo::getImagen(){
    return this->imagen;
}

DTFecha *Grupo::getFechaCreacion(){
    return this->fechaCreacion;
}

DTHora *Grupo::getHoraCreacion(){
    return this->horaCreacion;
}

list<Usuario*> *Grupo::getUsuariosGrupo(){
    return this->usuariosGrupo;
}

Usuario *Grupo::getAdministrador(){
    return this->administrador;
}

Conversacion *Grupo::getConversacion(){
    return this->conversacion;
}

//SETTERS

void Grupo::setNombre(string Nombre){
    this->nombre = Nombre;
}

void Grupo::setImagen(string Imagen){
    this->imagen = Imagen;
}

void Grupo::setHoraCreacion(DTHora *HoraCreacion){
    this->horaCreacion = HoraCreacion;
}

void Grupo::setFechaCreacion(DTFecha *FechaCreacion){
    this->fechaCreacion = FechaCreacion;
}

void Grupo::setAdministrador(Usuario *Administrador){
    this->administrador = Administrador;
}

void Grupo::setConversacion(Conversacion* ConversacionGrupo){
    this->conversacion = ConversacionGrupo;
}

//OTROS

void Grupo::agregarUsuario(Usuario *u){
    this->usuariosGrupo->push_back(u);
}

void Grupo::borrarUsuario(Usuario *u){
    this->usuariosGrupo->remove(u);
}
