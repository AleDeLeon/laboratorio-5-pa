#include "Estado.h"

//CONSTRUCTORES

Estado::Estado(string texto){
    this->texto = texto;
}

Estado::Estado() {
}

Estado::Estado(const Estado& orig) {
}

//DESTRUCTORES

Estado::~Estado() {
}

//GETTERS

string Estado::getTexto(){
    return this->texto;
}

DTFecha Estado::getFechaCreacion(){
    return this->fechaCreacion;
}

DTHora Estado::getHoraCreacion(){
    return this->horaCreacion;
}

//SETTERS

void Estado::setTexto(string texto){
    this->texto = texto;
}

void Estado::setFechaCreacion(DTFecha fechaCreacion){
    this->fechaCreacion = fechaCreacion;
}

void Estado::setHoraCreacion(DTHora horaCreacion){
    this->horaCreacion = horaCreacion;
}
