#ifndef RELOJ_H
#define RELOJ_H

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

#include "../DataTypes/DTFecha.h"
#include "../DataTypes/DTHora.h"

using namespace std;

class Reloj {
public:
    int getDia();
    int getMes();
    int getAnio();
    int getHoras();
    int getMinutos();
    DTFecha *getFecha();
    DTHora *getHora();
    static Reloj *getInstancia();
    
    void consultarReloj();
    void modificarReloj(int Dia, int Mes, int Anio, int Horas, int Minutos);
private:
    Reloj(int Dia, int Mes, int Anio, int Horas, int Minutos);
    
    static Reloj *instancia;
    
    int dia;
    int mes;
    int anio;
    int horas;
    int minutos;
};

#endif

