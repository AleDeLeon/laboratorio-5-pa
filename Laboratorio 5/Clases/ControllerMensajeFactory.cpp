#include "ControllerMensajeFactory.h"
#include "ControllerMensaje.h"

//CONSTRUCTORES

ControllerMensajeFactory::ControllerMensajeFactory() {
}

ControllerMensajeFactory::ControllerMensajeFactory(const ControllerMensajeFactory& orig) {
}

//DESTRUCTOR

ControllerMensajeFactory::~ControllerMensajeFactory() {
}

//OTROS

IControllerMensaje *ControllerMensajeFactory::getIControllerMensaje(){
    IControllerMensaje *i;
    i = new ControllerMensaje();
    return i;
}
