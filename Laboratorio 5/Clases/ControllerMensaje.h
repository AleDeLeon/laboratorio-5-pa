#ifndef CONTROLLERMENSAJE_H
#define CONTROLLERMENSAJE_H

#include "Sesion.h"
#include "Conversacion.h"
#include "ConversacionSimple.h"

#include "../DataTypes/DTConversacion.h"
#include "../DataTypes/DTContacto.h"
#include "../DataTypes/DTVisualizacion.h"

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

using namespace std;

class ControllerMensaje:public IControllerMensaje {
public:
    ControllerMensaje();
    ControllerMensaje(const ControllerMensaje& orig);
    
    //OPERACIONES DE ENVIAR MENSAJE
    list<DTConversacion*> *listarConversaciones();
    list<DTMensaje*> *listarMensajes(int idConversacion);
    list<DTConversacion*> *listarConversacionesArchivadas();
    list<DTContacto*> *listarContactos();
    int crearConversacion(string Telefono);
    void crearMensaje(DTMensaje *datosMensaje, int idConversacion);
    bool elegirConversacion(int idConversacion);
    
    //OPERACIONES DE VER MENSAJE
    list<DTVisualizacion*> *infoMensajeEnviado(int idMensaje, int idConversacion);
    DTContacto *listarConversaciones(string telefono);
    
    //OPERACIONES DE ELIMINAR MENSAJE
    void eliminarMensaje(int idMensaje);
    
    virtual ~ControllerMensaje();
};

#endif

