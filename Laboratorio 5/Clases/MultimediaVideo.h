#ifndef MULTIMEDIAVIDEO_H
#define MULTIMEDIAVIDEO_H

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

#include "Sesion.h"

#include "../DataTypes/DTMensaje.h"
#include "../DataTypes/DTMultimediaVideo.h"
#include "../DataTypes/DTVisualizacion.h"

using namespace std;

class MultimediaVideo:public Mensaje {
public:
    MultimediaVideo();
    MultimediaVideo(string Url, int MinutosDuracion, int SegundosDuracion);
    MultimediaVideo(const MultimediaVideo& orig);
    
    virtual ~MultimediaVideo();
    
    string getUrl();
    int getMinutosDuracion();
    int getSegundosDuarcion();
    
    void setUrl(string Url);
    void setMinutosDuracion(int MinutosDuracion);
    void setSegundosDuracion(int SegundosDuracion);
    
    //OPERACIONES DE VER MENSAJE
    DTMensaje *getDatos(Mensaje *m);
private:
    string url;
    int minutosDuracion;
    int segundosDuracion;
};

#endif

