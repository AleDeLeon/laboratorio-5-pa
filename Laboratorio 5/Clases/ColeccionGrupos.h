#ifndef COLECCIONGRUPOS_H
#define COLECCIONGRUPOS_H

#include "Grupo.h"

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

using namespace std;

class ColeccionGrupos {
public:
    list<Grupo*> *getListaGrupos();
    static ColeccionGrupos *getInstancia();
    
    void agregarGrupo(Grupo *g);
    list<Grupo*>::iterator borrarGrupo(list<Grupo*>::iterator i);
private:
    ColeccionGrupos();
    
    static ColeccionGrupos *instancia;
    list<Grupo*> *grupos;
};

#endif

