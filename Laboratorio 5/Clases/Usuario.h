#ifndef USUARIO_H
#define USUARIO_H

#include "Conversacion.h"
#include "Mensaje.h"
#include "Visualizacion.h"
#include "Reloj.h"
#include "ColeccionUsuarios.h"

#include "../DataTypes/DTConversacion.h"
#include "../DataTypes/DTConexion.h"
#include "../DataTypes/DTFecha.h"
#include "../DataTypes/DTHora.h"
#include "../DataTypes/DTMensaje.h"
#include "../DataTypes/DTContacto.h"
#include "../DataTypes/DTVisualizacion.h"

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <unistd.h>
#include <list>

using namespace std;


class Usuario {
public:
    Usuario();
    Usuario(string Telefono, string Nombre, string ImagenDePerfil, string descripcion);
    Usuario(const Usuario& orig);
    
    virtual ~Usuario();
    
    string getTelefono();
    string getNombre();
    string getImagenDePerfil();
    string getDescripcion();
    DTFecha *getFechaRegistro();
    DTConexion *getUltimaConexion();
    list<Usuario*> *getContactos();
    list<Conversacion*> *getConversaciones();
    Mensaje *getMensaje(int idConversacion, int idMensaje);
    
    void setTelefono(string Telefono);
    void setNombre(string Nombre);
    void setDescripcion(string Descripcion);
    void setImagenDePerfil(string ImagenDePerfil);
    void setFechaRegistro(DTFecha *FechaRegistro);
    void setUltimaConexion(DTConexion *UltimaConexion);
    
    void agregarContacto(Usuario *u);
    void agregarConversacion(Conversacion *c);
    void borrarContacto(Usuario *u);
    void borrarConversacion(Conversacion *c);
    
    //AGREGAR CONTACTO
    DTContacto *getDatos();
    
    //OPERACIONES DE ENVIAR MENSAJE
    void agregarReceptor(Usuario *u, int idMensaje, int idConversacion);
    int crearMensaje(DTMensaje *datosMensaje, int idConversacion);
    list<string> getTelefonosParticipantes(int idConversacion);
    list<DTConversacion*> *obtenerDatosConversaciones();
    list<DTConversacion*> *getConversacionesArchivadas();
    list<DTContacto*> *getDatosContactos();
    DTContacto *getDatos(Usuario *u);
    DTContacto *getDatosContacto(string telefono);
    bool elegirConversacion(int idConversacion);
    int crearConversacion (string telefono);
    void agregarMensajeNuevo(int idConversacion, int idMensaje, string telefonoEmisor);
    
    //OPERACIONES DE VER MENSAJE
    list<DTVisualizacion*> *infoMensajeEnviado(int idMensaje, int idConversacion);
    list<DTMensaje*> *listarMensajes(int idConversacion);
    
    //OPERACIONES DE ELIMINAR MENSAJE
    void eliminarMensaje(int idMensaje);
    
private:
    string telefono;
    string nombre;
    string imagenDePerfil;
    string descripcion;
    DTFecha *fechaRegistro;
    DTConexion *ultimaConexion;
    list<Usuario*> *contactos;
    list<Conversacion*> *conversaciones;
};

#endif

