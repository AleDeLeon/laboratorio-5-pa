#ifndef ICONTROLLERUSUARIO_H
#define ICONTROLLERUSUARIO_H

#include "../DataTypes/DTConexion.h"
#include "../DataTypes/DTUsuario.h"
#include "../DataTypes/DTContacto.h"
#include "../DataTypes/DTGrupo.h"
#include "../DataTypes/DTConversacion.h"

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

using namespace std;

class IControllerUsuario {
public:
    IControllerUsuario();
    IControllerUsuario(const IControllerUsuario& orig);
    
    virtual ~IControllerUsuario();
    
    //OPERACIONES DE ABRIR GUASAPFING
    virtual bool ingresoNumero(string telefono) = 0;
    virtual void ingresoUsuario(string telefono) = 0;
    virtual DTConexion altaUsuario(DTUsuario *nuevoUsuario) = 0;
    
    //OPERACIONES DE CERRAR GUASAPFING
    virtual void cerrar() = 0;
    
    //OPERACIONES DE AGREGAR CONTACTO
    virtual DTContacto *mostrarContacto(string telefono) = 0;
    virtual void agregarContacto(string telefono) = 0;
    virtual list<DTContacto*>* listarContactos() = 0;
    
    //OPERACIONES ALTA GRUPO
    virtual list<DTContacto*> *quitarContacto(string telefono, list<DTContacto*> *contactosElegidos) = 0;
    virtual list<DTContacto*> *listarContactosRestantes(list<string> telefonosElegidos) = 0;
    virtual list<DTContacto*> *agregarContacto(string telefono, list<DTContacto*> *contactosElegidos) = 0;
    virtual void altaGrupo(DTGrupo *datosGrupo, list<DTContacto*> *contactosElegidos) = 0;
    
    //OPERACIONES DE MODIFICAR USUARIO
    virtual void cambiarNombre(string nuevoNombre) = 0;
    virtual void cambiarImagenDePerfil(string nuevaImagen) = 0;
    virtual void cambiarDescripcion(string nuevaDescripcion) = 0;
    
    //OTRAS OPERACIONES
    virtual void cargarDatosDePrueba() = 0;
    virtual void modificarReloj(int dia, int mes, int anio, int horas, int minutos) = 0;
    virtual void consultarReloj() = 0;
};

#endif

