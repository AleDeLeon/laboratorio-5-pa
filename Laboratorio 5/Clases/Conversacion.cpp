#include "Conversacion.h"
#include "Sesion.h"
#include "ConversacionSimple.h"
#include "MensajeSimple.h"
#include "Reloj.h"
#include "MensajeContacto.h"
#include "MultimediaImagen.h"
#include "MultimediaVideo.h"

int Conversacion::contador = 1;

//CONSTRUCTORES

Conversacion::Conversacion(){
    this->estado = true;
    this->mensajes = new list<Mensaje*>;
}

Conversacion::Conversacion(const Conversacion& orig) {
}

//DESTRUCTOR

Conversacion::~Conversacion() {
}

//GETTERS

int Conversacion::getID(){
    return this->id;
}

bool Conversacion::getEstado(){
    return this->estado;
}

list<Mensaje*> *Conversacion::getMensajes(){
    return this->mensajes;
}

int Conversacion::getContadorID(){
    return contador;
}

Mensaje *Conversacion::getMensaje(int idMensaje){
    Mensaje *m;
    for(list<Mensaje*>::iterator i = this->mensajes->begin(); i != this->mensajes->end(); i++){
        if((*i)->getCodigo() == idMensaje){
            m = (*i);
        }
    }
    return m;
}

//SETTERS

void Conversacion::setID(int ID){
    this->id = ID;
}

void Conversacion::setEstado(bool Estado){
    this->estado = Estado;
}

//OTROS

void Conversacion::agregarMensaje(Mensaje* m){
    this->mensajes->push_back(m);
}

void Conversacion::borrarMensaje(Mensaje* m){
    this->mensajes->remove(m);
}

void Conversacion::aumentarContador(){
    contador ++;
}

void Conversacion::resetContador(){
    contador = 1;
}

void Conversacion::agregarParticipanteNuevaConversacion(Usuario* u, Usuario *r, Conversacion *c){
    (&dynamic_cast<ConversacionSimple&>(*c))->agregarParticipante(u);
    (&dynamic_cast<ConversacionSimple&>(*c))->agregarParticipante(r);
}

int Conversacion::crearMensaje(DTMensaje* datosMensaje){
    int codigoMensaje;
    Mensaje *m;
    Reloj *r;
    DTFecha *fechaEnviado;
    DTHora *horaEnviado;
    r = r->getInstancia();
    Sesion *s;
    s = s->getInstancia();
    try{
        if((&dynamic_cast<DTMensajeSimple&>(*datosMensaje))!=NULL){
            m = new MensajeSimple((&dynamic_cast<DTMensajeSimple&>(*datosMensaje))->getTexto());
            m->setCodigo(m->getContador());
        }
    }
    catch(bad_cast &errorNoEsSimple){
        try{
            if((&dynamic_cast<DTMensajeContacto&>(*datosMensaje))!=NULL){
                m = new MensajeContacto((&dynamic_cast<DTMensajeContacto&>(*datosMensaje))->getNombre(), (&dynamic_cast<DTMensajeContacto&>(*datosMensaje))->getTelefono());
                m->setCodigo(m->getContador());
            }
        }
        catch(bad_cast &errorNoEsContacto){
            try{
                if((&dynamic_cast<DTMultimediaImagen&>(*datosMensaje))!=NULL){
                    m = new MultimediaImagen((&dynamic_cast<DTMultimediaImagen&>(*datosMensaje))->getUrl(), (&dynamic_cast<DTMultimediaImagen&>(*datosMensaje))->getFormato(), (&dynamic_cast<DTMultimediaImagen&>(*datosMensaje))->getTamanio(), (&dynamic_cast<DTMultimediaImagen&>(*datosMensaje))->getTexto());
                    m->setCodigo(m->getContador());
                }
            }
            catch(bad_cast &errorNoEsImagen){
                m = new MultimediaVideo((&dynamic_cast<DTMultimediaVideo&>(*datosMensaje))->getUrl(), (&dynamic_cast<DTMultimediaVideo&>(*datosMensaje))->getMinutosDuracion(), (&dynamic_cast<DTMultimediaVideo&>(*datosMensaje))->getSegundosDuracion());
                m->setCodigo(m->getContador());
            }
        }
    }
    
    list<Usuario*> *listaParticipantes = this->getParticipantesConversacion();
    for(list<Usuario*>::iterator i = listaParticipantes->begin(); i != listaParticipantes->end(); i++){
        if((*i)->getTelefono() != s->getTelefono()){
            m->agregarReceptor(*i);
            m->crearVisualizacion(*i);
        }
    }
    
    fechaEnviado = new DTFecha(r->getDia(), r->getMes(), r->getAnio());
    horaEnviado = new DTHora(r->getHoras(), r->getMinutos());
    m->setFechaEnviado(fechaEnviado);
    m->setHoraEnviado(horaEnviado);
    m->setEmisor(s->getUsuarioSesion());
    
    this->mensajes->push_back(m);
    codigoMensaje = m->getCodigo();
    if(this->estado == false){
        this->setEstado(true);
    }
    cout << endl;
    cout << "¡Mensaje creado con exito!" << endl;
    sleep(1);
    return codigoMensaje;
}

void Conversacion::agregarReceptor(Usuario *u, int idMensaje){
    for(list<Mensaje*>::iterator i = this->mensajes->begin(); i != this->mensajes->end(); i++){
        if((*i)->getCodigo() == idMensaje){
            (*i)->agregarReceptor(u);
        }
    }
}

void Conversacion::agregarMensajeNuevo(Mensaje* m){
    this->mensajes->push_back(m);
}

//OPERACIONES DE VER MENSAJES

list<DTVisualizacion*> *Conversacion::infoMensajeEnviado(int idMensaje){
    list<DTVisualizacion*> *listaVisualizaciones = new list<DTVisualizacion*>;
    for(list<Mensaje*>::iterator i = this->mensajes->begin(); i != this->mensajes->end(); i++){
        if((*i)->getCodigo() == idMensaje){
            listaVisualizaciones = (*i)->infoMensajeEnviado();
            break;
        }
    }
    return listaVisualizaciones;
}

list<DTMensaje*> *Conversacion::obtenerDatosMensajes(Conversacion* c){
    list<DTMensaje*> *listaMensajes = new list<DTMensaje*>;
    DTMensaje *auxMensaje = new DTMensaje();
    Mensaje *m;
    Reloj *r;
    r = r->getInstancia();
    Sesion *s;
    s = s->getInstancia();
    for(list<Mensaje*>::iterator i = c->getMensajes()->begin(); i != c->getMensajes()->end(); i++){
        for(list<Visualizacion*>::iterator j = (*i)->getVisualizaciones()->begin(); j != (*i)->getVisualizaciones()->end(); j++){
            if((*j)->getUsuarioVisto()->getTelefono() == s->getUsuarioSesion()->getTelefono()){
                (*j)->setHoraVisualizacion(r->getHora());
                (*j)->setFechaVisualizacion(r->getFecha());
                (*j)->setVisto(true);
            }
        }    
        m = (*i);
        auxMensaje = m->getDatos(m);
        listaMensajes->push_back(auxMensaje);
    }
    return listaMensajes;
}