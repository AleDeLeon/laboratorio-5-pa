#include "Mensaje.h"
#include "MultimediaImagen.h"
#include "Usuario.h"
#include "Sesion.h"

//CONSTRUCTORES

MultimediaImagen::MultimediaImagen(string Url, string Formato, float Tamanio, string Texto):Mensaje(){
    this->url = Url;
    this->formato = Formato;
    this->tamanio = Tamanio;
    this->texto = Texto;
}

MultimediaImagen::MultimediaImagen() {
}

MultimediaImagen::MultimediaImagen(const MultimediaImagen& orig) {
}

//DESTRUCTORES

MultimediaImagen::~MultimediaImagen() {
}

//GETTERS

string MultimediaImagen::getUrl(){
    return this->url;
}

string MultimediaImagen::getFormato(){
    return this->formato;
}

float MultimediaImagen::getTamanio(){
    return this->tamanio;
}

string MultimediaImagen::getTexto(){
    return this->texto;
}

//SETTERS

void MultimediaImagen::setUrl(string Url){
    this->url = Url;
}

void MultimediaImagen::setFormato(string Formato){
    this->formato = Formato;
}

void MultimediaImagen::setTamanio(float Tamanio){
    this->tamanio = Tamanio;
}

void MultimediaImagen::setTexto(string Texto){
    this->texto = Texto;
}

//OPERACIONES DE VER MENSAJE

DTMensaje *MultimediaImagen::getDatos(Mensaje* m){
    DTMensaje *datosMensaje;
    DTUsuario *datosEmisor;
    DTUsuario *auxReceptor;
    DTVisualizacion *auxVisualizacion;
    DTUsuario *datosUsuarioVisto;
    list<DTUsuario*> *datosReceptores = new list<DTUsuario*>;
    list<DTVisualizacion*> *datosVisualizaciones = new list<DTVisualizacion*>;
    for(list<Usuario*>::iterator i = m->getReceptores()->begin(); i != m->getReceptores()->end(); i++){
        auxReceptor = new DTUsuario((*i)->getTelefono(), (*i)->getNombre(), (*i)->getImagenDePerfil(), (*i)->getDescripcion());
        datosReceptores->push_back(auxReceptor);
    }
    
    for(list<Visualizacion*>::iterator j = m->getVisualizaciones()->begin(); j != m->getVisualizaciones()->end(); j++){
        datosUsuarioVisto = new DTUsuario((*j)->getUsuarioVisto()->getTelefono(), (*j)->getUsuarioVisto()->getNombre(), (*j)->getUsuarioVisto()->getImagenDePerfil(), (*j)->getUsuarioVisto()->getDescripcion());
        auxVisualizacion = new DTVisualizacion((*j)->getVisto(), (*j)->getHoraVisualizacion(), (*j)->getFechaVisualizacion(), datosUsuarioVisto);
        datosVisualizaciones->push_back(auxVisualizacion);
    }
    
    datosEmisor = new DTUsuario(m->getEmisor()->getTelefono(), m->getEmisor()->getNombre(), m->getEmisor()->getImagenDePerfil(), m->getEmisor()->getDescripcion());
    datosMensaje = new DTMultimediaImagen((&dynamic_cast<MultimediaImagen&>(*m))->getUrl(), (&dynamic_cast<MultimediaImagen&>(*m))->getFormato(), (&dynamic_cast<MultimediaImagen&>(*m))->getTamanio(), (&dynamic_cast<MultimediaImagen&>(*m))->getTexto(), m->getCodigo(), m->getFechaEnviado(), m->getHoraEnviado(), datosEmisor, datosReceptores, datosVisualizaciones);
    return datosMensaje;
}