#ifndef MENSAJE_H
#define MENSAJE_H

#include "../DataTypes/DTFecha.h"
#include "../DataTypes/DTHora.h"
#include "../DataTypes/DTMensaje.h"
#include "../DataTypes/DTVisualizacion.h"

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

using namespace std;

class Conversacion;
class Usuario;
class Visualizacion;

class Mensaje {
public:
    Mensaje();
    Mensaje(const Mensaje& orig);
    
    virtual ~Mensaje();
    
    int getCodigo();
    DTHora *getHoraEnviado();
    DTFecha *getFechaEnviado();
    list<Usuario*> *getReceptores();
    Usuario *getEmisor();
    list<Visualizacion*> *getVisualizaciones();
    int getContador();
    
    void setCodigo(int Codigo);
    void setHoraEnviado(DTHora *HoraEnviado);
    void setFechaEnviado(DTFecha *FechaEnviado);
    void setEmisor(Usuario *Emisor);
    
    void agregarReceptor(Usuario *u);
    void borrarReceptor(Usuario *u);
    void agregarVisualizacion(Visualizacion *v);
    void borrarVisualizacion(Visualizacion *v);
    
    void aumentarContador();
    void resetContador();
    
    //OPERACIONES DE ENVIAR MENSAJE
    void crearVisualizacion(Usuario *u);
    
    //OPERACIONES VER MENSAJE
    list<DTVisualizacion*> *infoMensajeEnviado();
    virtual DTMensaje *getDatos(Mensaje *m) = 0;
private:
    int codigo;
    DTHora *horaEnviado;
    DTFecha *fechaEnviado;
    list<Visualizacion*> *visualizaciones;
    list<Usuario*> *receptores;
    Usuario *emisor;
    static int contador;
};

#endif

