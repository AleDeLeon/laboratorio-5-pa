#ifndef MENU_H
#define MENU_H

#include "ControllerUsuarioFactory.h"
#include "IControllerUsuario.h"
#include "IControllerMensaje.h"
#include "Sesion.h"
/*#include "Conversacion.h"
#include "ConversacionGrupo.h"
#include "ConversacionSimple.h"
#include "Grupo.h"
#include "Visualizacion.h"
#include "Usuario.h"*/
#include "Reloj.h"

#include "../DataTypes/DTConexion.h"
#include "../DataTypes/DTUsuario.h"
#include "../DataTypes/DTConversacion.h"
#include "../DataTypes/DTConversacionSimple.h"
#include "../DataTypes/DTConversacionGrupo.h"
#include "../DataTypes/DTMensaje.h"
#include "../DataTypes/DTMensajeSimple.h"
#include "../DataTypes/DTMensajeContacto.h"
#include "../DataTypes/DTMultimediaImagen.h"
#include "../DataTypes/DTMultimediaVideo.h"
#include "../DataTypes/DTFecha.h"
#include "../DataTypes/DTHora.h"
#include "../DataTypes/DTVisualizacion.h"

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <unistd.h>
#include <list>

using namespace std;

class Menu {
public:
    /*
     DIAGRAMAS MODIFICADOS:
     * agregarContacto
     * listarConversaciones
     * elegirConversacion 
     * dss alta grupo
     * listarConversacionesArchivadas
     */
    static Menu *getInstancia();
    
    //OPERACIONES DEL SISTEMA
    void abrirGuasapFING();
    void cerrarGuasapFING();
    void agregarContacto();
    void modificarUsuario();
    void altaGrupo();
    void enviarMensaje();
    void verMensaje();
    void eliminarMensaje();
    
    //OTROS
    void cargarDatosDePrueba();
    void consultarReloj();
    void modificarReloj();
    void interaccionMenuPrincipal();
    void interaccionMenuUsuario();
    void interaccionMenuReloj();
private:
    Menu();
    static Menu *instancia;
};

#endif 

