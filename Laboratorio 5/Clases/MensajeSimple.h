#ifndef MENSAJESIMPLE_H
#define MENSAJESIMPLE_H

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

#include "Sesion.h"

#include "../DataTypes/DTMensaje.h"
#include "../DataTypes/DTMensajeSimple.h"
#include "../DataTypes/DTVisualizacion.h"

using namespace std;

class MensajeSimple:public Mensaje {
public:
    MensajeSimple();
    MensajeSimple(const MensajeSimple& orig);
    MensajeSimple(string Texto);
    
    virtual ~MensajeSimple();
    
    string getTexto();
    
    void setTexto(string Texto);
    
    //OPERACIONES DE VER MENSAJE
    DTMensaje *getDatos(Mensaje *m);
private:
    string texto;
};

#endif

