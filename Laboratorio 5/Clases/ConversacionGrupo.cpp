#include "Conversacion.h"
#include "ConversacionGrupo.h"
#include "Sesion.h"

//CONSTRUCTORES

ConversacionGrupo::ConversacionGrupo(string Nombre):Conversacion(){
    this->nombre = Nombre;
    this->participantes = new list<Usuario*>;
}

ConversacionGrupo::ConversacionGrupo() {
}

ConversacionGrupo::ConversacionGrupo(const ConversacionGrupo& orig) {
}

//DESTRUCTOR

ConversacionGrupo::~ConversacionGrupo() {
}

//GETTERS

string ConversacionGrupo::getNombre(){
    return this->nombre;
}

list<Usuario*> *ConversacionGrupo::getParticipantes(){
    return participantes;
}

list<Usuario*> *ConversacionGrupo::getParticipantesConversacion(){
    return participantes;
}
//SETTERS

void ConversacionGrupo::setNombre(string Nombre){
    this->nombre = Nombre;
}

//OTROS

void ConversacionGrupo::agregarParticipante(Usuario *u){
    this->participantes->push_back(u);
}

void ConversacionGrupo::borrarParticipante(Usuario* u){
    this->participantes->remove(u);
}

//OPERACIONES DE ENVIAR MENSAJE

DTConversacion *ConversacionGrupo::getDatos(Conversacion* c){
    DTConversacion *datos;
    datos = new DTConversacionGrupo((&dynamic_cast<ConversacionGrupo&> (*c))->getNombre(), c->getID(), c->getEstado());
    return datos;
}

list<string> ConversacionGrupo::getTelefonosParticipantes(){
    list<string> telefonosParticipantes;
    for(list<Usuario*>::iterator i = this->participantes->begin(); i!= this->participantes->end(); i++){
        telefonosParticipantes.push_back((*i)->getTelefono());
    }
    return telefonosParticipantes;
}