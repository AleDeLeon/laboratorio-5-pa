#ifndef ICONTROLLERMENSAJE_H
#define ICONTROLLERMENSAJE_H

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

#include "../DataTypes/DTConversacion.h"
#include "../DataTypes/DTMensaje.h"
#include "../DataTypes/DTContacto.h"
#include "../DataTypes/DTVisualizacion.h"

using namespace std;

class IControllerMensaje {
public:
    IControllerMensaje();
    IControllerMensaje(const IControllerMensaje& orig);
    
    //OPERACIONES DE ENVIAR MENSAJE
    virtual list<DTConversacion*> *listarConversaciones() = 0;
    virtual list<DTMensaje*> *listarMensajes(int idConversacion) = 0;
    virtual list<DTConversacion*> *listarConversacionesArchivadas() = 0;
    virtual list<DTContacto*> *listarContactos() = 0;
    virtual int crearConversacion(string Telefono) = 0;
    virtual void crearMensaje(DTMensaje *datosMensaje, int idConversacion) = 0;
    virtual bool elegirConversacion(int idConversacion) = 0;
    
    //OPERACIONES DE VER MENSAJE
    virtual list<DTVisualizacion*> *infoMensajeEnviado(int idMensaje, int idConversacion) = 0;
    virtual DTContacto *listarConversaciones(string telefono) = 0;
    
    //OPERACIONES DE ELIMINAR MENSAJE
    virtual void eliminarMensaje(int idMensaje) = 0;
    
    virtual ~IControllerMensaje();
};

#endif 

