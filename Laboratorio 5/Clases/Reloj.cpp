#include "Reloj.h"

Reloj *Reloj::instancia = NULL;

//CONSTRUCTOR

Reloj::Reloj(int Dia, int Mes, int Anio, int Horas, int Minutos) {
    dia = Dia;
    mes = Mes;
    anio = Anio;
    horas = Horas;
    minutos = Minutos;
}

//GETTERS

Reloj *Reloj::getInstancia(){
    if(instancia == NULL){
        instancia = new Reloj(0, 0, 0, 0, 0);
    }
    return instancia;
}

int Reloj::getDia(){
    return dia;
}

int Reloj::getMes(){
    return mes;
}

int Reloj::getAnio(){
    return anio;
}

int Reloj::getHoras(){
    return horas;
}

int Reloj::getMinutos(){
    return minutos;
}

DTFecha *Reloj::getFecha(){
    DTFecha *fecha;
    fecha = new DTFecha(this->dia, this->mes, this->anio);
    return fecha;
}

DTHora *Reloj::getHora(){
    DTHora *hora;
    hora = new DTHora(this->horas, this->minutos);
    return hora;
}

//OTROS

void Reloj::consultarReloj(){
    cout << "Dia: " << dia << endl;
    cout << "Mes: " << mes << endl;
    cout << "Año: " << anio << endl;
    cout << "Minutos: " << minutos << endl;
    cout << "Horas: " << horas << endl;
}

void Reloj::modificarReloj(int Dia, int Mes, int Anio, int Horas, int Minutos){
    dia = Dia;
    mes = Mes;
    anio = Anio;
    minutos = Minutos;
    horas = Horas;
}