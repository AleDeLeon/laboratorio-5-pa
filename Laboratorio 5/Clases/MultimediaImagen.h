#ifndef MULTIMEDIAIMAGEN_H
#define MULTIMEDIAIMAGEN_H

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

#include "../DataTypes/DTMensaje.h"
#include "../DataTypes/DTMultimediaImagen.h"
#include "../DataTypes/DTVisualizacion.h"

using namespace std;

class MultimediaImagen:public Mensaje {
public:
    MultimediaImagen();
    MultimediaImagen(string Url, string Formato, float Tamanio, string Texto);
    MultimediaImagen(const MultimediaImagen& orig);
    
    virtual ~MultimediaImagen();
    
    string getUrl();
    string getFormato();
    float getTamanio();
    string getTexto();
    
    void setUrl(string Url);
    void setFormato(string Formato);
    void setTamanio(float Tamanio);
    void setTexto(string Texto);
    
    //OPERACIONES DE VER MENSAJES    
    DTMensaje *getDatos(Mensaje *m);
private:
    string url;
    string formato;
    float tamanio;
    string texto;
};

#endif
