#ifndef COLECCIONUSUARIOS_H
#define COLECCIONUSUARIOS_H

#include "Usuario.h"

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

using namespace std;

class ColeccionUsuarios {
public:
    static ColeccionUsuarios *getInstancia();
    list<Usuario*> *getListaUsuarios();
    
    void agregarUsuario(Usuario *u);
    list<Usuario*>::iterator borrarUsuario(list<Usuario*>::iterator i);
private:
    static ColeccionUsuarios *instancia;
    list<Usuario*> *usuarios;
    ColeccionUsuarios(); //dsadas
};

#endif

