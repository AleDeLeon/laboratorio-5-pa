#ifndef SESION_H
#define SESION_H

#include "Usuario.h"

#include "../DataTypes/DTConversacion.h"
#include "../DataTypes/DTMensaje.h"
#include "../DataTypes/DTContacto.h"
#include "../DataTypes/DTVisualizacion.h"

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

using namespace std;

class Sesion {
public:
    Usuario *getUsuarioSesion();
    static Sesion *getInstancia();
    string getTelefono();
    
    void setUsuarioSesion(Usuario *UsuarioSesion);
    
    //OPERACIONES DE CERRAR GUASAPFING
    void cerrar();
    
    //OPERACIONES DE AGREGAR CONTACO
    void agregarContacto(Usuario *u);
    list<DTContacto*>* getDatosContactos();
    
    //OPERACIONES DE ENVIAR MENSAJE
    void agregarConversacion(Conversacion *c);
    int crearMensaje(DTMensaje *datosMensaje, int idConversacion);
    list<string> getTelefonosParticipantes(int idConversacion);
    list<DTConversacion*> *obtenerDatosConversaciones();
    list<DTConversacion*> *listarConversacionesArchivadas();
    list<DTContacto*> *listarContactos();
    DTContacto *getDatosContacto(string telefono);
    bool elegirConversacion(int idConversacion);
    int crearConversacion(string telefono);
    
    //OPERACIONES DE VER MENSAJE
    list<DTVisualizacion*> *infoMensajeEnviado(int idMensaje, int idConversacion);
    list<DTMensaje*> *listarMensajes(int idConversacion);
    
    //OPERACIONES DE ELIMINAR MENSAJE
    void eliminarMensaje(int idMensaje);
    
private:
    Sesion();
    
    static Sesion *instancia;
    Usuario *usuarioSesion;
};

#endif

