#include "Mensaje.h"
#include "MultimediaVideo.h"
#include "Usuario.h"

//CONSTRUCTORES

MultimediaVideo::MultimediaVideo(string Url, int MinutosDuracion, int SegundosDuracion):Mensaje(){
    this->url = Url;
    this->minutosDuracion = MinutosDuracion;
    this->segundosDuracion = SegundosDuracion;
}

MultimediaVideo::MultimediaVideo() {
}

MultimediaVideo::MultimediaVideo(const MultimediaVideo& orig) {
}

//DESTRUCORES

MultimediaVideo::~MultimediaVideo() {
}

//GETTERS

string MultimediaVideo::getUrl(){
    return this->url;
}

int MultimediaVideo::getMinutosDuracion(){
    return this->minutosDuracion;
}

int MultimediaVideo::getSegundosDuarcion(){
    return this->segundosDuracion;
}

//SETTERS

void MultimediaVideo::setUrl(string Url){
    this->url = Url;
}

void MultimediaVideo::setMinutosDuracion(int MinutosDuracion){
    this->minutosDuracion = MinutosDuracion;
}

void MultimediaVideo::setSegundosDuracion(int SegundosDuracion){
    this->segundosDuracion = SegundosDuracion;
}

//OPERACIONES DE VER MENSAJE

DTMensaje* MultimediaVideo::getDatos(Mensaje* m){
    DTMensaje *datosMensaje;
    DTUsuario *datosEmisor;
    DTUsuario *auxReceptor;
    DTVisualizacion *auxVisualizacion;
    DTUsuario *datosUsuarioVisto;
    list<DTUsuario*> *datosReceptores = new list<DTUsuario*>;
    list<DTVisualizacion*> *datosVisualizaciones = new list<DTVisualizacion*>;
    for(list<Usuario*>::iterator i = m->getReceptores()->begin(); i != m->getReceptores()->end(); i++){
        auxReceptor = new DTUsuario((*i)->getTelefono(), (*i)->getNombre(), (*i)->getImagenDePerfil(), (*i)->getDescripcion());
        datosReceptores->push_back(auxReceptor);
    }
    
    for(list<Visualizacion*>::iterator j = m->getVisualizaciones()->begin(); j != m->getVisualizaciones()->end(); j++){
        datosUsuarioVisto = new DTUsuario((*j)->getUsuarioVisto()->getTelefono(), (*j)->getUsuarioVisto()->getNombre(), (*j)->getUsuarioVisto()->getImagenDePerfil(), (*j)->getUsuarioVisto()->getDescripcion());
        auxVisualizacion = new DTVisualizacion((*j)->getVisto(), (*j)->getHoraVisualizacion(), (*j)->getFechaVisualizacion(), datosUsuarioVisto);
        datosVisualizaciones->push_back(auxVisualizacion);
    }
    datosEmisor = new DTUsuario(m->getEmisor()->getTelefono(), m->getEmisor()->getNombre(), m->getEmisor()->getImagenDePerfil(), m->getEmisor()->getDescripcion());
    datosMensaje = new DTMultimediaVideo((&dynamic_cast<MultimediaVideo&>(*m))->getUrl(), (&dynamic_cast<MultimediaVideo&>(*m))->getMinutosDuracion(), (&dynamic_cast<MultimediaVideo&>(*m))->getSegundosDuarcion(), m->getCodigo(), m->getFechaEnviado(), m->getHoraEnviado(), datosEmisor, datosReceptores, datosVisualizaciones);
    return datosMensaje;
}