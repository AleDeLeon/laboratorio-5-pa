#ifndef CONVERSACIONGRUPO_H
#define CONVERSACIONGRUPO_H

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

#include "Usuario.h"

using namespace std;

class ConversacionGrupo:public Conversacion {
public:
    ConversacionGrupo(string Nombre);
    ConversacionGrupo();
    ConversacionGrupo(const ConversacionGrupo& orig);
    
    virtual ~ConversacionGrupo();
    
    string getNombre();
    list<Usuario*> *getParticipantes();
    list<Usuario*> *getParticipantesConversacion();
    
    void setNombre(string Nombre);
    
    void agregarParticipante(Usuario *u);
    void borrarParticipante(Usuario *u);
    
    //OPERACIONES DE ENVIAR MENSAJE
    DTConversacion *getDatos(Conversacion *c);
    list<string> getTelefonosParticipantes();
private:
    list<Usuario*> *participantes;
    string nombre;
};

#endif

