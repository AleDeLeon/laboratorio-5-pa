#ifndef MENSAJECONTACTO_H
#define MENSAJECONTACTO_H

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

#include "Sesion.h"

#include "../DataTypes/DTMensaje.h"
#include "../DataTypes/DTMensajeContacto.h"
#include "../DataTypes/DTVisualizacion.h"

using namespace std;

class MensajeContacto:public Mensaje {
public:
    MensajeContacto();
    MensajeContacto(const MensajeContacto& orig);
    MensajeContacto(string Nombre, string Telefono);
    
    virtual ~MensajeContacto();
    
    string getNombre();
    string getTelefono();
    
    void setNombre(string Nombre);
    void setTelefono(string Telefono);
    
    //OPERACIONES DE VER MENSAJES
    DTMensaje *getDatos(Mensaje *m);
private:
    string nombre;
    string telefono;
};

#endif

