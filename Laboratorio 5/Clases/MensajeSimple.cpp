#include "Mensaje.h"
#include "MensajeSimple.h"
#include "Usuario.h"

//CONSTRUCTORES 

MensajeSimple::MensajeSimple(string Texto):Mensaje(){
    this->texto = Texto;
}

MensajeSimple::MensajeSimple() {
}

MensajeSimple::MensajeSimple(const MensajeSimple& orig) {
}

//DESTRUCTORES

MensajeSimple::~MensajeSimple() {
}

//GETTERS

string MensajeSimple::getTexto(){
    return this->texto;
}

//SETTERS

void MensajeSimple::setTexto(string Texto){
    this->texto = Texto;
}

//OPERACIONES DE VER MENSAJE

DTMensaje *MensajeSimple::getDatos(Mensaje* m){
    DTMensaje *datosMensaje;
    DTUsuario *datosEmisor;
    DTUsuario *auxReceptor;
    DTVisualizacion *auxVisualizacion;
    DTUsuario *datosUsuarioVisto;
    list<DTUsuario*> *datosReceptores = new list<DTUsuario*>;
    list<DTVisualizacion*> *datosVisualizaciones = new list<DTVisualizacion*>;
    for(list<Usuario*>::iterator i = m->getReceptores()->begin(); i != m->getReceptores()->end(); i++){
        auxReceptor = new DTUsuario((*i)->getTelefono(), (*i)->getNombre(), (*i)->getImagenDePerfil(), (*i)->getDescripcion());
        datosReceptores->push_back(auxReceptor);
    }
    for(list<Visualizacion*>::iterator j = m->getVisualizaciones()->begin(); j != m->getVisualizaciones()->end(); j++){
        datosUsuarioVisto = new DTUsuario((*j)->getUsuarioVisto()->getTelefono(), (*j)->getUsuarioVisto()->getNombre(), (*j)->getUsuarioVisto()->getImagenDePerfil(), (*j)->getUsuarioVisto()->getDescripcion());
        auxVisualizacion = new DTVisualizacion((*j)->getVisto(), (*j)->getHoraVisualizacion(), (*j)->getFechaVisualizacion(), datosUsuarioVisto);
        datosVisualizaciones->push_back(auxVisualizacion);
    }
    
    datosEmisor = new DTUsuario(m->getEmisor()->getTelefono(), m->getEmisor()->getNombre(), m->getEmisor()->getImagenDePerfil(), m->getEmisor()->getDescripcion());
    datosMensaje = new DTMensajeSimple((&dynamic_cast<MensajeSimple&>(*m))->getTexto(), m->getCodigo(), m->getFechaEnviado(), m->getHoraEnviado(), datosEmisor, datosReceptores, datosVisualizaciones);
    return datosMensaje;
}