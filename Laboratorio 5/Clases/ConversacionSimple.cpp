#include "Conversacion.h"
#include "ConversacionSimple.h"
#include "Sesion.h"

//CONSTRUCTORES

ConversacionSimple::ConversacionSimple():Conversacion(){
    this->participantes = new list<Usuario*>;
}

ConversacionSimple::ConversacionSimple(const ConversacionSimple& orig) {
}

//DESTRUCTOR

ConversacionSimple::~ConversacionSimple() {
}

//GETTERS
list<Usuario*> *ConversacionSimple::getParticipantes(){
    return this->participantes;
}

list<Usuario*> *ConversacionSimple::getParticipantesConversacion(){
    return participantes;
}
//OTROS

void ConversacionSimple::agregarParticipante(Usuario* u){
    if(this->participantes->size() <= 2){
        this->participantes->push_back(u);
    }
    else{
        cout << "ERROR: Esta clase ya tiene 2 participantes" << endl;
    }
}

void ConversacionSimple::borrarParticipante(Usuario* u){
    this->participantes->remove(u);
}

//OPERACIONES DE ENVIAR MENSAJE

DTConversacion *ConversacionSimple::getDatos(Conversacion* c){
    Sesion *s;
    s = s->getInstancia();
    DTConversacion *datos;
    string nombreParticipante;
    string telefonoParticipante;
    for(list<Usuario*>::iterator i = (&dynamic_cast<ConversacionSimple&>(*c))->getParticipantes()->begin(); i != (&dynamic_cast<ConversacionSimple&>(*c))->getParticipantes()->end(); i++){
        if((*i)->getTelefono() != s->getUsuarioSesion()->getTelefono()){
            nombreParticipante = (*i)->getNombre();
            telefonoParticipante = (*i)->getTelefono();
        }
    }
    datos = new DTConversacionSimple(nombreParticipante, telefonoParticipante, c->getID(), c->getEstado());
    return datos;
}

list<string> ConversacionSimple::getTelefonosParticipantes(){
    list<string> telefonosParticipantes;
    for(list<Usuario*>::iterator i = this->participantes->begin(); i!= this->participantes->end(); i++){
        telefonosParticipantes.push_back((*i)->getTelefono());
    }
    return telefonosParticipantes;
}