#ifndef CONVERSACION_H
#define CONVERSACION_H

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

#include "Reloj.h"
#include "Visualizacion.h"

#include "../DataTypes/DTConversacion.h"
#include "../DataTypes/DTConversacionGrupo.h"
#include "../DataTypes/DTConversacionSimple.h"
#include "../DataTypes/DTMensaje.h"
#include "../DataTypes/DTMensajeSimple.h"
#include "../DataTypes/DTMensajeContacto.h"
#include "../DataTypes/DTMultimediaImagen.h"
#include "../DataTypes/DTMultimediaVideo.h"
#include "../DataTypes/DTVisualizacion.h"

using namespace std;

class Usuario;
class Visualizacion;
class Mensaje;

class Conversacion {
public:
    
    Conversacion();
    Conversacion(const Conversacion& orig);
    
    virtual ~Conversacion();
    
    int getID();
    bool getEstado();
    list<Mensaje*> *getMensajes();
    int getContadorID();
    virtual list<Usuario*> *getParticipantesConversacion() = 0;
    Mensaje *getMensaje(int idMensaje);
    
    void setID(int ID);
    void setEstado(bool Estado);
    
    void agregarMensaje(Mensaje *m);
    void borrarMensaje(Mensaje *m);
    
    void aumentarContador();
    void resetContador();
    
    //OPERACIONES DE ENVIAR MENSAJE
    virtual DTConversacion *getDatos(Conversacion *c) = 0;
    virtual list<string> getTelefonosParticipantes() = 0;
    
    void agregarParticipanteNuevaConversacion(Usuario *u, Usuario *r, Conversacion *c);
    int crearMensaje(DTMensaje *datosMensaje);
    void agregarReceptor(Usuario *u, int idMensaje);
    void agregarMensajeNuevo(Mensaje *m);
    
    //OPERACIONES DE VER MENSAJE
    list<DTVisualizacion*> *infoMensajeEnviado(int idMensaje);
    list<DTMensaje*> *obtenerDatosMensajes(Conversacion *c);
    
private:
    bool estado; //SI ES TRUE ES ACTIVA Y SI ES FALSE ESTA ARCHIVADA
    int id;
    list<Mensaje*> *mensajes;
    static int contador; //CUENTA LA CANTIDAD DE INSTANCIAS QUE SE HAN CREADO DE CONVERSACION PARA DEFINIR EL ID
};

#endif

