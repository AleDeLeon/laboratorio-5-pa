#include "Usuario.h"
#include "Reloj.h"
#include "ColeccionUsuarios.h"
#include "ConversacionSimple.h"

//CONSTRUCTORES

Usuario::Usuario(string Telefono, string Nombre, string ImagenDePerfil, string Descripcion){
    this->telefono = Telefono;
    this->nombre = Nombre;
    this->imagenDePerfil = ImagenDePerfil;
    this->descripcion = Descripcion;
    this->contactos = new list<Usuario*>;
    this->conversaciones = new list<Conversacion*>;
}

Usuario::Usuario() {
}

Usuario::Usuario(const Usuario& orig) {
}

//DESTRUCTORES

Usuario::~Usuario() {
}

//GETTERS

string Usuario::getTelefono(){
    return this->telefono;  
}

string Usuario::getNombre(){
    return this->nombre;
}

string Usuario::getImagenDePerfil(){
    return this->imagenDePerfil;
}

string Usuario::getDescripcion(){
    return this->descripcion;
}

DTFecha *Usuario::getFechaRegistro(){
    return this->fechaRegistro;
}

DTConexion *Usuario::getUltimaConexion(){
    return this->ultimaConexion;
}

list<Usuario*> *Usuario::getContactos(){
    return this->contactos;
}

list<Conversacion*> *Usuario::getConversaciones(){
    return this->conversaciones;
}

//SETTERS

void Usuario::setTelefono(string Telefono){
    this->telefono = Telefono;
}

void Usuario::setNombre(string Nombre){
    this->nombre = Nombre;
}

void Usuario::setImagenDePerfil(string ImagenDePerfil){
    this->imagenDePerfil = ImagenDePerfil;
}

void Usuario::setDescripcion(string Descripcion){
    this->descripcion = Descripcion;
}

void Usuario::setFechaRegistro(DTFecha *FechaRegistro){
    this->fechaRegistro = FechaRegistro;
}

void Usuario::setUltimaConexion(DTConexion *UltimaConexion){
    this->ultimaConexion = UltimaConexion;
}

//OTROS

void Usuario::agregarContacto(Usuario* u){
    this->contactos->push_back(u);
}

void Usuario::agregarConversacion(Conversacion* c){
    this->conversaciones->push_back(c);
}

void Usuario::borrarContacto(Usuario* u){
    this->contactos->remove(u);
}

void Usuario::borrarConversacion(Conversacion* c){
    this->conversaciones->remove(c);
}

//AGREGAR CONTACTO

DTContacto *Usuario::getDatos(){
    return new DTContacto(this->getTelefono(), this->getNombre());
}

//OPERACIONES DE ENVIAR MENSAJE

list<DTConversacion*> *Usuario::obtenerDatosConversaciones(){
    list<DTConversacion*> *listaConversaciones = new list<DTConversacion*>;
    DTConversacion *auxConversacion = new DTConversacion();
    Conversacion *c;
    for(list<Conversacion*>::iterator i = this->getConversaciones()->begin(); i != this->getConversaciones()->end(); i++){
        c = (*i);
        auxConversacion = c->getDatos(c);
        listaConversaciones->push_back(auxConversacion);
    }
    return listaConversaciones;
}

list<DTConversacion*> *Usuario::getConversacionesArchivadas(){
    list<DTConversacion*> *listaConversacionesArchivadas = new list<DTConversacion*>;
    DTConversacion *auxConversacion = new DTConversacion();
    Conversacion *c;
    for(list<Conversacion*>::iterator i = this->conversaciones->begin(); i != this->conversaciones->end(); i++){
        if((*i)->getEstado() == false){
            c = (*i);;
            auxConversacion = c->getDatos(c);
            listaConversacionesArchivadas->push_back(auxConversacion);
            
        }
    }
    return listaConversacionesArchivadas;
}

list<DTContacto*> *Usuario::getDatosContactos(){
    list<DTContacto*> *listaContactos = new list<DTContacto*>;
    DTContacto *auxiliarContacto;
    Usuario *c;
    for(list<Usuario*>::iterator i = this->getContactos()->begin(); i != this->getContactos()->end(); i++){
        c = (*i); 
        auxiliarContacto = c->getDatos(c);
        listaContactos->push_back(auxiliarContacto);
    }
    return listaContactos;
}

DTContacto *Usuario::getDatos(Usuario *u){
    DTContacto *datosContacto;
    datosContacto = new DTContacto(u->getTelefono(), u->getNombre());
    return datosContacto;
}

Mensaje *Usuario::getMensaje(int idConversacion, int idMensaje){
    Mensaje *m;
    for(list<Conversacion*>::iterator i = this->conversaciones->begin(); i != this->conversaciones->end(); i++){
        if((*i)->getID() == idConversacion){
            m = (*i)->getMensaje(idMensaje);
        }
    }
    return m;
}

int Usuario::crearMensaje(DTMensaje *datosMensaje, int idConversacion){
    Conversacion *c;
    for(list<Conversacion*>::iterator i = this->conversaciones->begin(); i != this->conversaciones->end(); i++){
        if((*i)->getID() == idConversacion){
            c = (*i);
        }
    }
    int codigoMensaje;
    codigoMensaje = c->crearMensaje(datosMensaje);
    return codigoMensaje;
}

list<string> Usuario::getTelefonosParticipantes(int idConversacion){
    list<string> listaTelefonos;
    for(list<Conversacion*>::iterator i = this->conversaciones->begin(); i != this->conversaciones->end(); i++){
        if((*i)->getID() == idConversacion){
            listaTelefonos = (*i)->getTelefonosParticipantes();
            break;
        }
    }    
    return listaTelefonos;
}

void Usuario::agregarReceptor(Usuario *u, int idMensaje, int idConversacion){
    for(list<Conversacion*>::iterator i = this->conversaciones->begin(); i != this->conversaciones->end(); i++){
        if((*i)->getID() == idConversacion){
            (*i)->agregarReceptor(u, idMensaje);
        }
    }
}

DTContacto *Usuario::getDatosContacto(string telefono){
    DTContacto *datosContacto;
    Usuario *u;
    for(list<Usuario*>::iterator i = this->contactos->begin(); i != this->contactos->end(); i++){
        if((*i)->getTelefono() == telefono){
            u = (*i);
            datosContacto = u->getDatos(u);
            break;
        }
    }
    return datosContacto;
}

bool Usuario::elegirConversacion(int idConversacion){
    bool existe = false;
    for(list<Conversacion*>::iterator i = this->conversaciones->begin(); i != this->conversaciones->end(); i++){
        if((*i)->getID() == idConversacion){
            existe = true;
            sleep(1);
        }
    }
    return existe;
}

int Usuario::crearConversacion(string telefono){
    int idConversacionNueva;
    Conversacion *c;
    c = new ConversacionSimple();
    c->setID(c->getContadorID());
    idConversacionNueva = c->getID();
    Usuario *r;
    ColeccionUsuarios *usuarios;
    usuarios = usuarios->getInstancia();
    
    for(list<Usuario*>::iterator i = usuarios->getListaUsuarios()->begin(); i != usuarios->getListaUsuarios()->end(); i++){
        if((*i)->getTelefono() == telefono){
            r = (*i);
            break;
        }
    }
    
    c->agregarParticipanteNuevaConversacion(this, r, c);
    this->conversaciones->push_back(c);
    return idConversacionNueva;
}

void Usuario::agregarMensajeNuevo(int idConversacion, int idMensaje, string telefonoEmisor){
    
    ColeccionUsuarios *usuarios;
    usuarios = usuarios->getInstancia();
    
    Mensaje *m;
    
    for(list<Usuario*>::iterator i = usuarios->getListaUsuarios()->begin(); i != usuarios->getListaUsuarios()->end(); i++){
        if((*i)->getTelefono() == telefonoEmisor){
            m = (*i)->getMensaje(idConversacion, idMensaje);
        }
    }
    
    for(list<Conversacion*>::iterator j = this->conversaciones->begin(); j != this->conversaciones->end(); j++){
        if((*j)->getID() == idConversacion){
            (*j)->agregarMensajeNuevo(m);
        }
    }
}

//OPERACIONES DE VER MENSAJE

list<DTVisualizacion*> *Usuario::infoMensajeEnviado(int idMensaje, int idConversacion){
    list<DTVisualizacion*> *listaVisualizaciones = new list<DTVisualizacion*>;
    for(list<Conversacion*>::iterator i = this->conversaciones->begin(); i != this->conversaciones->end(); i++){
        if((*i)->getID() == idConversacion){
            listaVisualizaciones = (*i)->infoMensajeEnviado(idMensaje);
            break;
        }
    }
    return listaVisualizaciones;
}

list<DTMensaje*> *Usuario::listarMensajes(int idConversacion){
    list<DTMensaje*> *listaMensajes = new list<DTMensaje*>;
    Conversacion *c;
    for(list<Conversacion*>::iterator i = this->conversaciones->begin(); i != this->conversaciones->end(); i++){
        if((*i)->getID() == idConversacion){
            c = (*i);
            listaMensajes = c->obtenerDatosMensajes(c);
            break;
        }
    }
    return listaMensajes;
}


void Usuario::eliminarMensaje(int idMensaje){
    
}