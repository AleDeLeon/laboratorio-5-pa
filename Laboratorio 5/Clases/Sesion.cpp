#include "Sesion.h"

Sesion *Sesion::instancia = NULL;

//CONSTRUCTORES

Sesion::Sesion() {
    this->usuarioSesion = NULL;
}

//GETTERS

Usuario *Sesion::getUsuarioSesion(){
    return this->usuarioSesion;
}

Sesion *Sesion::getInstancia(){
    if(instancia == NULL){
        instancia = new Sesion();
    }
    return instancia;
}

string Sesion::getTelefono(){
    return this->usuarioSesion->getTelefono();
}

//SETTERS

void Sesion::setUsuarioSesion(Usuario* UsuarioSesion){
    this->usuarioSesion = UsuarioSesion;
}

//OPERACIONES DE CERRAR GUASAPFING

void Sesion::cerrar(){
    this->usuarioSesion = NULL;
}

//OPERACIONES DE AGREGAR CONTACTO
void Sesion::agregarContacto(Usuario *u){
    this->usuarioSesion->agregarContacto(u);
}

list<DTContacto*> *Sesion::getDatosContactos(){
    Usuario *u;
    u = this->usuarioSesion;
    return u->getDatosContactos();
}

//OPERACIONES DE ENVIAR MENSAJE

list<DTConversacion*> *Sesion::obtenerDatosConversaciones(){
    list<DTConversacion*> *listaConversaciones = new list<DTConversacion*>;
    listaConversaciones = this->usuarioSesion->obtenerDatosConversaciones();
    return listaConversaciones;
}

list<DTConversacion*> *Sesion::listarConversacionesArchivadas(){
    list<DTConversacion*> *listaConversacionesArchivadas = new list<DTConversacion*>;
    listaConversacionesArchivadas = this->usuarioSesion->getConversacionesArchivadas();
    return listaConversacionesArchivadas;
}

list<DTContacto*> *Sesion::listarContactos(){
    list<DTContacto*> *listaContactos = new list<DTContacto*>;
    listaContactos = this->usuarioSesion->getDatosContactos();
    return listaContactos;
}

void Sesion::agregarConversacion(Conversacion *c){
    this->usuarioSesion->agregarConversacion(c);
}

int Sesion::crearMensaje(DTMensaje* datosMensaje, int idConversacion){
    int codigoMensaje;
    codigoMensaje = this->usuarioSesion->crearMensaje(datosMensaje, idConversacion);
    return codigoMensaje;
}

list<string> Sesion::getTelefonosParticipantes(int idConversacion){
    list<string> listaTelefonos;
    listaTelefonos = this->usuarioSesion->getTelefonosParticipantes(idConversacion);
    return listaTelefonos;
}

DTContacto *Sesion::getDatosContacto(string telefono){
    DTContacto *datosContacto;
    datosContacto = this->usuarioSesion->getDatosContacto(telefono);
    return datosContacto;
}

bool Sesion::elegirConversacion(int idConversacion){
    bool existe;
    existe = this->usuarioSesion->elegirConversacion(idConversacion);
    return existe;
}

int Sesion::crearConversacion(string telefono){
    int idConversacionNueva;
    idConversacionNueva = this->usuarioSesion->crearConversacion(telefono);
    return idConversacionNueva;
}

//OPERACIONES DE VER MENSAJE

list<DTVisualizacion*> *Sesion::infoMensajeEnviado(int idMensaje, int idConversacion){
    list<DTVisualizacion*> *listaVisualizaciones = new list<DTVisualizacion*>;
    listaVisualizaciones = this->usuarioSesion->infoMensajeEnviado(idMensaje, idConversacion);
    return listaVisualizaciones;
}

list<DTMensaje*> *Sesion::listarMensajes(int idConversacion){
    list<DTMensaje*> *listaMensajes = new list<DTMensaje*>;
    listaMensajes = this->usuarioSesion->listarMensajes(idConversacion);
    return listaMensajes;
}

void Sesion::eliminarMensaje(int idMensaje){
    
    
}