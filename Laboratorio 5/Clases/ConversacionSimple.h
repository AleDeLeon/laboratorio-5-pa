#ifndef CONVERSACIONSIMPLE_H
#define CONVERSACIONSIMPLE_H

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

#include "Usuario.h"

using namespace std;

class ConversacionSimple:public Conversacion {
public:
    ConversacionSimple();
    ConversacionSimple(const ConversacionSimple& orig);
    
    virtual ~ConversacionSimple();
    
    list<Usuario*> *getParticipantesConversacion();
    list<Usuario*> *getParticipantes();
    
    void agregarParticipante(Usuario *u);
    void borrarParticipante(Usuario *u);
    
    //OPERACIONES DE ENVIAR MENSAJE
    DTConversacion *getDatos(Conversacion *c);
    list<string> getTelefonosParticipantes();
private:
    list<Usuario*> *participantes;
};

#endif

