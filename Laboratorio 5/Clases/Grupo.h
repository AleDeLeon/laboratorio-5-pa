#ifndef GRUPO_H
#define GRUPO_H

#include "../DataTypes/DTFecha.h"
#include "../DataTypes/DTHora.h"
#include "Usuario.h"
#include "Conversacion.h"
#include "ConversacionGrupo.h"

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

using namespace std;

class Grupo {
public:
    Grupo(string Nombre, string Imagen);
    Grupo();
    Grupo(const Grupo& orig);
    
    virtual ~Grupo();
    
    string getNombre();
    string getImagen();
    DTFecha *getFechaCreacion();
    DTHora *getHoraCreacion();
    list<Usuario*> *getUsuariosGrupo();
    Usuario *getAdministrador();
    Conversacion *getConversacion();
    
    void setNombre(string Nombre);
    void setImagen(string Imagen);
    void setHoraCreacion(DTHora *HoraCreacion);
    void setFechaCreacion(DTFecha *FechaCreacion);
    void setAdministrador(Usuario *Administrador);
    void setConversacion(Conversacion *ConversacionGrupo);
    
    void agregarUsuario(Usuario *u);
    void borrarUsuario(Usuario *u);
private:
    string nombre;
    string imagen;
    DTFecha *fechaCreacion;
    DTHora *horaCreacion;
    list<Usuario*> *usuariosGrupo;
    Usuario *administrador;
    Conversacion *conversacion;
};

#endif

