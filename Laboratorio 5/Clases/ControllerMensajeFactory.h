#ifndef CONTROLLERMENSAJEFACTORY_H
#define CONTROLLERMENSAJEFACTORY_H

#include "IControllerMensaje.h"

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <typeinfo>
#include <stdio.h>
#include <list>

using namespace std;

class ControllerMensajeFactory {
public:
    ControllerMensajeFactory();
    ControllerMensajeFactory(const ControllerMensajeFactory& orig);
    
    virtual ~ControllerMensajeFactory();
    
    IControllerMensaje *getIControllerMensaje();
};

#endif 

