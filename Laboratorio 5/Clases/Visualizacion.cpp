#include "Visualizacion.h"
#include "Usuario.h"

//CONSTRUCTORES

Visualizacion::Visualizacion(DTHora *HoraVisualizacion, DTFecha *FechaVisualizacion, bool Visto, Usuario *UsuarioVisto){
    this->horaVisualizacion = HoraVisualizacion;
    this->fechaVisualizacion = FechaVisualizacion;
    this->visto = Visto;
    this->usuarioVisto = UsuarioVisto;
}

Visualizacion::Visualizacion() {
}

Visualizacion::Visualizacion(const Visualizacion& orig) {
}

//DESTRUCTOR

Visualizacion::~Visualizacion() {
}

//GETTERS

DTHora *Visualizacion::getHoraVisualizacion(){
    return this->horaVisualizacion;
}

DTFecha *Visualizacion::getFechaVisualizacion(){
    return this->fechaVisualizacion;
}

bool Visualizacion::getVisto(){
    return this->visto;
}

Usuario *Visualizacion::getUsuarioVisto(){
    return this->usuarioVisto;
}


//SETTERS

void Visualizacion::setHoraVisualizacion(DTHora *HoraVisualizacion){
    this->horaVisualizacion = HoraVisualizacion;
}

void Visualizacion::setFechaVisualizacion(DTFecha *HoraVisualizacion){
    this->fechaVisualizacion = HoraVisualizacion;
}

void Visualizacion::setVisto(bool Visto){
    this->visto = Visto;
}

void Visualizacion::setUsuarioVisto(Usuario *UsuarioVisto){
    this->usuarioVisto = UsuarioVisto;
}

//OPERACIONES DE VER MENSAJES

DTVisualizacion *Visualizacion::getDatos(){
    DTVisualizacion *datosVisualizacion;
    DTUsuario *usuarioVisualizacion;
    usuarioVisualizacion = new DTUsuario(this->usuarioVisto->getTelefono(), this->usuarioVisto->getNombre(), this->usuarioVisto->getImagenDePerfil(), this->usuarioVisto->getDescripcion());
    datosVisualizacion = new DTVisualizacion(this->visto, this->getHoraVisualizacion(), this->getFechaVisualizacion(), usuarioVisualizacion);
    return datosVisualizacion;
}