#include "IControllerUsuario.h"
#include "ControllerUsuario.h"
#include "ConversacionSimple.h"

//CONSTRUCTORES

ControllerUsuario::ControllerUsuario() {
}

ControllerUsuario::ControllerUsuario(const ControllerUsuario& orig) {
}

//DESTRUCTOR

ControllerUsuario::~ControllerUsuario() {
}

//OPERACIONES ABRIR GUASAPFING

bool ControllerUsuario::ingresoNumero(string telefono) {
    ColeccionUsuarios *listaUsuarios;
    listaUsuarios = listaUsuarios->getInstancia();
    for (list<Usuario*>::iterator u = listaUsuarios->getListaUsuarios()->begin(); u != listaUsuarios->getListaUsuarios()->end(); u++) {
        if ((*u)->getTelefono() == telefono) {
            return true;
            break;
        }
    }
    return false;
}

void ControllerUsuario::ingresoUsuario(string telefono) {
    ColeccionUsuarios *listaUsuarios;
    listaUsuarios = listaUsuarios->getInstancia();
    DTConexion *auxiliarConexion;
    Sesion *nuevaSesion;
    nuevaSesion = nuevaSesion->getInstancia();
    Reloj *reloj;
    reloj = reloj->getInstancia();
    for (list<Usuario*>::iterator u = listaUsuarios->getListaUsuarios()->begin(); u != listaUsuarios->getListaUsuarios()->end(); u++) {
        if ((*u)->getTelefono() == telefono) {
            nuevaSesion->setUsuarioSesion(*u);
            auxiliarConexion = new DTConexion(reloj->getDia(), reloj->getMes(), reloj->getAnio(), reloj->getMinutos(), reloj->getHoras());
            (*u)->setUltimaConexion(auxiliarConexion);
            cout << endl;
            cout << "    -CONEXION ACTUALIZADA- " << endl;
            cout << "         " << auxiliarConexion->getDia() << "/" << auxiliarConexion->getMes() << "/" << auxiliarConexion->getAnio() << " - " << auxiliarConexion->getHoras() << ":" << auxiliarConexion->getMinutos() << endl;
            cout << endl;
            cout << "  ¡Sesion iniciada con exito!" << endl;
            sleep(2);
        }
    }
}

DTConexion ControllerUsuario::altaUsuario(DTUsuario* nuevoUsuario) {
    Usuario *auxiliarUsuario;
    auxiliarUsuario = new Usuario(nuevoUsuario->getTelefono(), nuevoUsuario->getNombre(), nuevoUsuario->getImagenDePerfil(), nuevoUsuario->getDescripcion());
    ColeccionUsuarios *listaUsuarios;
    listaUsuarios = listaUsuarios->getInstancia();
    listaUsuarios->agregarUsuario(auxiliarUsuario);
    cout << endl;
    cout << "  ¡Usuario agregado con exito!" << endl;
    sleep(1);
}

//OPERACIONES CERRAR GUASAPFING

void ControllerUsuario::cerrar() {
    Sesion *s;
    s = s->getInstancia();
    s->cerrar();
    cout << " ¡Sesion cerrada con exito!" << endl;
}

//OPERACIONES DE AGREGAR CONTACTO

DTContacto *ControllerUsuario::mostrarContacto(string telefono){
    DTContacto *datos;
    ColeccionUsuarios *cu;
    list <Usuario*> *usuarios;
    usuarios = cu->getListaUsuarios();
    for(list<Usuario*> :: iterator u = usuarios->begin(); u != usuarios->end(); u++) {
        if (telefono == (*u)->getTelefono()) {
            datos = (*u)->getDatos();
        }
    }
    return datos;
}

void ControllerUsuario::agregarContacto(string telefono){
    Sesion *s;
    s = s->getInstancia();
    ColeccionUsuarios *cu;
    list<Usuario*> *usuarios;usuarios = cu->getListaUsuarios();
    for(list<Usuario*>:: iterator u = usuarios->begin(); u != usuarios->end(); u++) {
        if(telefono == (*u)->getTelefono()) {
            s->agregarContacto((*u));
        }
        else if (u == (*usuarios).end()) {
            cout << "ERROR: Ya se encuentra en sus contactos" <<endl;
        }
    }
}

list<DTContacto*> *ControllerUsuario::listarContactos(){
    Sesion *s;
    s = s->getInstancia();
    return s->getDatosContactos();
}

//OPERACIONES ALTA GRUPO

list<DTContacto*> *ControllerUsuario::quitarContacto(string telefono, list<DTContacto*> *contactosElegidos) {
    for (list<DTContacto*>::iterator i = contactosElegidos->begin(); i != contactosElegidos->end(); i++) {
        if ((*i)->getTelefono() == telefono) {
            contactosElegidos->remove(*i);
            cout << endl;
            cout << " ¡Contacto quitado!" << endl;
            break;
        }
    }
    return contactosElegidos;
}

list<DTContacto*> *ControllerUsuario::listarContactosRestantes(list<string> telefonosElegidos) {
    list<DTContacto*> *contactosRestantes = new list<DTContacto*>;
    DTContacto *contactoNoElegido;
    Sesion *s;
    s = s->getInstancia();
    bool elegido = false;

    for (list<Usuario*>::iterator contacto = s->getUsuarioSesion()->getContactos()->begin(); contacto != s->getUsuarioSesion()->getContactos()->end(); contacto++) {
        for (list<string>::iterator telefono = telefonosElegidos.begin(); telefono != telefonosElegidos.end(); telefono++) {
            if ((*contacto)->getTelefono() == (*telefono)) {
                elegido = true;
            }
        }
        if (elegido == false) {
            contactoNoElegido = new DTContacto((*contacto)->getTelefono(), (*contacto)->getNombre());
            contactosRestantes->push_back(contactoNoElegido);
        }
        elegido = false;
    }

    return contactosRestantes;
}

list<DTContacto*> *ControllerUsuario::agregarContacto(string telefono, list<DTContacto*> *contactosElegidos) {
    Sesion *s;
    s = s->getInstancia();
    DTContacto *contactoNuevo;
    bool existeContacto = false;

    for (list<Usuario*>::iterator i = s->getUsuarioSesion()->getContactos()->begin(); i != s->getUsuarioSesion()->getContactos()->end(); i++) {
        if ((*i)->getTelefono() == telefono) {
            contactoNuevo = new DTContacto((*i)->getTelefono(), (*i)->getNombre());
            existeContacto = true;
        }
    }

    if (existeContacto == true) {
        contactosElegidos->push_back(contactoNuevo);
        cout << endl;
        cout << "  ¡Contacto agregado!" << endl;
        sleep(1);
    } else {
        cout << "ERROR: el numero no está" << endl;
        cout << "en su lista de contactos" << endl;
    }
    return contactosElegidos;
}

void ControllerUsuario::altaGrupo(DTGrupo *datosGrupo, list<DTContacto*> *contactosElegidos) {
    Sesion *s;
    s = s->getInstancia();
    Reloj *r;
    r = r->getInstancia();
    DTFecha *fecha;
    DTHora *hora;
    ColeccionUsuarios *usuarios;
    usuarios = usuarios->getInstancia();
    ColeccionGrupos *grupos;
    grupos = grupos->getInstancia();

    Grupo *nuevoGrupo;
    nuevoGrupo = new Grupo(datosGrupo->getNombre(), datosGrupo->getImagen());
    fecha = new DTFecha(r->getDia(), r->getMes(), r->getAnio());
    hora = new DTHora(r->getHoras(), r->getMinutos());
    nuevoGrupo->setFechaCreacion(fecha);
    nuevoGrupo->setHoraCreacion(hora);
    nuevoGrupo->setAdministrador(s->getUsuarioSesion());

    Conversacion *nuevaConversacion;

    list<DTContacto*>::iterator elegidos = contactosElegidos->begin();

    for (list<DTContacto*>::iterator elegidos = contactosElegidos->begin(); elegidos != contactosElegidos->end(); elegidos++) {
        for (list<Usuario*>::iterator u = usuarios->getListaUsuarios()->begin(); u != usuarios->getListaUsuarios()->end(); u++) {
            if ((*u)->getTelefono() == (*elegidos)->getTelefono()) {
                nuevaConversacion = new ConversacionGrupo(datosGrupo->getNombre());
                nuevaConversacion->setID(nuevaConversacion->getContadorID());
                for (list<DTContacto*>::iterator elegidos = contactosElegidos->begin(); elegidos != contactosElegidos->end(); elegidos++) {
                    for (list<Usuario*>::iterator u = usuarios->getListaUsuarios()->begin(); u != usuarios->getListaUsuarios()->end(); u++) {
                        if ((*u)->getTelefono() == (*elegidos)->getTelefono()) {
                            (&dynamic_cast<ConversacionGrupo&> (*nuevaConversacion))->agregarParticipante(*u);
                        }
                    }
                }
                nuevoGrupo->agregarUsuario(*u);
                (*u)->agregarConversacion(nuevaConversacion);
            }
        }
    }
    nuevaConversacion = new ConversacionGrupo(datosGrupo->getNombre());
    nuevaConversacion->setID(nuevaConversacion->getContadorID());
    for (list<DTContacto*>::iterator elegidos = contactosElegidos->begin(); elegidos != contactosElegidos->end(); elegidos++) {
        for (list<Usuario*>::iterator u = usuarios->getListaUsuarios()->begin(); u != usuarios->getListaUsuarios()->end(); u++) {
            if ((*u)->getTelefono() == (*elegidos)->getTelefono()) {
                (&dynamic_cast<ConversacionGrupo&> (*nuevaConversacion))->agregarParticipante(*u);
            }
        }
    }
    s->getUsuarioSesion()->agregarConversacion(nuevaConversacion);
    grupos->getListaGrupos()->push_back(nuevoGrupo);
    cout << endl;
    nuevaConversacion->aumentarContador();
    cout << " ¡Grupo creado con exito!" << endl;
    sleep(1);
}

//OPERACIONES DE MODIFICAR USUARIO

void ControllerUsuario::cambiarNombre(string nuevoNombre) {
    Sesion *s;
    s = s->getInstancia();
    s->getUsuarioSesion()->setNombre(nuevoNombre);
    cout << endl;
    cout << "¡Nombre cambiado con exito!" << endl;
    sleep(1);
}

void ControllerUsuario::cambiarImagenDePerfil(string nuevaImagen) {
    Sesion *s;
    s = s->getInstancia();
    s->getUsuarioSesion()->setImagenDePerfil(nuevaImagen);
    cout << endl;
    cout << "¡Imagén cambiada con exito!" << endl;
    sleep(1);
}

void ControllerUsuario::cambiarDescripcion(string nuevaDescripcion) {
    Sesion *s;
    s = s->getInstancia();
    s->getUsuarioSesion()->setDescripcion(nuevaDescripcion);
    cout << endl;
    cout << "¡Descripción cambiada con exito!" << endl;
    sleep(1);
}

//CARGAR DATOS DE PRUEBA

void ControllerUsuario::cargarDatosDePrueba() {
    DTHora *hora;
    DTFecha *fecha;
    ColeccionUsuarios *listaUsuarios;
    listaUsuarios = listaUsuarios->getInstancia();
    ColeccionGrupos *listaGrupos;
    listaGrupos = listaGrupos->getInstancia();
    Sesion *sesion;
    sesion = sesion->getInstancia();
    list<Usuario*>::iterator i1 = listaUsuarios->getListaUsuarios()->begin();
    list<Grupo*>::iterator i2 = listaGrupos->getListaGrupos()->begin();

    //VACIAMOS LAS LISTAR DE USUARIOS Y DE GRUPOS PARA REEMPLAZAR LOS VIEJOS DATOS CON LOS NUEVOS

    sesion->cerrar();

    while (listaUsuarios->getListaUsuarios()->size() != 0) {
        i1 = listaUsuarios->borrarUsuario(i1);
    }

    while (listaGrupos->getListaGrupos()->size() != 0) {
        i2 = listaGrupos->borrarGrupo(i2);
    }

    //CREACION DE USUARIOS
    Usuario *u1;
    Usuario *u2;
    Usuario *u3;
    Usuario *u4;

    u1 = new Usuario("090123654", "Juan Pérez", "home/img/perfil/juan.png", "Amo usar Guasap");
    u2 = new Usuario("090765432", "María Fernández", "home/img/perfil/maria.png", "Me encanta ProgAv");
    u3 = new Usuario("090246810", "Pablo Iglesias", "home/img/perfil/pablo.png", "Me gustan los animales");
    u4 = new Usuario("090666777", "Sara Ruiz", "home/img/perfil/sara.png", "¡Estoy feliz!");

    listaUsuarios->agregarUsuario(u1);
    listaUsuarios->agregarUsuario(u2);
    listaUsuarios->agregarUsuario(u3);
    listaUsuarios->agregarUsuario(u4);

    u1->agregarContacto(u2);
    u1->agregarContacto(u3);
    u1->agregarContacto(u4);

    u2->agregarContacto(u1);
    u2->agregarContacto(u3);

    u3->agregarContacto(u1);
    u3->agregarContacto(u2);
    u3->agregarContacto(u4);

    u4->agregarContacto(u1);
    u4->agregarContacto(u3);

    //CREACION DE CONVERSACIONES
    Conversacion *g1;
    Conversacion *cs2;
    Conversacion *cs3;

    //CONVERSACION GRUPAL ENTRE U1, U2, U3, U4
    Grupo *g;
    g = new Grupo("Amigos", "home/img/amigos.png");
    hora = new DTHora(15, 35);
    fecha = new DTFecha(22, 05, 2017);

    //Creo el grupo
    g->agregarUsuario(u1);
    g->agregarUsuario(u2);
    g->agregarUsuario(u3);
    g->agregarUsuario(u4);
    g->setFechaCreacion(fecha);
    g->setHoraCreacion(hora);
    g->setAdministrador(u1);

    listaGrupos->agregarGrupo(g);

    //Creacion de la conversacion del grupo

    //Creacion de mensajes
    fecha = NULL;
    hora = NULL;
    fecha = new DTFecha(22, 05, 2017);
    hora = new DTHora(18, 04);
    Mensaje *m1;
    m1 = new MensajeSimple("¡Miren que bueno este video!");
    m1->resetContador();
    m1->setCodigo(m1->getContador());
    (&dynamic_cast<MensajeSimple&> (*m1))->setFechaEnviado(fecha);
    (&dynamic_cast<MensajeSimple&> (*m1))->setHoraEnviado(hora);
    m1->setEmisor(u4);
    m1->agregarReceptor(u1);
    m1->agregarReceptor(u2);
    m1->agregarReceptor(u3);
    m1->aumentarContador();

    fecha = NULL;
    hora = NULL;
    fecha = new DTFecha(22, 05, 2018);
    hora = new DTHora(18, 05);
    Mensaje *m2;
    m2 = new MultimediaVideo("home/vid/videos/gatitos.mp4", 5, 0);
    m2->setCodigo(m2->getContador());
    (&dynamic_cast<MultimediaVideo&> (*m2))->setFechaEnviado(fecha);
    (&dynamic_cast<MultimediaVideo&> (*m2))->setHoraEnviado(hora);
    m2->setEmisor(u4);
    m2->agregarReceptor(u1);
    m2->agregarReceptor(u2);
    m2->agregarReceptor(u3);
    m2->aumentarContador();

    fecha = NULL;
    hora = NULL;
    fecha = new DTFecha(22, 05, 2018);
    hora = new DTHora(18, 12);
    Mensaje *m3;
    m3 = new MensajeSimple("¡Muy gracioso!");
    m3->setCodigo(m3->getContador());
    (&dynamic_cast<MensajeSimple&> (*m3))->setFechaEnviado(fecha);
    (&dynamic_cast<MensajeSimple&> (*m3))->setHoraEnviado(hora);
    m3->setEmisor(u1);
    m3->agregarReceptor(u2);
    m3->agregarReceptor(u3);
    m3->agregarReceptor(u4);
    m3->aumentarContador();

    fecha = NULL;
    hora = NULL;
    fecha = new DTFecha(22, 05, 2018);
    hora = new DTHora(18, 13);
    Mensaje *m4;
    m4 = new MensajeSimple("¡Excelentes!");
    m4->setCodigo(m4->getContador());
    (&dynamic_cast<MensajeSimple&> (*m4))->setFechaEnviado(fecha);
    (&dynamic_cast<MensajeSimple&> (*m4))->setHoraEnviado(hora);
    m4->setEmisor(u3);
    m4->agregarReceptor(u1);
    m4->agregarReceptor(u2);
    m4->agregarReceptor(u4);
    m4->aumentarContador();

    //Creacion de visualizaciones
    //Visualizaciones de m1
    fecha = NULL;
    hora = NULL;
    fecha = new DTFecha(22, 05, 2017);
    hora = new DTHora(18, 05);
    Visualizacion *v;
    v = new Visualizacion(hora, fecha, true, u1);
    m1->agregarVisualizacion(v);

    fecha = NULL;
    hora = NULL;
    fecha = new DTFecha(22, 05, 2017);
    hora = new DTHora(18, 05);
    v = NULL;
    v = new Visualizacion(hora, fecha, true, u3);
    m1->agregarVisualizacion(v);

    fecha = NULL;
    hora = NULL;
    fecha = new DTFecha(0, 0, 0);
    hora = new DTHora(0, 0);
    v = NULL;
    v = new Visualizacion(hora, fecha, false, u2);
    m1->agregarVisualizacion(v);

    //Visualizaciones de m2
    fecha = NULL;
    hora = NULL;
    fecha = new DTFecha(22, 05, 2018);
    hora = new DTHora(18, 06);
    v = NULL;
    v = new Visualizacion(hora, fecha, true, u1);
    m2->agregarVisualizacion(v);

    fecha = NULL;
    hora = NULL;
    fecha = new DTFecha(22, 05, 2018);
    hora = new DTHora(18, 06);
    v = NULL;
    v = new Visualizacion(hora, fecha, true, u3);
    m2->agregarVisualizacion(v);

    fecha = NULL;
    hora = NULL;
    fecha = new DTFecha(0, 0, 0);
    hora = new DTHora(0, 0);
    v = NULL;
    v = new Visualizacion(hora, fecha, false, u2);
    m2->agregarVisualizacion(v);

    //Vizualizaciones de m3
    fecha = NULL;
    hora = NULL;
    fecha = new DTFecha(22, 05, 2018);
    hora = new DTHora(18, 12);
    v = NULL;
    v = new Visualizacion(hora, fecha, true, u3);
    m3->agregarVisualizacion(v);

    fecha = NULL;
    hora = NULL;
    fecha = new DTFecha(22, 05, 2018);
    hora = new DTHora(18, 12);
    v = NULL;
    v = new Visualizacion(hora, fecha, true, u4);
    m3->agregarVisualizacion(v);

    fecha = NULL;
    hora = NULL;
    fecha = new DTFecha(0, 0, 0);
    hora = new DTHora(0, 0);
    v = NULL;
    v = new Visualizacion(hora, fecha, false, u2);
    m3->agregarVisualizacion(v);

    //Visualizacion de m4
    fecha = NULL;
    hora = NULL;
    fecha = new DTFecha(22, 05, 2018);
    hora = new DTHora(18, 13);
    v = NULL;
    v = new Visualizacion(hora, fecha, true, u1);
    m4->agregarVisualizacion(v);

    fecha = NULL;
    hora = NULL;
    fecha = new DTFecha(22, 05, 2018);
    hora = new DTHora(18, 13);
    v = NULL;
    v = new Visualizacion(hora, fecha, true, u4);
    m4->agregarVisualizacion(v);

    fecha = NULL;
    hora = NULL;
    fecha = new DTFecha(0, 0, 0);
    hora = new DTHora(0, 0);
    v = NULL;
    v = new Visualizacion(hora, fecha, false, u2);
    m4->agregarVisualizacion(v);

    //Termino de crear la conversacion
    g1 = new ConversacionGrupo("Amigos");
    g1->resetContador();
    g1->agregarMensaje(m1);
    g1->agregarMensaje(m2);
    g1->agregarMensaje(m3);
    g1->agregarMensaje(m4);
    (&dynamic_cast<ConversacionGrupo&> (*g1))->agregarParticipante(u1);
    (&dynamic_cast<ConversacionGrupo&> (*g1))->agregarParticipante(u2);
    (&dynamic_cast<ConversacionGrupo&> (*g1))->agregarParticipante(u3);
    (&dynamic_cast<ConversacionGrupo&> (*g1))->agregarParticipante(u4);
    g1->setID(g1->getContadorID());
    u1->agregarConversacion(g1);

    g1 = new ConversacionGrupo("Amigos");
    g1->agregarMensaje(m1);
    g1->agregarMensaje(m2);
    g1->agregarMensaje(m3);
    g1->agregarMensaje(m4);
    (&dynamic_cast<ConversacionGrupo&> (*g1))->agregarParticipante(u1);
    (&dynamic_cast<ConversacionGrupo&> (*g1))->agregarParticipante(u2);
    (&dynamic_cast<ConversacionGrupo&> (*g1))->agregarParticipante(u3);
    (&dynamic_cast<ConversacionGrupo&> (*g1))->agregarParticipante(u4);
    g1->setID(g1->getContadorID());
    u2->agregarConversacion(g1);

    g1 = new ConversacionGrupo("Amigos");
    g1->agregarMensaje(m1);
    g1->agregarMensaje(m2);
    g1->agregarMensaje(m3);
    g1->agregarMensaje(m4);
    (&dynamic_cast<ConversacionGrupo&> (*g1))->agregarParticipante(u1);
    (&dynamic_cast<ConversacionGrupo&> (*g1))->agregarParticipante(u2);
    (&dynamic_cast<ConversacionGrupo&> (*g1))->agregarParticipante(u3);
    (&dynamic_cast<ConversacionGrupo&> (*g1))->agregarParticipante(u4);
    g1->setID(g1->getContadorID());
    u3->agregarConversacion(g1);

    g1 = new ConversacionGrupo("Amigos");
    g1->agregarMensaje(m1);
    g1->agregarMensaje(m2);
    g1->agregarMensaje(m3);
    g1->agregarMensaje(m4);
    (&dynamic_cast<ConversacionGrupo&> (*g1))->agregarParticipante(u1);
    (&dynamic_cast<ConversacionGrupo&> (*g1))->agregarParticipante(u2);
    (&dynamic_cast<ConversacionGrupo&> (*g1))->agregarParticipante(u3);
    (&dynamic_cast<ConversacionGrupo&> (*g1))->agregarParticipante(u4);
    g1->setID(g1->getContadorID());
    u4->agregarConversacion(g1);

    g->setConversacion(g1);
    g1->aumentarContador();

    //CONVERSACION ENTRE U1 Y U2

    //Creacion de los mensajes de la conversacion
    Mensaje *m5;
    Mensaje *m6;
    Mensaje *m7;

    m5 = new MensajeSimple("Hola, me pasas el contacto de Sara que no lo tengo");
    m5->setCodigo(m5->getContador());
    (&dynamic_cast<MensajeSimple&> (*m5))->setEmisor(u2);
    (&dynamic_cast<MensajeSimple&> (*m5))->agregarReceptor(u1);
    hora = new DTHora(12, 23);
    fecha = new DTFecha(23, 05, 2018);
    m5->setFechaEnviado(fecha);
    m5->setHoraEnviado(hora);
    fecha = NULL;
    hora = NULL;
    m5->aumentarContador();
    
    m6 = new MensajeContacto("Sara Ruiz", "090666777");
    m6->setCodigo(m6->getContador());
    (&dynamic_cast<MensajeContacto&> (*m6))->setEmisor(u1);
    (&dynamic_cast<MensajeContacto&> (*m6))->agregarReceptor(u2);
    fecha = new DTFecha(23, 05, 2018);
    hora = new DTHora(12, 25);
    m6->setFechaEnviado(fecha);
    m6->setHoraEnviado(hora);
    fecha = NULL;
    hora = NULL;
    m6->aumentarContador();

    m7 = new MensajeSimple("Gracias");
    m7->setCodigo(m7->getContador());
    (&dynamic_cast<MensajeSimple&> (*m7))->setEmisor(u2);
    (&dynamic_cast<MensajeSimple&> (*m7))->agregarReceptor(u1);
    fecha = new DTFecha(23, 05, 2018);
    hora = new DTHora(12, 26);
    m7->setFechaEnviado(fecha);
    m7->setHoraEnviado(hora);
    fecha = NULL;
    hora = NULL;
    m7->aumentarContador();

    //Creacion de las visualizaciones de los mensajes

    fecha = new DTFecha(23, 05, 2018);
    hora = new DTHora(12, 23);
    v = new Visualizacion(hora, fecha, true, u1);
    m5->agregarVisualizacion(v);
    fecha = NULL;
    hora = NULL;
    v = NULL;

    fecha = new DTFecha(23, 05, 2018);
    hora = new DTHora(12, 25);
    v = new Visualizacion(hora, fecha, true, u2);
    m6->agregarVisualizacion(v);
    fecha = NULL;
    hora = NULL;
    v = NULL;

    fecha = new DTFecha(23, 05, 2018);
    hora = new DTHora(12, 26);
    v = new Visualizacion(hora, fecha, true, u1);
    m7->agregarVisualizacion(v);

    //Agrego la conversacion a todos los participantes
    cs2 = new ConversacionSimple();
    (&dynamic_cast<ConversacionSimple&> (*cs2))->agregarParticipante(u1);
    (&dynamic_cast<ConversacionSimple&> (*cs2))->agregarParticipante(u2);
    cs2->agregarMensaje(m5);
    cs2->agregarMensaje(m6);
    cs2->agregarMensaje(m7);
    cs2->setID(cs2->getContadorID());
    u1->agregarConversacion(cs2);

    cs2 = new ConversacionSimple();
    (&dynamic_cast<ConversacionSimple&> (*cs2))->agregarParticipante(u1);
    (&dynamic_cast<ConversacionSimple&> (*cs2))->agregarParticipante(u2);
    cs2->agregarMensaje(m5);
    cs2->agregarMensaje(m6);
    cs2->agregarMensaje(m7);
    cs2->setID(cs2->getContadorID());
    u2->agregarConversacion(cs2);

    cs2->aumentarContador();

    //CONVERSACION ENTRE U3 Y U4

    //Creo mensajes de la conversacion
    Mensaje *m8;
    m8 = new MensajeSimple("Hola Pablo, cómo estas?");
    m8->setCodigo(m8->getContador());
    (&dynamic_cast<MensajeSimple&> (*m8))->setEmisor(u4);
    (&dynamic_cast<MensajeSimple&> (*m8))->agregarReceptor(u3);
    fecha = new DTFecha(23, 05, 2018);
    hora = new DTHora(18, 30);
    m8->setFechaEnviado(fecha);
    m8->setHoraEnviado(hora);
    m8->aumentarContador();

    //Creo las visualizaciones
    fecha = new DTFecha(23, 05, 2018);
    hora = new DTHora(12, 31);
    v = new Visualizacion(hora, fecha, true, u3);
    m8->agregarVisualizacion(v);
    cout << endl;
    cout << " ¡Datos cargados con exito!" << endl;
    sleep(1);

    //Agrego la conversacion a los participantes
    cs3 = new ConversacionSimple();
    (&dynamic_cast<ConversacionSimple&> (*cs3))->agregarParticipante(u3);
    (&dynamic_cast<ConversacionSimple&> (*cs3))->agregarParticipante(u4);
    cs3->agregarMensaje(m8);
    cs3->setID(cs3->getContadorID());
    u3->agregarConversacion(cs3);

    cs3 = new ConversacionSimple();
    (&dynamic_cast<ConversacionSimple&> (*cs3))->agregarParticipante(u3);
    (&dynamic_cast<ConversacionSimple&> (*cs3))->agregarParticipante(u4);
    cs3->agregarMensaje(m8);
    cs3->setID(cs3->getContadorID());
    u4->agregarConversacion(cs3);

    cs3->aumentarContador();
}

void ControllerUsuario::modificarReloj(int dia, int mes, int anio, int horas, int minutos) {
    Reloj *r;
    r = r->getInstancia();
    r->modificarReloj(dia, mes, anio, horas, minutos);
    cout << endl;
    cout << "¡Reloj modificado con exito!" << endl;
    sleep(1);
}

void ControllerUsuario::consultarReloj() {
    Reloj *r;
    r = r->getInstancia();
    system("clear");
    cout << " ___________________________ " << endl;
    cout << "|                           |" << endl;
    cout << "|     -CONSULTAR RELOJ-     |" << endl;
    cout << "|___________________________|" << endl;
    cout << "                             " << endl;
    cout << "      " << r->getDia() << "/" << r->getMes() << "/" << r->getAnio() << " - " << r->getHoras() << ":" << r->getMinutos() << endl;
    sleep(3);
}
