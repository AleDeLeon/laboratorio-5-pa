#include "Mensaje.h"
#include "Visualizacion.h"
#include "Reloj.h"

int Mensaje::contador = 1;

//CONSTRUCTORES

Mensaje::Mensaje(){
    this->visualizaciones = new list<Visualizacion*>;
    this->receptores = new list<Usuario*>;
}

Mensaje::Mensaje(const Mensaje& orig) {
}

//DESTRUCTORES

Mensaje::~Mensaje() {
}

//GETTERS

int Mensaje::getCodigo(){
    return this->codigo;
}

DTHora *Mensaje::getHoraEnviado(){
    return this->horaEnviado;
}

DTFecha *Mensaje::getFechaEnviado(){
    return this->fechaEnviado;
}

list<Usuario*> *Mensaje::getReceptores(){
    return this->receptores;
}

Usuario *Mensaje::getEmisor(){
    return this->emisor;
}

list<Visualizacion*> *Mensaje::getVisualizaciones(){
    return this->visualizaciones;
}

int Mensaje::getContador(){
    return this->contador;
}

//SETTERS

void Mensaje::setCodigo(int Codigo){
    this->codigo = Codigo;
}

void Mensaje::setHoraEnviado(DTHora *HoraEnviado){
    this->horaEnviado = HoraEnviado;
}

void Mensaje::setFechaEnviado(DTFecha *FechaEnviado){
    this->fechaEnviado = FechaEnviado;
}

void Mensaje::setEmisor(Usuario* Emisor){
    this->emisor = Emisor;
}

//OTROS

void Mensaje::agregarReceptor(Usuario* u){
    this->receptores->push_back(u);
}

void Mensaje::borrarReceptor(Usuario* u){
    this->receptores->remove(u);
}

void Mensaje::agregarVisualizacion(Visualizacion* v){
    this->visualizaciones->push_back(v);
}

void Mensaje::borrarVisualizacion(Visualizacion* v){
    this->visualizaciones->remove(v);
}

void Mensaje::aumentarContador(){
    contador++;
}

void Mensaje::resetContador(){
    contador = 1;
}

//OPERACIONES DE ENVIAR MENSAJE

void Mensaje::crearVisualizacion(Usuario* u){
    DTHora *h = new DTHora(0, 0);
    DTFecha *f = new DTFecha(0, 0, 0);
    Visualizacion *v;
    v = new Visualizacion(h, f, false, u);
    this->visualizaciones->push_back(v);
}

//OPERACIONES DE VER MENSAJES

list<DTVisualizacion*> *Mensaje::infoMensajeEnviado(){
    list<DTVisualizacion*> *listaVisualizaciones = new list<DTVisualizacion*>;
    DTVisualizacion *auxVisualizacion;
    for(list<Visualizacion*>::iterator i = this->visualizaciones->begin(); i != this->visualizaciones->end(); i++){
        auxVisualizacion = (*i)->getDatos();
        listaVisualizaciones->push_back(auxVisualizacion);
    }
    return listaVisualizaciones;
}